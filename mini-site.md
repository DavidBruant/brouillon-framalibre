---
layout: default
styles:
    - href: "style/mini-site.css"
---


<h1><span>Les mini-sites Framalibre : partagez vos sélections !</span></h1>

## C'est quoi un mini-site ?

Un mini-site c'est :
- **une liste d'outils libres** que vous voulez recommander,
- la création simplifiée d'une page web à l'aide de [Scribouilli](https://framalibre.org/notices/scribouilli.html),
- un thème Framalibre aux petits oignons,
- **une citation facilitée des notices Framalibre** grâce à un bouton magique,
- à la fin, **un lien à partager avec votre liste de recommandations** rien qu'à vous !

## Pourquoi faire un mini-site ?

> Dis, tu connais un outil pour faire des visios qui respecte la vie privée ?

> Dis, je cherche un logiciel pour retoucher mes photos, t'as quoi à me recommander ?

> Dis, je veux arrêter d'utiliser mon vieux mail, il y en a qui sont plus éthiques ?

Votre entourage vous a peut-être déjà demandé conseil pour choisir un outil numérique ?

Que cela soit courant ou non, **nous avons besoin de vous** pour continuer à faire savoir que oui, **il existe des outils numériques respectueux** de leurs utilisateurs et utilisatrices !

Pour vous faciliter la vie, nous vous proposons de générer **une liste de recommandations** sous forme d'un mini-site pour pouvoir la partager autour de vous !

Que se soit un mini-site pour :
- <a href="https://numahell.gitlab.io/applications-libres-sur-android">présenter **ses applications pour ordiphone** préférées,</a>
- savoir **comment randonner** avec des outils libres,
- **écrire** avec des outils non-privateurs,
- <a href="https://maiwann.monpetitsite.org/logiciels-et-services-libres-pour-une-association/">vouloir **communiquer pour son association** avec des outils éthiques</a>…

il y a une galaxie de mini-sites imaginables. **À vous de** [créer le vôtre](https://atelier.scribouilli.org/) !

## Comment on le crée ce mini-site ?

### 1° Créer son mini-site
- Suivez le parcours d'inscription [de Scribouilli](https://atelier.scribouilli.org/).
- Choisissez **"ma liste de recommandations liée à Framalibre"** dans la liste déroulante

### 2° Choisissez les logiciels à recommander
- Naviguez dans les notices de [Framalibre](https://framalibre.org)
- Ouvrez la notice du logiciel que vous voulez recommander
- Tout en **bas à gauche** de la notice, cliquez sur **"Copier pour mon mini-site"**
- Un bout de code HTML vient de se copier dans votre presse-papier
- Il vous reste à **le coller dans votre mini-site…**
- …et **recommencer** pour les suivants 
- et enfin, **publier la page** pour tout enregistrer !


<img src="{{'images/capture_bouton_copier_minisite.png' | relative_url}}" alt="">

Et voilà ! Il n'y a plus qu'à voir le résultat, rajouter un logiciel que vous auriez oublié… et à le partager autour de vous !
