# frozen_string_literal: true

module NoticeJson
  # Jekyll page class that doesn't read from a file
  class PageWithoutAFile < Jekyll::Page
    def read_yaml(*)
      @data ||= {}
    end
  end

  # Jekyll plugin to generate raw content
  class Generator < Jekyll::Generator
    safe true

    def generate(site)
      @site = site

      site.collections.each do |name, collection|
        next unless name == 'notices'

        Jekyll.logger.info "Generating JSON for #{collection.docs.size} notices"

        all_notices_data = []

        collection.docs.each do |notice|
          notice.data['raw_content'] = notice.content
          notice.data['id'] = notice.data['slug']
          notice.data['url'] = notice.url

          all_notices_data << notice.data
          site.pages << json_page(notice.data, notice.url)
        end

        site.pages << json_page(all_notices_data, '/notices.json')

        Jekyll.logger.info "Done generating JSON for #{collection.docs.size} notices!"
      end
    end

    def json_page(notice_data, notice_url)
      filepath = CGI.unescape(notice_url).sub(/.html$/, '')

      PageWithoutAFile.new(site, site.source, '.', filepath).tap do |file|
        file.content = notice_data.to_json
        file.ext = '.json'
        file.output
      end
    end

    private

    attr_reader :site
  end
end
