---
layout: default
styles:
    - href: "style/contribuer.css"
    - href: "style/notice.css"
scripts:
    - src: "build/contribute.js"
      defer: true
---

<h1><span>Contribuer</span></h1>

{% assign allTags =  site.notices | map: 'tags' | uniq | sort_natural %}


<form class="contribute">
    <input type="hidden" name="date_creation">
    <input type="hidden" name="logo_src">
    <label>
        <span class="mb-1">Nom du logiciel (champ requis)</span>
        <input name="nom" required class="form-control" />
    </label>
    <label>
        <span class="mb-1">Image du logo</span>
        <input name="logo" type="file" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Site web du logiciel</span>
        <input name="site_web" type="url" class="form-control" />
    </label>
    <section class="plateformes">
        <span class="mb-1">Le logiciel est disponible sur...</span>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="le web">
            <span>le web</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="Apple iOS">
            <span>Apple iOS</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="Android">
            <span>Android</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="Mac OS X">
            <span>Mac OS X</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="Windows">
            <span>Windows</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="GNU/Linux">
            <span>GNU/Linux</span>
        </label>
        <label>
            <input type="checkbox" name="plateformes" class="form-control" value="Autre">
            <span>Autre</span>
        </label>
    </section>
    <section class="langues">
        <span class="mb-1">Langues</span>
        <label>
            <input type="checkbox" name="langues" class="form-control" value="Français">
            <span>Français</span>
        </label>
        <label>
            <input type="checkbox" name="langues" class="form-control" value="English">
            <span>English</span>
        </label>
        <label>
            <input type="checkbox" name="langues" class="form-control" value="Español">
            <span>Español</span>
        </label>
        <label>
            <input type="checkbox" name="langues" class="form-control" value="Autres langues">
            <span>Autres langues</span>
        </label>
    </section>
    <label>
        <span class="mb-1">Personnes ou collectif qui ont créé le logiciel</span>
        <input name="createurices" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Alternative à</span>
        <input name="alternative_a" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Licences du logiciel (champ requis)</span>
        <select multiple name="licences" required class="form-control">
            <option>Berkeley Software Distribution License (BSD)</option>
            <option>Licence MIT/X11 (MIT)</option>
            <option>Licence Publique Générale GNU (GNU GPL)</option>
            <option>Licence Publique Générale Affero (AGPL)</option>
            <option>Licence publique générale limitée GNU (LGPL)</option>
            <option>Licence Apache (Apache)</option>
            <option>Licence Publique Mozilla (MPL)</option>
            <option>Creative Commons Zero (CC0-1.0)</option>
            <option>Licence CECILL (Inria)</option>
            <option>Multiples licences</option>
            <!-- -->
            <option>Cern Open Hardware Licence</option>
            <option>Common Public License (CPL)</option>
            <option>Design Science License (DSL)</option>
            <option>Licence Art Libre (LAL)</option>
            <option>Licence Beerware</option>
            <option>Licence de base de données ouverte (OdbL)</option>
            <option>Licence de Documentation Libre GNU (GNU FDL)</option>
            <option>Licence de Publication Ouverte (OPL)</option>
            <option>Licence Libre Académique (AFL)</option>
            <option>Licence Open Source NCSA/Université de l'Illinois (NCSA)</option>
            <option>Licence Publique Eclipse (EPL)</option>
            <option>Licence publique f***-en ce que vous voulez (WTFPL)</option>
            <option>Licence publique LaTeX (LPPL)</option>
            <option>Licence publique Sun (SPL)</option>
            <option>Licence Publique Union Européenne (EUPL)</option>
            <option>Licence Zlib</option>
            <option>ODC Public Domain Dedication and Licence (PDDL)</option>
            <option>Open Font License (SIL-OFL)</option>
            <option>Open Software License (OSL)</option>
            <option>TAPR Open Hardware License</option>
            <option>Unlicense</option>
            <option>Creative Commons (CC-By)</option>
            <option>Creative Commons (CC-By-Sa)</option>
            <option>Domaine public</option>
            <option>FSF Unlimited License</option>
            <option>Autre</option>
        </select>
    </label>
    <label>
        <span class="mb-1">Liste de mots-clefs</span>
        <p>(privilégiez les mots-clefs suggérés par le formulaire)</p>
        <datalist id="tags">
        {% for tag in allTags -%}
            <option>{{ tag }}</option>
        {%- endfor -%}
        </datalist>
        <input name="tag" list="tags" class="form-control mb-1" />
        <input name="tag" list="tags" class="form-control mb-1" />
        <input name="tag" list="tags" class="form-control mb-1" />
        <input name="tag" list="tags" class="form-control mb-1" />
        <input name="tag" list="tags" class="form-control mb-1" />
    </label>
    <label>
        <span class="mb-1">Description courte. Sera affichée dans les résultats de recherche. Max 200 caractères</span>
        <input name="description_courte" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Description longue (<a href="https://flus.fr/carnet/markdown.html" target="_blank">markdown</a> autorisé)</span>
        <textarea cols="100" rows="10" name="description_longue" class="form-control"></textarea>
    </label>
    <label>
        <span class="mb-1">Lien vers la <a href="https://fr.wikipedia.org/">page Wikipedia</a> du logiciel</span>
        <input name="lien_wikipedia" type="url" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Lien vers la <a href="https://reports.exodus-privacy.eu.org/fr/">page Exodus</a> du logiciel</span>
        <input name="lien_exodus" type="url" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Identifiant du logiciel sur <a href="https://wikidata.org/">wikidata</a></span>
        <input name="identifiant_wikidata" placeholder="Exemple: Q698" class="form-control" />
    </label>
    <label>
        <span class="mb-1">Votre pseudo ou nom</span>
        <input name="contributeur.rice" class="form-control" />
    </label>

    <div hidden id="alert" class="alert alert-danger">
    </div>

    <button class="btn btn-outline-warning" type="submit">Créer la notice !</button>
    
    <span class="info-licence">🌱 Votre contribution sera placée sous licence libre
        <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><span lang="en">Creative Commons By</span>-<abbr>SA</abbr> 4.0</a>.
    </span>
</form>
