---
nom: "Replicant"
date_creation: "Vendredi, 5 avril, 2019 - 22:24"
date_modification: "Jeudi, 17 août, 2023 - 21:07"
logo:
    src: "images/logo/Replicant.png"
site_web: "https://replicant.us/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un Android dégooglisé, entièrement libre et soutenu par la FSF."
createurices: "Bradley M. Kuhn, Aaron Williamson, Graziano Sorbaioli, Denis Carikli"
alternative_a: "Microsoft Windows, ios"
licences:
    - "Licence Apache (Apache)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "android"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Replicant_(syst%C3%A8me_d%27exploitation)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/replicant"
---

Replicant est un système d'exploitation Android dérivé de LineageOS dont les logiciels privateurs ont étés enlevés et remplacés par des logiciels open-source. Ce système d'exploitation est soutenu par la Free Software Foundation depuis 2013.

