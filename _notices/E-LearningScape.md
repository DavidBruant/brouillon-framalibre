---
nom: "E-LearningScape"
date_creation: "Mercredi, 21 octobre, 2020 - 16:23"
date_modification: "Mardi, 13 décembre, 2022 - 11:58"
logo:
    src: "images/logo/E-LearningScape.png"
site_web: "https://github.com/Mocahteam/E-LearningScape"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Sortirez-vous de votre rêve ?"
createurices: "SAPIENS, CRI, Sorbonne Université, INS HEA"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu sérieux"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/e-learningscape"
---

E-LearningScape est un escape game mêlant contenus réels et virtuels. Les joueurs incarnent des marchands de sable naviguant dans le rêve d'une personne et devant l'aider à répondre à toutes les questions qu'elle se pose avant qu'elle ne se réveille.
Trois versions du jeu sont disponibles :
1 - E-LearningScape Péda est la version originelle du jeu traitant de notions généralistes sur la pédagogie
2 - E-LearningScape Access est une version du jeu traitant de notions relatives au handicap, l'accessibilité et la société inclusive
3 - E-LearningScape Info est une version du jeu traitant de notions relatives à l'informatique
D'autres "mods" existent (créés par la communauté) et transforment son contenu pour aborder des thèmes comme le cinéma, le développement durable, la science forensique...

