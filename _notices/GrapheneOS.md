---
nom: "GrapheneOS"
date_creation: "lundi, 24 juin, 2024 - 17:39"
date_modification: "lundi, 24 juin, 2024 - 17:39"
logo:
    src: "images/logo/GrapheneOS.svg"
site_web: "https://grapheneos.org/"
plateformes:
    - "Android"
langues:

description_courte: "Un système d'exploitation mobile privé et sécurisé compatible avec les applications Android"
createurices: "GrapheneOS"
alternative_a: "Android"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "Android"
    - "vie privée"
    - "sécurité"
lien_wikipedia: "https://en.wikipedia.org/wiki/GrapheneOS"
lien_exodus: ""
identifiant_wikidata: "Q85764357"
mis_en_avant: "non"

---

GrapheneOS est un système d'exploitation mobile axé sur la protection de la vie privée et la sécurité, compatible avec les applications Android, développé dans le cadre d'un projet open source à but non lucratif. Il se concentre sur la recherche et le développement de technologies de sécurité et de protection de la vie privée. Il n'est compatible qu'avec les smartphones Google Pixel.


