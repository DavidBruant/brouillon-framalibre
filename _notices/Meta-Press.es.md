---
nom: "Meta-Press.es"
date_creation: "Lundi, 16 décembre, 2019 - 15:11"
date_modification: "mercredi, 27 décembre, 2023 - 10:54"
logo:
    src: "images/logo/Meta-Press.es.png"
site_web: "https://www.meta-press.es"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Moteur de recherche pour la presse : libre et décentralisé Revue de presse automatisée"
createurices: "Simon Descarpentries"
alternative_a: "Google Search"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "presse"
    - "métamoteur de recherche"
    - "firefox"
    - "web"
    - "journal"
    - "journalisme"
    - "recherche"
    - "recherche web"
    - "accessibilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/meta-presses"
---

Envie d’alléger l’empreinte écologique de votre usage du numérique ? De protéger votre vie privée ? De vous passer de Google pour faire une recherche ? Meta-Press.es est là pour ça.
Il s’agit d'une WebExtension pour Firefox, qui se limite à la presse mais se passe d’intermédiaire !
Avec Meta-Press.es c’est votre ordinateur qui travaille (pas besoin de datacenter au cercle polaire) et il vous permet d’interroger des centaines de journaux et de découvrir des millions de résultats en quelques secondes.
Meta-Press.es vous redonne le contrôle de vos sources d'information, quand les autres moteurs de recherche vous prient de croire qu’ils ont cherché partout, sans vous permettre de le vérifier…
De plus Meta-Press.es permet de programmer des recherches, de sélectionner des résultats et d’exporter une revue de presse en quelques clics.
Après, de nombreux journaux ont encore besoin d’être intégrés et maintenir les sources existantes demande un travail régulier.


