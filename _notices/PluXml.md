---
nom: "PluXml"
date_creation: "Mercredi, 25 février, 2015 - 17:33"
date_modification: "mercredi, 27 décembre, 2023 - 10:54"
logo:
    src: "images/logo/PluXml.png"
site_web: "https://pluxml.org"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Système de gestion de contenu sans base de données développé en PHP"
createurices: "Anthony GUERIN, Florent MONTHEL, Stephane FERRARI, Pedro CADETE, Jean-Pierre POURREZ"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
    - "php"
lien_wikipedia: "http://fr.wikipedia.org/wiki/PluXml"
lien_exodus: ""
identifiant_wikidata: "Q16670482"
mis_en_avant: "non"
redirect_from: "/content/pluxml"
---

Système de gestion de contenu développé en PHP. Les données sont stockées dans des fichiers XML. Il n'est donc pas nécessaire de disposer d'une base de données.

- PluXml est personnalisable par l'intermédiaire de thèmes et/ou de plugins.
- Multi-utilisateurs avec des niveaux d'autorisations différents
- Pages statiques, catégories, gestion des tags, archives
- Gestion des commentaires
- Gestionnaire de médias : images, documents
- Flux Rss des articles, commentaires, tags, catégories
- Traduit en 11 langues (français, allemand, anglais, espagnol, italien, néerlandais, occitan, polonais, portugais, roumain, russe)
- Réécriture d'urls

Le tout développé par une équipe francophone.
