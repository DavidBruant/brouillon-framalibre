---
nom: "PosteRazor"
date_creation: "vendredi, 19 avril, 2024 - 14:04"
date_modification: "vendredi, 19 avril, 2024 - 14:04"
logo:
    src: "images/logo/PosteRazor.jpg"
site_web: "https://posterazor.sourceforge.io/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Découpe une grande image en plusieurs sous parties avec chevauchement pour assembler de grandes affiches"
createurices: "PosteRazor team"
alternative_a: "RonyaSoft Poster Printer, Mindcad Tiler, Easy Poster Printer, Posterizer, Block Posters"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "pdf"
    - "manipulation d'images"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cet utilitaire permet de découper une image (de préférence au format png même si de nombreux formats d'images sont acceptés par le logiciel) en plusieurs pages A4. Une bande de chevauchement est prévue pour faciliter l'assemblage des différentes pages ensemble. Ainsi, une image de taille A4 pourra être découpée en 2 ou 4 pages A4 pour imprimer et assembler une affiche en format A3 ou A2. Il est possible de produire des banderoles ou des affiches de très grande taille.
