---
nom: "ClicShopping"
date_creation: "Mercredi, 7 novembre, 2018 - 16:10"
date_modification: "Jeudi, 8 novembre, 2018 - 11:30"
logo:
    src: "images/logo/ClicShopping.png"
site_web: "https://www.clicshopping.org"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Solution de e-commerce complète, facile et rapide"
createurices: "ClicShopping"
alternative_a: ""
licences:
    - "Common Public License (CPL)"
    - "Licence MIT/X11"
tags:
    - "cms"
    - "e-commerce"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

ClicShoppingTm est une solution Open Source, facile à installer, légère, utilisable sur tout serveur et très modulaire. Le code est facile à comprendre et très flexible.
 Lorsque vous installez ClicShoppingTm Store Online, certains modules sont pré-installés au préalable. Vous pouvez rapidement installer de nouveaux modules dans l'application et créer votre propre application.
Certaines solutions de panier d'achat semblent être des exercices de programmation compliqués au lieu de répondre aux besoins des utilisateurs. CliShoppingTm donne la priorité aux exigences des commerçants et des acheteurs. De même, il est presque impossible d'installer et d'utiliser d'autres logiciels de panier d'achat sans diplôme en informatique. ClicShoppingTm peut être installé et configuré par toute personne possédant les connaissances de base en construction de site Web.
Des centaines de programmes de paniers d'achat sont disponibles, mais aucun n'offre ce niveau de fonctionnalités.

