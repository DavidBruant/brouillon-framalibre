---
nom: "Emargements"
date_creation: "mardi, 16 avril, 2024 - 13:51"
date_modification: "mardi, 16 avril, 2024 - 13:51"
logo:
    src: "images/logo/Emargements.png"
site_web: "https://emargements.philnoug.com/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Feuille de présences numériques"
createurices: "NOUGAILLON Philippe"
alternative_a: "edusign"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "signature"
    - "formation"
    - "reporting"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cette application permet à des participants de signer numériquement leur présence à une assemblée.
