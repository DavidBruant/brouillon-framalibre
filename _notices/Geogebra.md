---
nom: "Geogebra"
date_creation: "Jeudi, 29 décembre, 2016 - 22:35"
date_modification: "Mercredi, 20 avril, 2022 - 17:51"
logo:
    src: "images/logo/Geogebra.png"
site_web: "https://www.geogebra.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "GeoGebra permet d'explorer la géométrie affine"
createurices: "Markus Hohenwarter"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "mathématiques"
    - "géométrie"
    - "éducation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GeoGebra"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/geogebra"
---

GeoGebra permet d'explorer la géométrie affine : l'utilisateur peut manipuler les différents objets géométriques de base dans un plan : cercle, droite, angle, etc.
Il est principalement utilisé par des enseignants, mais toute personne souhaitant explorer de façon visuelle les transformations géométriques dans le plan en tirera profit. Des chercheurs en géométrie peuvent l'utiliser pour valider de nouvelles constructions faisant appel aux propriétés d'incidence du plan, telles le parallélisme.
La possibilité d'adapter les unités du repère au problème en cours en fait un excellent grapheur, d'autant plus qu'il est immédiat (grâce à la fenêtre de saisie) d'entrer la fonction, et qu'une fois que c'est fait, on peut lui appliquer du calcul formel (recherche de zéros, d'un extremum...).

