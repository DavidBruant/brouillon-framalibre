---
nom: "MultiCalc"
date_creation: "jeudi, 27 février, 2025 - 08:38"
date_modification: "jeudi, 27 février, 2025 - 08:38"
logo:
    src: "images/logo/MultiCalc.png"
site_web: "https://educajou.forge.apps.education.fr/multicalc/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Trouver différentes façons d'atteindre un nombre demandé, avec trois cases contigües. Les nombres se combinent avec + - × dans n'importe quel ordre."
createurices: "Arnaud Champollion"
alternative_a: "Trio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "calcul"
    - "procédure"
    - "primaire"
    - "cycle 2"
    - "cycle 3"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

MultiCalc est un jeu permettant de s'entraîner au calcul réfléchi.

En combinant trois nombres et les opérateurs + - x dans n'importe quel ordre il faut atteindre un nombre cible, de plusieurs façons.

Cette activité permet de mettre en oeuvre les compétences déclaratives (ex: connaissance des tables) et procédurales (techniques s'appuyant sur les calculs connus et les propriétés des nombres pour accéder à un résultat).
