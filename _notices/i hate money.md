---
nom: "i hate money"
date_creation: "Jeudi, 22 octobre, 2020 - 14:01"
date_modification: "dimanche, 19 mai, 2024 - 07:16"
logo:
    src: "images/logo/i hate money.png"
site_web: "https://ihatemoney.org/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Application WEB permettant de gérer un budget partagé entre plusieurs personnes."
createurices: ""
alternative_a: "tricount"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "comptabilité"
    - "finances"
    - "gestion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/i-hate-money"
---

Application WEB (pas besoin d'installer un logiciel sur votre ordinateur) qui permet simplement de gérer un budget à plusieurs.
Vous créez un projet avec un nom et un mot de passe, et ensuite pouvez ajouter des personnes participant au budget, des factures et des remboursements.
Des soldes sont calculés automatiquement pour chaque personne enregistrée, permettant de savoir où vous en êtes.
Vous pouvez utiliser l'instance officielle, ou installer la votre, sachant que l'application est disponible pour Yunohost.
