---
nom: "Cadsol"
date_creation: "Mercredi, 2 juin, 2021 - 18:58"
date_modification: "lundi, 23 septembre, 2024 - 11:33"
logo:
    src: "images/logo/Cadsol.jpg"
site_web: "https://cadsol.web-pages.fr"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de gnomonique Tracés de cadrans et visualisation en 3D Impression en 2D (svg) ou 3D (stl ou obj)"
createurices: "Astre Jean-Luc"
alternative_a: "Shadows"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "science"
    - "gnomonique"
    - "cadran solaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cadsol"
---

Cadsol est un logiciel de tracé de cadrans solaires.
Sous licence GNU GPL (Cadsol est donc un logiciel libre).
Le logiciel, et les sources sont disponibles sur SourceForge
Version en ligne disponible (pas de téléchargement, pas d'installation)
 Types de cadran
Cadran classique à gnomon, ou à style polaire
Cadran analemmatique
Cadran bifilaire
Cadran sur une surface paramétrée
Cadrans polyédriques
