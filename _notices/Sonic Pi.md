---
nom: "Sonic Pi"
date_creation: "Samedi, 23 février, 2019 - 19:10"
date_modification: "Lundi, 25 février, 2019 - 13:00"
logo:
    src: "images/logo/Sonic Pi.png"
site_web: "http://sonic-pi.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Sonic Pi est un logiciel de codage de musique en live."
createurices: "Sam Aaron"
alternative_a: ""
licences:
    - "Multiples licences"
tags:
    - "création"
    - "mao"
    - "musique"
    - "live"
    - "streaming"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sonic-pi"
---

Sonic Pi est un logiciel de codage de musique en live.
Il permet de créer de la musique en direct via des lignes de code, en utilisant des synthétiseurs, des samples...
Il est très simple, très puissant, très ergonomique et est basé pour tourner sur Raspberry Pi.

