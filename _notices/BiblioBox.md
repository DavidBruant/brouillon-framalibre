---
nom: "BiblioBox"
date_creation: "Mercredi, 17 octobre, 2018 - 21:52"
date_modification: "samedi, 31 août, 2024 - 17:39"
logo:
    src: "images/logo/BiblioBox.png"
site_web: "https://github.com/djfiander/BiblioBox"
plateformes:
    - "Autre"
langues:
    - "Français"
description_courte: "Dispositif nomade permettant d’accéder à des ressources numériques"
createurices: "Jason Griffey"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "téléchargement"
    - "epub"
    - "musique"
    - "film"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bibliobox"
---

Une BiblioBox est un dispositif de partage de ressources numériques (livres électroniques, vidéos, musiques, logiciels, photos, etc.). Une BiblioBox est une LibraryBox en version française, c'est une déclinaison de la PirateBox.
La BiblioBox génère un réseau auquel on se connecte en wifi. Il suffit alors d'ouvrir un navigateur web pour accéder à une interface à partir de laquelle on télécharge les contenus multimédias mis à disposition.
