---
nom: "Vorta"
date_creation: "vendredi, 6 septembre, 2024 - 12:13"
date_modification: "vendredi, 6 septembre, 2024 - 12:13"
logo:
    src: "images/logo/Vorta.png"
site_web: "https://vorta.borgbase.com"
plateformes:
    - "Mac OS X"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un client de bureau pour Borg Backup"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sauvegarde"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q130243983"
mis_en_avant: "non"

---

Vorta est un client de sauvegarde pour les postes de travail macOS et Linux. Il propose une interface graphique pour sauvegarder ses données avec le logiciel **Borg Backup**.

Il permet d'effectuer des sauvegardes chiffrées, dédupliquées et compressées, de choisir de manière très fine les données à sauvegarder, de programmer la sauvegarde et la purge des anciennes archives et de définir plusieurs profils de configuration.

Enfin, il permet simplement d'accéder aux archives pour récupérer les fichiers sauvegardés.
