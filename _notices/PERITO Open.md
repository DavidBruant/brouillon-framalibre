---
nom: "PERITO Open"
date_creation: "Lundi, 26 octobre, 2020 - 08:06"
date_modification: "Jeudi, 11 février, 2021 - 08:40"
logo:
    src: "images/logo/PERITO Open.png"
site_web: "https://nomad-kreo.fr/index.php/perito-open-gratuit-telechargement"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logiciel de gestion de cabinet d'avocat en mode client/serveur accessible depuis tout navigateur internet."
createurices: "Minh NGUYEN-VAN"
alternative_a: "Kleos, Heliaste"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/perito-open"
---

Perito Open est un logiciel métier de gestion de cabinet d'avocat.
Perito Open a été développé par un informaticien au sein d'un cabinet d'avocat.
Perito Open est opérationnel depuis 2016, il est multi-utilisateurs et est écrit en PHP/Postgresql.
Le logiciel regroupe toutes les fonctions de gestion des dossiers clients sous un même interface, de la prise de rendez-vous à la facturation..

