---
nom: "F-Droid"
date_creation: "Jeudi, 5 janvier, 2017 - 08:06"
date_modification: "Jeudi, 5 janvier, 2017 - 11:58"
logo:
    src: "images/logo/F-Droid.png"
site_web: "https://f-droid.org/repository/browse/?fdfilter=f-droid&fdid=org.fdroid.fdroid"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "F-Droid est une banque d'applications libres pour Android"
createurices: ""
alternative_a: "Google Play"
licences:
    - "Creative Commons -By-Sa"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/F-Droid"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/f-droid"
---

F-Droid est un magasin d'applications libres pour Android
Il permet d'installer sur son matériel (smartphone, tablette, etc...) des applications libres.
F-Droid se révèle très utile pour installer des applications sur des systèmes d’exploitation ne disposant pas de Google Play (ex: OmniRom, CyanogenMod, LineageOS, etc...).
Certaines applications sont d'ailleurs uniquement disponibles sur F-Droid (ex: Dandelion*)

