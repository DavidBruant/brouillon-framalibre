---
nom: "Reactive Resume"
date_creation: "Vendredi, 31 juillet, 2020 - 16:15"
date_modification: "Mercredi, 4 novembre, 2020 - 10:43"
logo:
    src: "images/logo/Reactive Resume.png"
site_web: "https://rxresu.me/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Générateur de CV en ligne."
createurices: "Amruth Pillai"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/reactive-resume"
---

Reactive Resume vous permet de facilement créer votre ou vos CV.
La connexion à Reactive Resume peut se faire via un compte Google ou avec un compte anonyme (dans ce cas, si vous vous déconnectez vous n'aurez plus accès aux CV créés, sauf si vous les avez exportés en Json pour pouvoir ensuite les réimporter ultérieurement).
Après la connexion, l'interface vous guide dans l'ajout des informations à ajouter dans votre CV puis vous permet de personnaliser le thème et la disposition du CV.
Il est ensuite possible d'exporter son CV en PDF ou en Json (afin de pouvoir l'importer de nouveau dans Reactive Resume pour le modifier si vous préférez la connexion anonyme).
Il est possible de partager un CV via une URL.
Une instance est librement proposée (https://rxresu.me/app ) et il est possible de créer sa propre instance à partir des sources GitHub.

