---
nom: "Dokiel"
date_creation: "Mercredi, 18 mai, 2022 - 16:41"
date_modification: "mardi, 21 janvier, 2025 - 16:53"
logo:
    src: "images/logo/Dokiel.png"
site_web: "https://doc.scenari.software/Dokiel"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Dokiel est dédié au domaine de la communication technique et documentation logicielle."
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "documentation"
    - "gestion documentaire"
    - "chaîne éditoriale"
    - "formation"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dokiel"
---

Dokiel optimise la conception de manuels utilisateurs, de sites documentaires web de référence et de supports pédagogiques pour les formations présentielles et distancielles.
Dokiel s'adresse aux rédacteurs techniques et formateurs des éditeurs de logiciels, industriels, fabricants d'équipements, directions informatiques, services support...
Dokiel permet de concevoir des documentations et des supports de formation.
Documentations :
Manuels utilisateur web et PDF
Tutoriels
Guide d'installation et d'exploitation
Document de spécification, cahier des charges
Site web de référence multi-entrées
Supports de formation :
Diaporama de formation vidéoprojeté
Livret stagiaire PDF
Supports pour le formateur
Module e-learning
Auto-évaluation...
Dokiel est un des modèles documentaires de Scenari.
