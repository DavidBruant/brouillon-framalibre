---
nom: "Quant-UX"
date_creation: "jeudi, 15 août, 2024 - 18:24"
date_modification: "jeudi, 15 août, 2024 - 18:24"


site_web: "https://www.quant-ux.com/"
plateformes:
    - "le web"
langues:
    - "English"
    - "Autres langues"
description_courte: "Quant-UX est un outil de prototypage UX/UI permettant de tester (et faire tester) rapidement vos conceptions et d'obtenir des indicateurs basés sur des mesures."
createurices: "Klaus Schaefers"
alternative_a: "Figma"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "ux design"
    - "prototypage"
    - "design"
    - "maquette"
    - "création"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

**Quant-UX est un outil de recherche, d'utilisabilité et de prototypage qui permet de concevoir, évaluer et faire évoluer vos maquettes, afin de valider vos idées rapidement.**

### Prototypage
L'outil permet un prototypage rapide de l'interface utilisateur, grâce à un éditeur visuel et des composants prédéfinis largement configurables. En quelques minutes, vous pouvez créer des maquettes simplifiées mais aussi des prototypes interactifs qui ressemblent à de vraies applications.

### Collaboration
Il est très facile de partager un ou plusieurs projets de prototypage avec d'autres collaborateurs, mais aussi de partager un lien permettant de tester la maquette (avec de nombreuses options pour personnaliser l'expérience de test, ou y ajouter des tâches à effectuer) ou de la visualiser pour y ajouter des commentaires.

### Données analytiques
Pour chaque test, Quant-UX compile automatiquement les informations pertinentes et permet de consulter les clics (heatmaps), les parcours utilisateurs, les tâches effectuées, les temps d'inactivité ou de scroll, les formulaires remplis, etc. Il est même possible de rejouer un enregistrement du test.
