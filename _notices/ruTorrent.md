---
nom: "ruTorrent"
date_creation: "Lundi, 18 septembre, 2017 - 06:12"
date_modification: "Lundi, 18 septembre, 2017 - 06:12"
logo:
    src: "images/logo/ruTorrent.png"
site_web: "https://github.com/Novik/ruTorrent"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "ruTorrent est une interface web pour rTorrent."
createurices: "davorb"
alternative_a: "µTorrent®, BitTorrent"
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "torrent"
    - "bittorrent"
    - "client bittorrent"
    - "web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rutorrent"
---

ruTorrent est une interface web permettant d’utiliser rTorrent qui lui, seul n'a pas d'interface graphique. Il est inspiré de l'interface de µTorrent. Le duo rTorrent - ruTorrent est souvent utilisé pour créer des seedbox. ruTorrent tient son nom de la contraction de rTorrent et µTorrent (µTorrent est souvent appelé uTorrent).
Il peut être amélioré par des plugins.

