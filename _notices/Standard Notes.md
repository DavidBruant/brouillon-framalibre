---
nom: "Standard Notes"
date_creation: "Dimanche, 23 avril, 2017 - 10:10"
date_modification: "Lundi, 4 décembre, 2017 - 11:14"
logo:
    src: "images/logo/Standard Notes.png"
site_web: "https://standardnotes.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "English"
description_courte: "Prise de notes simple avec synchronisation et chiffrement"
createurices: "Mo Bitar"
alternative_a: "Ulysses"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "prise de notes"
    - "markdown"
    - "synchronisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/standard-notes"
---

Standard Notes est un logiciel de prises de notes très simple dans l'esprit d'Ulysses sur MacOS.
Les notes sont sauvegardées et chiffrées automatiquement pour garantir la confidentialité de vos données. Elles sont synchronisées ensuite sur tous vos terminaux. Cela dit, on peut ne pas connecter l'extension et n'en avoir qu'un usage local.
Bien qu'étant en développement, Standard Notes est déjà très fonctionnel et paramétrable via des thèmes et des extensions. Une partie des extensions est subordonnée à un abonnement payant, mais les essentielles, comme l'éditeur MarkDown, sont en accès libre.
Standard Notes permet de se concentrer sur l'essentiel : la prise de notes. On retrouve très facilement ses notes via les tag ou le moteur de recherche intégré.

