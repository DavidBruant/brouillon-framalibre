---
nom: "CrystalDATA"
date_creation: "Jeudi, 2 novembre, 2023 - 15:11"
date_modification: "mercredi, 3 juillet, 2024 - 14:10"
logo:
    src: "images/logo/CrystalDATA.png"
site_web: "https://crystal-data.philnoug.com"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Vos données métier 'In da Cloud'"
createurices: "Philippe NOUGAILLON"
alternative_a: "Microsoft Access, Filemaker, Airtable"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "travail collaboratif"
    - "nocode"
    - "base de données"
    - "intelligence artificielle"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/crystaldata"
---

CrystalData est un gestionnaire d'informations NO-CODE qui vous permet de créer des collections d'objets métiers (ex: Clients, Interventions, Technicien, Frais, Projets, etc.), de les lier, et de les partager en ligne avec vos collaborateurs.

Un Assistant (IA) permet de poser une question en langage naturel, basée sur les données de vos collections d'objets, comme par exemple : "Quel est le client qui revient le plus souvent dans cette liste ?".

Principales fonctionnalités :

- Créez des Objets constitués d'attributs variés. Texte, Nombre, Euros, Date, Oui/Non, Liste de choix, Fichier, Image, PDF, Position GPS, Lien vers un Objet dans une autre Collection, QRCode, Météo, Etc.. )
- Ajoutez, éditez vos objets dans un formulaire
- Recherchez, filtrez et triez vos objets
- Partagez vos collections d'objets avec d'autres utilisateurs
- Exportez vos collections en CSV, XSL et PDF
- Soyez notifié après chaque ajout d'objet dans une de vos collections, ou quand un attribut change d'état
- Activité: chaque modification d'un objet est consignée dans un historique (quand, qui, quoi, valeur avant, valeur après)
- Accédez à toutes vos collections en mobilité (Les écrans s'adaptent automatiquement aux smartphones/tablettes)
- Assistant (IA). Posez une question à l'Assistant, basée sur les données de vos collections d'objets
