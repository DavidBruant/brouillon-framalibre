---
nom: "Castopod"
date_creation: "Dimanche, 18 octobre, 2020 - 20:30"
date_modification: "Lundi, 14 février, 2022 - 16:32"
logo:
    src: "images/logo/Castopod.png"
site_web: "https://castopod.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "La solution gratuite et open-source pour tous les podcasts"
createurices: "Yassine Doghri, Benjamin Bellamy"
alternative_a: "Anchor, Soundcloud, Spotify, Deezer"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "multimédia"
    - "podcast"
    - "hébergement"
    - "auto-hébergement"
    - "réseau social"
    - "audio"
    - "cms"
    - "activitypub"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/castopod"
---

Hébergez facilement vos podcasts, gardez le contrôle sur vos créations et dialoguez avec votre audience sans aucun intermédiaire.
Grâce à ActivityPub et au Fédiverse, le podcast est le réseau social.
Votre podcast et votre audience sont à vous et à vous uniquement.
Mesurez votre audience (norme IABv2) tout en respectant la vie privée de vos auditeurs.
Castopod gère également la plupart des fonctionnalités du podcast namespace de Podcast Index : GUID, Locked, Funding, Transcript, Chapters, Soundbite, Location, Person…
Enfin, Castopod s’installe en 5mn sur n'importe quelle infrastructure PHP/MySQL : dézippez le paquet, lancez l'assistant et vous êtes prêt à publier vos créations sur toutes les plateformes de podcasts !

