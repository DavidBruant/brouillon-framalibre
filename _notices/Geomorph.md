---
nom: "Geomorph"
date_creation: "Dimanche, 2 juillet, 2017 - 17:09"
date_modification: "Dimanche, 2 juillet, 2017 - 17:09"
logo:
    src: "images/logo/Geomorph.jpg"
site_web: "http://geomorph.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Français"
description_courte: "Générateur et éditeur d'images de relief."
createurices: "Patrice St-Gelais"
alternative_a: "Fracplanet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "2d"
    - "image"
    - "graphisme"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/geomorph"
---

Geomorph est un générateur et un éditeur d'images de relief (traduction Anglaise: "height maps") fonctionnant sous Linux et testé sur Ubuntu 14.04 LTS, 15.04 et 16.04 LTS.
Il permet de générer et éditer des images de relief avec divers outils et algorithmes, la partie droite de la fenêtre montre un aperçu en perspective des terrains correspondants, le rendu peut se faire avec Povray (logiciel d'illumination) et permet d'obtenir des paysages réalistes.
Une images de relief est une projection en 2 dimensions d'un terrain en 3 dimensions.

