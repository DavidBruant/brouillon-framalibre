---
nom: "Fossify Camera"
date_creation: "mercredi, 30 octobre, 2024 - 15:17"
date_modification: "mercredi, 30 octobre, 2024 - 15:17"
logo:
    src: "images/logo/Fossify Camera.png"
site_web: "https://github.com/FossifyOrg/Camera"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Prenez des photos et filmez librement avec cette belle application Fossify !"
createurices: "Fossify"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "caméra"
    - "appareil photo"
    - "photo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le collectif **Fossify** développe une gamme d'applications libres élégantes et fonctionnelles.
Voici leur application d'appareil photo qui fait tout ce qu'on attend d'une appli d'appareil photo !
Vous pouvez régler la qualité par défaut des photos, mettre un mode retardateur, choisir la dimension des photos, etc.
Des réglages et paramètres d'apparence sont disponibles, y compris la possibilité de choisir la couleur de l'icône (vert par défaut) de l'application !
