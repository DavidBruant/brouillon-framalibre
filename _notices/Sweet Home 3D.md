---
nom: "Sweet Home 3D"
date_creation: "Dimanche, 22 février, 2015 - 22:29"
date_modification: "Lundi, 1 mai, 2017 - 09:06"
logo:
    src: "images/logo/Sweet Home 3D.png"
site_web: "http://www.sweethome3d.com/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Logiciel très complet permettant de créer des maisons depuis la partie plan (2d) en 3d !"
createurices: "Emmanuel PUYBARET"
alternative_a: "Architecte 3D"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "maison"
    - "3d"
lien_wikipedia: "http://fr.wikipedia.org/wiki/Sweet_Home_3D"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/sweet-home-3d"
---

Logiciel très complet permettant de créer des maisons depuis la partie plan (2d) en 3d !
Grâce a un grand nombre d'objets (meubles) inclus dans le logiciel, il est facile d'utiliser ce logiciel.
La prise en main est rapide, grâce aux barres d'outils mais les réglages peuvent êtres plus poussés si nécessaire.
Écrit en java, il peut se lancer depuis plusieurs systèmes d'exploitation mais aussi en ligne !

