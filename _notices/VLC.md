---
nom: "VLC"
date_creation: "Mardi, 10 février, 2015 - 19:11"
date_modification: "Samedi, 26 janvier, 2019 - 11:47"
logo:
    src: "images/logo/VLC.png"
site_web: "https://www.videolan.org/vlc/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Windows Mobile"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "VLC est un lecteur multimédia très populaire."
createurices: ""
alternative_a: "Windows Media Player"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur vidéo"
    - "lecteur multimédia"
    - "lecteur musique"
    - "codecs"
    - "vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/VLC_media_player"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/vlc"
---

VLC est un lecteur multimédia très populaire. C'est un logiciel indispensable sur toute machine ! Il supporte un très grand nombre de formats vidéo et audio, et vous fera oublier tous vos problèmes de codecs si vous avez des fichiers aux formats « exotiques ».
Il peut également lire sur beaucoup de supports (fichiers, flux réseau, disque, etc.), mais aussi convertir des formats (encodage) et enregistrer du contenu. VLC gère aussi des listes de lecture, l'organisation de sa médiathèque, les sous-titres (et leurs synchronisations), et beaucoup d'autres fonctionnalités. C'est un vrai couteau suisse du multimédia.
Si son thème par défaut ne vous convient pas, vous pourrez en trouver de nombreux autres.

