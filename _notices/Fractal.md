---
nom: "Fractal"
date_creation: "jeudi, 13 juin, 2024 - 18:13"
date_modification: "jeudi, 13 juin, 2024 - 18:13"
logo:
    src: "images/logo/Fractal.png"
site_web: "https://gitlab.gnome.org/World/fractal"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Fractal est une application de messagerie Matrix."
createurices: ""
alternative_a: "Discord, Whatsapp, Telegram"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "matrix"
    - "GNOME"
    - "messagerie instantanée"
    - "chat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q59209813"
mis_en_avant: "non"

---

Fractal est un client de messagerie basée sur le protocole Matrix pour GNOME écrite en Rust. Son interface est optimisée pour la collaboration dans les grands groupes, tels que les projets de logiciels libres, et s'adapte à tous les écrans, grands ou petits.
