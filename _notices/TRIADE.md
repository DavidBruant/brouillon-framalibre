---
nom: "TRIADE"
date_creation: "samedi, 7 décembre, 2024 - 17:51"
date_modification: "samedi, 7 décembre, 2024 - 17:51"
logo:
    src: "images/logo/TRIADE.png"
site_web: "https://triade-educ.org/fr/"
plateformes:
    - "le web"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "TRIADE est une plate-forme pédagogique et administrative sous licence libre gratuite destinée aux établissements scolaires français ou étranger, intégrant en natif une plate-forme d'apprentissage en ligne"
createurices: "T.R.I.A.D.E."
alternative_a: "Pronote, Charlemagne/EcoleDirecte, Skolengo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "Espace numérique de travail (ENT)"
    - "education"
    - "travail collaboratif"
    - "école"
    - "collège"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

TRIADE est une plate-forme libre et gratuite dédiée à la gestion de la vie scolaire dans les établissements français et étrangers. Elle combine des fonctionnalités pédagogiques et administratives, incluant notamment une plate-forme d'apprentissage en ligne intégrée.

Ecrit en PHP, le logiciel est disponible à travers un simple navigateur internet pour les utilisateurs (pas besoin d'installer un logiciel supplémentaire).

Entièrement personnalisable, TRIADE peut s'adapter aux besoins spécifiques des établissements. Cet outil vise à simplifier la gestion, favoriser la collaboration entre les équipes et répondre aux enjeux organisationnels de manière sécurisée et intuitive.

TRIADE est gratuit de base et sous licence libre (GNU/GPL version 2), des option payantes (tels que l'envoi de SMS, la signature électronique, ou encore l'utilisation d'un assistant IA nommé TRIADE-COPILOT) sont disponibles.
