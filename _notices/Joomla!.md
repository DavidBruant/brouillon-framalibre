---
nom: "Joomla!"
date_creation: "Mercredi, 6 mai, 2020 - 01:14"
date_modification: "mardi, 10 septembre, 2024 - 18:08"
logo:
    src: "images/logo/Joomla!.png"
site_web: "https://www.joomla.org"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Création de site responsive, évolutif. Il est open source, robuste, sécurisé, scalable et polyvalent."
createurices: "Communauté mondiale de développeurs"
alternative_a: "wordpress, drupal"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "création de site web"
    - "gestionnaire de contenus"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Joomla!"
lien_exodus: ""
identifiant_wikidata: "Q13167"
mis_en_avant: "non"
redirect_from: "/content/joomla"
---

Joomla est le système de gestion de contenu (CMS) le plus utilisé après wordpress. Conçu par une communauté de bénévoles, Joomla ! offre une technologie web moderne entièrement gratuite !
Ses thèmes par défaut respectent les directives d'accessibilité du W3C (WCAG) 2.1 niveau AA.
Joomla est multilangue par défaut et propose plus de 70 packs de traduction prêts à l'emploi.
Joomla a la particularité d'avoir une édition à partir du front-end native, très rapide et facile, ce qui permet de gérer son contenu aux personnes peu à l'aise avec l'informatique. Avec la partie administration, vous pouvez profiter de l'édition par lot pour gérer plusieurs éléments en une seule fois.
Le template par défaut de Joomla est optimisé pour le référencement.
Les besoins de gestion sont traités avec la gestion des utilisateurs de Joomla. Organisez et contrôlez facilement les utilisateurs et les groupes du site.​

Plus de...
141 millions de téléchargements sur le site officiel
1500 volontaires
10 000 extensions et templates

Présentation en français: https://5.joomla.fr/
