---
nom: "Joplin"
date_creation: "dimanche, 31 décembre, 2023 - 07:25"
date_modification: "jeudi, 8 février, 2024 - 20:10"
logo:
    src: "images/logo/Joplin.svg"
site_web: "https://joplinapp.org/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Joplin est une application gratuite et open source de prise de notes et de tâches qui peut gérer un grand nombre de notes organisées en blocs."
createurices: "Laurent Cozic"
alternative_a: "Microsoft OneNote"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "prise de notes"
    - "todo-list"
    - "markdown"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Joplin_(logiciel)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/net.cozic.joplin/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

Joplin est une application gratuite et open source de prise de notes et de tâches qui peut gérer un grand nombre de notes organisées en blocs. Les notes sont recherchables, peuvent être copiées, marquées et modifiées soit à partir des applications directement ou à partir de votre propre éditeur de texte. Les notes sont au format Markdown.

Les notes exportées depuis Evernote peuvent être importées dans Joplin, y compris le contenu formaté (qui est converti en Markdown), les ressources (images, pièces jointes, etc.). et les métadonnées complètes (géolocalisation, temps mis à jour, temps créé, etc.). Les fichiers Markdown simples peuvent également être importés.

Joplin est "hors ligne d'abord", ce qui signifie que vous avez toujours toutes vos données sur votre téléphone ou votre ordinateur. Cela garantit que vos notes sont toujours accessibles, que vous ayez ou non une connexion Internet.

Les notes peuvent être synchronisées en toute sécurité synchronisées en utilisant le cryptage de bout en bout avec divers services cloud incluant Nextcloud, Dropbox, OneDrive et Joplin Cloud.

La recherche en texte intégral est disponible sur toutes les plateformes pour trouver rapidement les informations dont vous avez besoin. L'application peut être personnalisée en utilisant des plugins et des thèmes, et vous pouvez également créer facilement les vôtres.

L'application est disponible pour Windows, Linux, macOS, Android et iOS. Un Web Clipper, pour enregistrer des pages Web et des captures d'écran de votre navigateur, est également disponible pour Firefox et Chrome.
