---
nom: "QSpeakers"
date_creation: "Lundi, 24 février, 2020 - 20:45"
date_modification: "mercredi, 5 février, 2025 - 01:12"
logo:
    src: "images/logo/QSpeakers.png"
site_web: "http://brouits.free.fr/qspeakers/"
plateformes:
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel d'aide à la conception d'enceintes acoustiques avec haut-parleurs."
createurices: "Benoît Rouits"
alternative_a: "winisd, basta, hornresp, vituixcad"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "acoustique"
    - "simulation"
    - "diy"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qspeakers"
---

QSpeakers est un logiciel graphique simple qui simule la réponse en fréquence d'enceintes acoustiques de type clos, bass-reflex ou caisson symétrique, en se basant sur les paramètres électromécaniques du haut-parleur choisi, à savoir ses paramètres de Thiele & Small. Ce logiciel est utilisé pour le do-it-yourself (fabriquer soi-même), pour l'enseignement de l'électro-acoustique, et dans une moindre mesure l'ingénierie des haut-parleurs.
