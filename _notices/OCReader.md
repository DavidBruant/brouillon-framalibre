---
nom: "OCReader"
date_creation: "Samedi, 25 mars, 2017 - 14:44"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/OCReader.png"
site_web: "https://github.com/schaal/ocreader"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Client Android pour l'application Nextcloud News."
createurices: "Daniel Schaal"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "lecteur de flux rss"
    - "synchronisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ocreader"
---

OCReader est un lecteur de flux RSS, à synchroniser avec l'application Nexctcloud News.
L'application permet d'ajouter, classer et supprimer des flux. Elle possède une catégorie « articles récents » recensant les articles de moins de 24h.

