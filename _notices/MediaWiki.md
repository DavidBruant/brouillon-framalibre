---
nom: "MediaWiki"
date_creation: "Mercredi, 8 mars, 2017 - 00:14"
date_modification: "Mercredi, 3 octobre, 2018 - 12:50"
logo:
    src: "images/logo/MediaWiki.jpg"
site_web: "https://www.mediawiki.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Windows Mobile"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "MediaWiki, un moteur de Wiki robuste et éprouvé."
createurices: "Magnus Manske"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "wiki"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MediaWiki"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/mediawiki"
---

MediaWiki est un moteur de Wiki robuste et éprouvé utilisé par la fondation Wikimédia pour concevoir et publier leurs pages documentaires. L'esprit collaboratif est l'un des socles de l'application.
Développé en PHP sur une base de données de type MySQL/MariaDB ou PostgreSQL, le moteur s'installe facilement avec une documentation claire et précise.

