---
nom: "Paperless NGX"
date_creation: "lundi, 26 août, 2024 - 11:08"
date_modification: "lundi, 26 août, 2024 - 11:08"
logo:
    src: "images/logo/Paperless NGX.svg"
site_web: "https://paperless-ngx.com/"
plateformes:
    - "le web"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Paperless-ngx est un gestionaire de documents qui transforme vos papier physiques en copie numerique navigable en ligne"
createurices: "Paperless NGX"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "document"
    - "gestion"
    - "archives"
    - "numerisation"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/en/reports/de.astubenbord.paperless_mobile/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

- Organiser et indexer vos documents scanne avec des tags, correspondants, types, ...
- Toutes vos données sont stockées sur votre serveur et ne sont jamais partagées
- Utilise de la reconnaissance de caractères pour permettre la recherche de texte dans vos documents, y compris ceux scandés.
- Reconnais plus de 100 langues différentes.
- Les documents sont stockés sous forme de PDF/A pour le stockage à long terme, de même que la copie originale.
- Utilise de l'apprentissage automatique pour ajouter des tags, correspondants et types a vos documents.
- Supporte les PDF, image, texte, documents office (Word, Excel, PowerPoint et équivalent LibreOffice), ...
- Garde les documents directement dans le système de fichier.
- Une application web pleine de fonctionnalités
- Import de documents directement depuis vos comptes e-mail.
