---
nom: "Illico Editor"
date_creation: "Vendredi, 1 avril, 2016 - 19:12"
date_modification: "vendredi, 24 janvier, 2025 - 22:06"
logo:
    src: "images/logo/Illico Editor.png"
site_web: "http://illico.ti-nuage.fr"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Couteau-suisse pour qualifier les données sans effort"
createurices: "Arnaud Coche"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "donnée"
    - "nettoyage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/illico-editor"
---

Un couteau-suisse de la qualification de données dédié :

- aux acteurs métiers : services administratifs, etc.
- aux acteurs techniques : développeurs, administrateurs de BD, etc.

lorsqu’il s’agit d’explorer des données, de les vérifier, de transformer leur format pour mieux les exploiter, analyser, valoriser et de les corriger en masse.

Illico Editor possède de très nombreuses fonctionnalités classiques ou complexes :

- analyse de données (répartition des valeurs, valeurs non-saisies)
- croisement de fichiers (comparaison, enrichissement, exclusion)
- correction en masse (pivot, syntaxe, condition, etc.)
- recherche et filtre (n-ième occurrence, doublon, isoler/exclure une cohorte, échantillonner)
- restitution (enchaînements de tableaux croisé-dynamiques, tableaux de synthèse)
