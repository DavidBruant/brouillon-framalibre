---
nom: "framabin"
date_creation: "Vendredi, 7 décembre, 2018 - 12:15"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/framabin.png"
site_web: "https://framabin.org/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Parce que l'ignorance c'est le bonheur"
createurices: "Sébastien Sauvage"
alternative_a: "Pastebin"
licences:
    - "Licence Zlib"
tags:
    - "privatebin"
    - "pastebin"
    - "bin"
    - "texte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Liste_des_produits_et_services_de_Framasoft#Framab…"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/framabin"
---

framabin est  un service de partage de texte en ligne sécurisé.
Les données stockées sur le serveur sont chiffrées puisque le déchiffrement a lieu dans le navigateur grâce à un algorithme AES 256 bits.
Framabin permet de paramétrer plusieurs options pour les textes envoyés :
Une date d'expiration
La coloration syntaxique
Une protection par mot de passe
L'effacement après lecture
Les commentaires
Ce service reprend le code source de ZeroBin de SebSauvage et a été l'oeuvre de nombreux contributeurs.

