---
nom: "WhosWho"
date_creation: "Mercredi, 29 juin, 2022 - 17:13"
date_modification: "Mercredi, 29 juin, 2022 - 17:13"
logo:
    src: "images/logo/WhosWho.png"
site_web: "https://framagit.org/Yvan-Masson/WhosWho/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Crée des documents PDF avec les photos des personnes et le nom correspondant, également appelés trombinoscopes"
createurices: "Yvan Masson"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/whoswho"
---

À partir d'un ensemble de photos et de noms, WhosWho crée un document PDF avec les visages et les noms correspondants, également appelé trombinoscope, comme cela est utilisé dans les écoles. WhosWho essaie de rendre les choses aussi simples que possible pour l'utilisateur :
Les photos peuvent être automatiquement recadrées pour ne garder que le visage de la personne
Si vous possédez un fichier ODS/XLSX contenant les noms des personnes, exportez-le simplement au format CSV et WhosWho pourra l'utiliser
Une photo par défaut peut être utilisée si vous n'avez pas la photo d'une personne
Plusieurs dispositions de page sont disponibles (A4, A3, portrait, paysage…), chacune en 150 et en 300 PPP

