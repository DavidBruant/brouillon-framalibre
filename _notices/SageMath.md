---
nom: "SageMath"
date_creation: "Jeudi, 19 octobre, 2017 - 13:26"
date_modification: "Mercredi, 25 octobre, 2017 - 11:59"
logo:
    src: "images/logo/SageMath.png"
site_web: "http://www.sagemath.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "SageMath est un logiciel libre de calcul mathématique."
createurices: "William Stein, et al."
alternative_a: "Maple, Mathematica, Magma, Matlab"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "mathématiques"
    - "calcul formel"
    - "calcul numérique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/SageMath"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sagemath"
---

SageMath est un logiciel libre de calcul mathématique. Construit sur un assemblage de nombreux logiciels et bibliothèques libres de calcul mathématique, il permet d'accéder de manière unifiée à de très nombreuses fonctionnalités de calcul. Les domaines couverts incluent l'algèbre et l'algèbre linéaire, l'analyse, la géométrie et la topologie, les probabilités et les statistiques, les graphes et les mathématiques discrètes, la théorie des nombres, la géométrie algébrique, les codes correcteurs d'erreur, la cryptographie, etc.
Le langage de programmation utilisé par l'interface est le langage Python.

