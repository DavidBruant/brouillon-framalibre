---
nom: "Radinus"
date_creation: "mercredi, 14 août, 2024 - 11:56"
date_modification: "mercredi, 14 août, 2024 - 11:56"
logo:
    src: "images/logo/Radinus.png"
site_web: "https://www.radinus.fr"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Radinus est un logiciel de gestion de finances personnelles qui permet de suivre ses dépenses, de créer des budgets, et d'analyser ses finances à l'aide de statistiques détaillées pour une meilleure maîtrise financière."
createurices: "Fabrice Garcia"
alternative_a: "gnucash, grisbi"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "comptabilité personnelle"
    - "gestion finances personnelles"
    - "assistant finances personnelles"
    - "comptabilité"
    - "Finances"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Radinus est un logiciel de gestion de finances personnelles gratuit et open source conçu pour vous aider à mieux maîtriser vos finances. Il permet de suivre et de catégoriser vos dépenses, de créer des budgets personnalisés, et de gérer différents types de transactions, qu'elles soient déjà réalisées, engagées, ou prévues. Radinus offre également la possibilité d'affecter chaque opération à des catégories spécifiques telles que "Alimentation" ou "Bricolage", et même à des personnes de votre foyer, comme "Monsieur" ou "Enfant". De plus, il vous permet de définir des postes budgétaires regroupant plusieurs catégories, et de contrôler votre budget grâce à des statistiques détaillées qui analysent vos dépenses. Vous pouvez ainsi visualiser les écarts entre vos prévisions et vos dépenses réelles, et ajuster vos finances en conséquence pour une gestion plus efficace. Un tableau de bord présentant une synthèse graphique de vos transactions bancaire est aussi disponible.
