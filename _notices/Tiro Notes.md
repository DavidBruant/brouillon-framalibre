---
nom: "Tiro Notes"
date_creation: "Lundi, 4 avril, 2022 - 14:24"
date_modification: "samedi, 31 août, 2024 - 18:06"
logo:
    src: "images/logo/Tiro Notes.png"
site_web: "https://framagit.org/dotgreg/tiro-notes"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Logiciel de markdown extensible disponible sur linux, mac, windows, android et web"
createurices: "dotgreg"
alternative_a: "Evernote, OneNote, Google Keep"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "bureautique"
    - "markdown"
    - "prise de notes"
    - "notes"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tiro-notes"
---

Tiro Notes est conçu pour être une alternative open source aux logiciels propriétaires comme Notion, Evernote, OneNote, etc.
👼 Open source & gratuit, les données sont totalement en markdown, pas de base de données cachées. Permet d'utiliser n'importe quel système de synchronisation.
🏋 Extensible : possibilité d'étendre les fonctionnalités avec un système de tags customs comme [[calendar]], [[latex]] ou [[uml]].
☁️ / 🖥️ / 💻 / 📱 : Applications pour Windows, Mac, Linux. Ligne de commande avec npx. Installable sur Android avec Termux. Peut être utilisé comme application web.
🚅 Rapide : recherche rapide (en utilisant ripgrep). Vous pouvez faire défiler la liste de plusieurs milliers d'articles sans ralentissement.
