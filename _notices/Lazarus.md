---
nom: "Lazarus"
date_creation: "Mardi, 17 janvier, 2017 - 18:02"
date_modification: "Jeudi, 30 mars, 2017 - 13:18"
logo:
    src: "images/logo/Lazarus.png"
site_web: "http://www.lazarus-ide.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Lazarus : l'IDE multi-plateformes pour le développement rapide d'application"
createurices: "Lazarus Project"
alternative_a: "WinDev, Borland, Delphi, Microsoft VisualStudio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "ide"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Lazarus"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lazarus"
---

Lazarus peut remplacer WinDev pour le développement rapide d'applications en FreePascal et Pascal Objet sur différentes plateformes; il se veut compatible avec les anciens projets écrits en Delphi.
Il est possible d'ajouter plusieurs ajouts appelés "Widjets" dont notamment :
GTK 1: Support complet, utilisé par les anciennes applications Gnome sous Unix, GIMP 1.x sous diverses plateformes. Il est aujourd'hui considéré comme obsolète.
GTK 2: Support complet, version actuelle de GTK utilisé par Gnome 2, GIMP 2.x et bien d'autres. Utilisation conseillée pour une bonne intégration sous Gnome, XFCE et autres ou pour des applications multiplateformes (Windows, Mac, Unix).
GTK 3: Support expérimental en cours de développement.
Qt : Fondé sur la version 4.5 ou supérieure de Qt, le support complet. Ce widgetset est utilisé par KDE 4 et beaucoup d'autres sous Unix. Utilisation conseillée pour une bonne intégration dans KDE et pour les applications multiplateformes. Et d'autres...

