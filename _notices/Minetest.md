---
nom: "Minetest"
date_creation: "Mercredi, 4 janvier, 2017 - 15:31"
date_modification: "samedi, 6 janvier, 2024 - 02:46"
logo:
    src: "images/logo/Minetest.png"
site_web: "https://www.minetest.net/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un jeu de cubes dans un espace illimité ou pioches et pelles sont de rigueur pour édifier des constructions..."
createurices: "Perttu Ahola"
alternative_a: "Minecraft"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Minetest"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/net.minetest.minetest/latest/"
identifiant_wikidata: "Q722334"
mis_en_avant: "oui"
redirect_from: "/content/minetest"
---

Un jeu de cubes dans un espace illimité ou pioches et pelles sont de rigueur pour édifier des constructions imaginaires, creuser des tunels, se promener au grès de ses envies, faire des rencontres, ramasser quelques fleurs de ci de la, nager, ...
Minetest c'est l'équivalent de son petit frère Minecraft avec en supplément la gratuité du jeu et la possibilité d'intégrer une équipe de développement pour améliorer le produit.
Minetest, c'est aussi un serveur qui permet d'accueillir vos amis ou simplement quelques visiteurs curieux voire même des joueurs chevronnés.
L'installation est simple et le jeu est entièrement traduit en français.
