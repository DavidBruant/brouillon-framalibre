---
nom: "Bacula"
date_creation: "Samedi, 15 août, 2020 - 22:34"
date_modification: "Samedi, 15 août, 2020 - 22:39"
logo:
    src: "images/logo/Bacula.png"
site_web: "https://www.bacula.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Logiciel de sauvegarde et de restauration construit comme un système de sauvegarde natif basé sur Linux."
createurices: "Rob Morrison"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "système"
    - "sauvegarde"
    - "restauration"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bacula"
---

L'adéquation de Linux en tant que système d'exploitation principal pour le développement et l'utilisation d'une solution de sauvegarde d'entreprise en a fait le système d'exploitation de base de choix pour Bacula.
Bacula est une solution agile prête à l'emploi, capable à la fois de sauvegarder et de restaurer des données dans les plus brefs délais. Bacula a la réputation d'être un logiciel de sauvegarde Linux extrêmement flexible, sécurisé et, à ce jour, a eu plus de téléchargements que tout autre logiciel de sauvegarde Linux open source.
Il prend en charge une grande variété de types de sauvegarde - de la sauvegarde physique à la sauvegarde virtuelle en passant par la sauvegarde cloud prenant en charge Amazon S3. Il offre également une flexibilité supplémentaire du côté du stockage, s'intégrant aux disques, aux baies et à une large gamme de lecteurs de bande.

