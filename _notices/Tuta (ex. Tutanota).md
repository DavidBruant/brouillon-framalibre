---
nom: "Tuta (ex. Tutanota)"
date_creation: "Lundi, 8 avril, 2019 - 14:47"
date_modification: "lundi, 11 mars, 2024 - 11:56"
logo:
    src: "images/logo/Tutanota.png"
site_web: "https://tutanota.com/fr"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un service e-mail sécurisé et respectueux de votre vie privée."
createurices: ""
alternative_a: "Gmail, Hotmail"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "vie privée"
    - "sécurité"
    - "mail"
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tutanota"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/65415/"
identifiant_wikidata: "Q21659489"
mis_en_avant: "oui"
redirect_from: "/content/tutanota"
---

Tutanota est un fournisseur de boîte mail ayant son propre client en ligne (pour votre navigateur) et pour votre téléphone intelligent (Android ou iOS). L'offre de base est gratuite et des offres premium sont disponibles.

À noter que pour garantir le chiffrement de bout en bout des données, Tutanota ne permet pas l'utilisation du protocole standard IMAP. Vous devrez donc utiliser les clients Tutanota (disponibles pour Windows et Linux si vous ne voulez pas utiliser l'application en ligne) et vous ne pourrez pas utiliser de client comme Thunderbird. L'application Android est disponible dans la bibliothèque F-Droid.
