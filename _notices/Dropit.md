---
nom: "Dropit"
date_creation: "Jeudi, 19 novembre, 2020 - 13:00"
date_modification: "Lundi, 10 mai, 2021 - 14:36"
logo:
    src: "images/logo/Dropit.png"
site_web: "http://www.dropitproject.com/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Lorsque vous avez besoin d'organiser des fichiers, DropIt peut éliminer une grande partie de la corvée de rech"
createurices: ""
alternative_a: "File Juggler, Hazel"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gestionnaire de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dropit"
---

Vous pouvez configurer DropIt pour effectuer 17 actions différentes sur vos fichiers et dossiers, en filtrant les fichiers par nom, répertoire, taille, date, propriétés, contenu ou expressions régulières. Vous pouvez même enregistrer des ensembles d'associations dans des profils et associer un profil à chaque dossier souhaité, pour analyser les dossiers surveillés à un intervalle de temps défini.
Déposez un groupe de différents fichiers et dossiers sur l'image DropIt flottante et il les trie dans des dossiers de destination définis, les compresse ou les extrait, les ouvre avec les programmes associés ou effectue d'autres actions définies.

