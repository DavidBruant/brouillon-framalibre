---
nom: "dato"
date_creation: "Lundi, 20 septembre, 2021 - 16:28"
date_modification: "Mercredi, 29 septembre, 2021 - 14:42"
logo:
    src: "images/logo/dato.png"
site_web: "https://squeak.eauchat.org/apps/dato/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "English"
description_courte: "Créer et gérer vos bases de données facilement, dans votre navigateur Web"
createurices: "squeak"
alternative_a: "Microsoft Access, Filemaker"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dato"
---

dato est un gestionnaire de bases de données auto-hébergeable, très personnalisable, permettant de structurer tout type d'information.
L'utilisateur crée des "types", définis par un ensemble de "clés" pouvant stocker des données de différentes natures. Par exemple, un type "contact" pourra contenir les clés "nom" et "prénom" (clés textuelles), une clé "image", ou encore une clé "birthday" (de format date). Par ailleurs, l'utilisateur crée des bases, chaque base pouvant accueillir des entrées de un ou plusieurs types.
Les vues permettant d'explorer les données sont filtrables et personnalisables. Par exemple, au sein d'une base, les entrées peuvent être présentées sous forme de liste, de mosaïque, ou être localisée sur une carte. La visualisation des entrées peut également être personnalisée.
dato regorge de nombreuses fonctionnalités. Il peut être utilisé hors ligne. La synchronisation sur plusieurs périphériques nécessite l'utilisation d'un serveur couchdb.

