---
nom: "ForColormap"
date_creation: "mardi, 12 mars, 2024 - 19:01"
date_modification: "mardi, 12 mars, 2024 - 19:01"
logo:
    src: "images/logo/ForColormap.svg"
site_web: "https://github.com/vmagnin/forcolormap"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "English"
description_courte: "La bibliothèque Fortran ForColormap permet de convertir une valeur réelle en valeurs RVB que vous pouvez utiliser avec n'importe quelle bibliothèque graphique."
createurices: "Vincent Magnin & Seyed Ali Ghasemi"
alternative_a: ""
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "carte thermique"
    - "palette de couleurs"
    - "Fortran"
    - "colormap"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

La bibliothèque Fortran ForColormap permet de convertir une valeur réelle en valeurs RVB que vous pouvez utiliser avec n'importe quelle bibliothèque graphique.

Elle comprend en particulier :
- les 222 palettes de couleurs de la collection Scientific colour maps v8.0.1 de Fabio Crameri,
- les palettes matplotlib "magma", "inferno", "plasma", "viridis",
- et la palette cubehelix de Dave Green.

ForColormap propose également diverses méthodes et options pour gérer et manipuler ces palettes de couleurs.

