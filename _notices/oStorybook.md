---
nom: "oStorybook"
date_creation: "Samedi, 4 novembre, 2017 - 19:49"
date_modification: "mercredi, 23 octobre, 2024 - 18:54"
logo:
    src: "images/logo/oStorybook.png"
site_web: "http://ostorybook.eu/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pour les écrivains, essayistes, romanciers, etc..."
createurices: "Martin Munstun, FaVdB"
alternative_a: "Scrivener"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "nouvelle"
    - "roman"
    - "écriture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ostorybook"
---

oStorybook est un Logiciel Libre pour les écrivains, essayistes, auteurs. Utilisé de l'ébauche jusqu'à l'ouvrage final, oStorybook vous permet de ne jamais perdre de vue la trame de l'histoire. oStorybook vous aide à conserver la haute main sur les différents développements de votre récit.
