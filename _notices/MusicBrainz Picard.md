---
nom: "MusicBrainz Picard"
date_creation: "Mercredi, 5 septembre, 2018 - 15:20"
date_modification: "Mercredi, 5 septembre, 2018 - 15:20"
logo:
    src: "images/logo/MusicBrainz Picard.png"
site_web: "https://picard.musicbrainz.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
langues:
    - "English"
description_courte: "Un logiciel d'identification de fichiers audio."
createurices: ""
alternative_a: "MP3tag, The GodFather"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "musique"
    - "bibliothèque musicale"
    - "métadonnées"
lien_wikipedia: "https://en.wikipedia.org/wiki/MusicBrainz_Picard"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/musicbrainz-picard"
---

MusicBrainz Picard est un logiciel d'identification et de marquage de fichiers audio.
L'identification se fait en comparant les caractéristiques sonores du fichier (empreintes acoustiques) avec la base de données MusicBrainz Database. Les métadonnées manquantes ou erronées sont alors ajoutées.
De nombreux formats de fichiers sont pris en charge : flac, wav, mp3, ogg vorbis, wma, etc.
Le logiciel présente également l'intérêt de pouvoir traiter l'intégralité de la bibliothèque musicale d'un seul coup.

