---
nom: "GNU Backgammon"
date_creation: "Mardi, 10 janvier, 2017 - 02:45"
date_modification: "Mercredi, 12 mai, 2021 - 16:54"
logo:
    src: "images/logo/GNU Backgammon.png"
site_web: "http://gnubg.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Jouez au backgammon, avec analyses et conseils à disposition."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
    - "stratégie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnu-backgammon"
---

GNU Backgammon permet de jouer à deux, ou seul contre l'ordinateur. Le logiciel propose analyses statistiques et conseils, d'enregistrer des parties et même de revenir en arrière. Mais ce n'est pas pour autant que vous l'emporterez toujours. Il participe même à des tournois et il n'est pas mauvais !

