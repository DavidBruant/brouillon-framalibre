---
nom: "Logidownload"
date_creation: "mardi, 15 octobre, 2024 - 17:31"
date_modification: "mardi, 15 octobre, 2024 - 17:31"
logo:
    src: "images/logo/Logidownload.png"
site_web: "https://ladigitale.dev/logidownload"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logidownload est un logiciel de téléchargement de vidéos et de fichiers audio en ligne basé sur node-ytdl-core"
createurices: "Emmanuel ZIMMERT // ez[at]ladigitale.dev"
alternative_a: "yt-dlp"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "téléchargement"
    - "téléchargement de youtube"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Logidownload permet de télécharger des vidéos et des fichiers audio à partir de YouTube, Dailymotion, Vimeo, PeerTube, Bandcamp, Soundcloud et de nombreux autres sites.

Il repose sur le logiciel libre yt-dlp pour proposer une interface graphique intuitive et simple : il suffit de coller le lien de la page de la vidéo à télécharger et de cliquer sur le bouton Télécharger.
