---
nom: "Carnet"
date_creation: "Mercredi, 10 avril, 2019 - 13:43"
date_modification: "Mercredi, 10 avril, 2019 - 13:43"
logo:
    src: "images/logo/Carnet.png"
site_web: "https://framagit.org/PhieF/CarnetDocumentation"
plateformes:
    - "GNU/Linux"
    - "Android"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Application de prise de notes rapides : texte, images, li"
createurices: "Phie"
alternative_a: "Google Keep"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "prise de notes"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/69667/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/carnet"
---

Application de prise de notes rapides : texte, images, liens
Possibilité de changer la couleur de fond, mettre des étiquettes, liste à cocher ("todo list").
Environnements : NextCloud, Linux, Android + une version en ligne.

