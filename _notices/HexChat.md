---
nom: "HexChat"
date_creation: "Jeudi, 29 décembre, 2016 - 23:35"
date_modification: "Mercredi, 12 mai, 2021 - 16:15"
logo:
    src: "images/logo/HexChat.png"
site_web: "https://hexchat.github.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Chattez (clavardez) sur IRC avec un client multiplateforme et personnalisable."
createurices: "Berke Viktor (fondateur)"
alternative_a: "Xchat, mIRC"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "chat"
    - "irc"
    - "messagerie instantanée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/HexChat"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/hexchat"
---

Hexchat est un client de messagerie instantanée permettant de se connecter sur des salons IRC (Internet Relay Chat). Il supporte l'échange de fichiers en utilisant le protocole DCC (Direct Client-to-Client) et permet de se connecter en sécurité grâce au protocole SSL (Secure Sockets Layer). Hexchat est personnalisable, qu'il s'agisse de scripts (des plugins prêts à l'emploi sont disponibles depuis la documentation sur le site officiel) ou de thèmes de couleur (disponibles sur le site officiel).

