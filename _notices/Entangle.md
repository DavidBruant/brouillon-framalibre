---
nom: "Entangle"
date_creation: "Vendredi, 10 mars, 2017 - 23:07"
date_modification: "Mercredi, 18 octobre, 2023 - 11:19"
logo:
    src: "images/logo/Entangle.png"
site_web: "https://entangle-photo.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "English"
description_courte: "Outil génial pour le contrôle de son appareil photo à distance et piloter la prise de vue."
createurices: "Daniel P. Berrangé"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "photo"
    - "contrôle à distance"
    - "animation"
    - "stopmotion"
    - "créativité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Entangle"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/entangle"
---

Brancher l'appareil photo à l'ordinateur, en général par câble USB, et tout le reste est géré depuis l'interface d'Entangle : mise au point, choix des options (ISO, ouverture...) et déclenchement de la prise de vue. L'interface affiche la vue temps réel du viseur.
Très utile pour des captures sans risquer de toucher l'appareil photo, notamment pour la photo astronomique, ou l'animation en volume (stop motion).
D'autres logiciels comme GTKam et DigiKam implémente aussi cette fonctionnalité. Mais Entangle ne fait que cette tâche et le fait très bien en toute simplicité.
Entangle fonctionne partout où un environnement graphique GTK3 a été porté et dont la bibliothèque libgphoto est disponible. Le logiciel est stable et activement développé.

