---
nom: "Compute Web App"
date_creation: "dimanche, 4 août, 2024 - 15:32"
date_modification: "jeudi, 15 août, 2024 - 11:01"
logo:
    src: "images/logo/Compte Web App.png"
site_web: "https://soqut-imaging.fr/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Compute Web App (ou CWA) est une application logicielle web conçue pour distribuer et exécuter des traitements de données intensifs en parallèle sur plusieurs machines."
createurices: "Bruno Martin"
alternative_a: "Hadoop & Spark"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "Traitement de données"
    - "Calcul scientifique"
    - "Calcul distribué"
    - "Application web"
    - "RESTful API"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Compute Web App (ou CWA) est une application logicielle web conçue pour distribuer et exécuter des traitements de données intensifs en parallèle sur plusieurs machines. Chaque instance s’occupe d’une partie des données d’entrée et CWA peut ainsi adresser des traitement de données intensifs sur des tailles pouvant atteindre quelques pétaoctets (1000 téraoctets).
