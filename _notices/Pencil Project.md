---
nom: "Pencil Project"
date_creation: "Lundi, 24 avril, 2017 - 14:43"
date_modification: "Mardi, 4 juin, 2019 - 21:21"
logo:
    src: "images/logo/Pencil Project.png"
site_web: "http://pencil.evolus.vn"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Pencil Project sera utilisé pour prototyper des interfaces et créer des diagrammes."
createurices: ""
alternative_a: "axure, balsamiq, Adobe XD"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "prototypage"
    - "diagramme"
    - "ux design"
    - "maquette"
    - "web"
    - "internet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pencil"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pencil-project"
---

L'application permet de choisir dans des bibliothèques de «stencils» comportant des boutons, des éléments d'interfaces qui permettent de composer rapidement l'aperçu d'une interface pour une application ou un site web. On pourra aussi utiliser cette application pour créer des diagrammes et des organigrammes.
L'application peut aussi aller chercher des éléments graphiques (par exemple un aperçu vide de tablette) sur openclipart.
On peut créer un ensemble de design d'écran pour un même projet en ajoutant des interaction qui permettront de créer un premier prototype interactif.
L'export se fera aux formats html, pdf ou encore images (svg ou png) et même au format odt.

