---
nom: "Toolkit Atlas"
date_creation: "Mercredi, 14 septembre, 2022 - 09:23"
date_modification: "Mercredi, 14 septembre, 2022 - 09:23"
logo:
    src: "images/logo/Toolkit Atlas.png"
site_web: "https://atlastk.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "English"
description_courte: "Ajouter facilement et rapidement une interface graphique à vos programmes."
createurices: "Claude SIMON"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "développement"
    - "web"
    - "java"
    - "python"
    - "ruby"
    - "réseau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/toolkit-atlas"
---

Le toolkit Atlas est une bibliothèque logicielle multi-langage (Java, Node.js, Perl, Python et Ruby), ultra-légère et sans dépendances pour ajouter facilement une interface graphique et des fonctionnalités réseau à vos applications.

