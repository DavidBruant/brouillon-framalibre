---
nom: "Neovim"
date_creation: "Vendredi, 6 janvier, 2017 - 16:10"
date_modification: "Mercredi, 22 mars, 2017 - 21:15"
logo:
    src: "images/logo/Neovim.png"
site_web: "https://neovim.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Le futur de Vim, littéralement."
createurices: ""
alternative_a: "Sublime Text, Microsoft VisualStudio, Notepad, PyCharm, UltraEdit"
licences:
    - "Licence Apache (Apache)"
tags:
    - "développement"
    - "traitement de texte"
    - "texte"
    - "éditeur"
    - "environnement de développement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/neovim"
---

Neovim étend les fonctionnalités de Vim et peut-être utilisé comme remplacement direct de ce dernier.
Il dispose notamment :
- D'une API asynchrone pour le développement d'extensions
- De valeurs par défaut adaptées aux GUI/TUI modernes (activation des couleurs, de la souris)
De plus il peut être employé comme composant dans des UI complètes ce qui permet la création d'éditeurs de texte ou d'IDEs avancés comme Neovim-qt, NyaoVim ou SolidOak

