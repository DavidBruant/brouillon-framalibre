---
nom: "Répartiion"
date_creation: "dimanche, 31 mars, 2024 - 08:59"
date_modification: "dimanche, 31 mars, 2024 - 08:59"
logo:
    src: "images/logo/Répartiion.svg"
site_web: "https://educajou.forge.apps.education.fr/repartition/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un outil de visualisation et d’organisation pour les répartitions de classes à l’école primaire."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "répartition"
    - "direction"
    - "école"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Répartition est un outil de visualisation et d’organisation pour les répartitions de classes à l’école primaire.

Elle vous propose une première ventilation automatique, qui peut ne pas correspondre à une réalité envisageable, mais vous permet d’avoir une base, à partir de laquelle vous n'avez plus qu’à déplacer des élèves.
Vous récupérez ensuite votre répartition au format tableur (CSV).

En quelques clics ...
- Renseignez vos effectifs dans les encadrés correspondants.
- Ajustez si nécessaire les paramètres.
- Observez la répartition proposée, et réajustez éventuellement vos paramètres.
- Cliquez sur “Déplacer des élèves”.
- Faites les ajustements nécessaires au moyen des flèches marron (en faisant “monter” ou “descendre” des élèves).
- Téléchargez vos effectifs en CSV (format utilisable avec un tableur).
