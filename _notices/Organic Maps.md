---
nom: "Organic Maps"
date_creation: "mardi, 17 octobre, 2023 - 18:14"
date_modification: "dimanche, 7 janvier, 2024 - 19:46"
logo:
    src: "images/logo/Organic Maps.svg"
site_web: "https://organicmaps.app/fr/"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Application libre de cartes hors ligne et navigation GPS basée sur les données OpenStreetMap - Pour Android et iOS"
createurices: ""
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "cartographie"
    - "gps"
    - "OpenStreetMap"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Organic_Maps"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/app.organicmaps/latest/"
identifiant_wikidata: "Q107078602"
mis_en_avant: "oui"

---

Organic Maps est une application de navigation respectueuse de la vie privée, destinée aux conducteurs, aux randonneurs et aux cyclistes. Il n'y a pas de suivi de localisation, pas de collecte de données et pas de publicité, vous pouvez ainsi contrôler vos données. La recherche, le guidage et la navigation s'effectuent sans signal de téléphone portable, ce qui est idéal pour les sentiers de randonnée éloignés ou pour les endroits où les connexions sont insuffisantes.

Organic Maps utilise les données d'OpenStreetMap, dont les contributeurs viennent du monde entier. Le projet est géré par la communauté, le code est ouvert et la priorité est donnée au développement de la communauté et à la collaboration.

- Aucun signal cellulaire requis
- Recherche et itinéraire sans signal
- Utilisation efficace de la batterie
- Réduction de la consommation de la batterie
- Recherche rapide
- Trouver des lieux rapidement
