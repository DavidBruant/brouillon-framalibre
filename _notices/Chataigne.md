---
nom: "Chataigne"
date_creation: "Jeudi, 21 septembre, 2023 - 13:40"
date_modification: "jeudi, 17 octobre, 2024 - 12:44"
logo:
    src: "images/logo/Chataigne.jpg"
site_web: "https://benjamin.kuperberg.fr/chataigne/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Logiciel d'orchestration audio et vidéo pour les arts scéniques, installations ou performances."
createurices: "Benjamin Kuperberg"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "live"
    - "création multimédia"
    - "son"
    - "logiciel de gestion de vidéos"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/chataigne"
---

Ce logiciel ne permet pas en lui même de diffuser, ni d'éditer ou créer des vidéos ou des sons. Il se charge plutôt d'orchestrer d'autres logiciels ou machines qui s'en chargeront. Pour cela il est compatible avec de nombreux protocoles et API de logiciels, ainsi que des signaux d'entrés variés (clavier, MIDI, wiimote, arduino...).
Une fois que l'on a connecté ses entrées et ses sorties, il faut définir les règles d'interaction. Cela peut se faire à l'aide  d'états variables, mais aussi avec des séquences (timelines).
Un point important est que son créateur, Benjamin Kuperberg, est français et très proche de sa communauté hyper spécialisée (régisseurs vidéo dans le théâtre par exemple). Il encourage à le contacter et à organiser des ateliers autour du logiciel.
