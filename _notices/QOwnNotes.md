---
nom: "QOwnNotes"
date_creation: "Lundi, 3 juillet, 2017 - 23:00"
date_modification: "Lundi, 10 mai, 2021 - 14:56"
logo:
    src: "images/logo/QOwnNotes.png"
site_web: "http://www.qownnotes.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Gérez vos notes en utilisant votre cloud"
createurices: "Patrizio Bekerle"
alternative_a: "OneNote, Evernote, Google Keep"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "notes"
    - "synchronisation"
    - "tâche"
    - "organisation"
    - "prise de notes"
lien_wikipedia: "https://en.wikipedia.org/wiki/QOwnNotes"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qownnotes"
---

QOwnNotes est un logiciel hautement configurable qui vous permet d'utiliser une instance Owncloud ou Nextcloud pour gérer vos notes et vos listes de tâches.
Il permet notamment d'éditer du texte au format Markdown, synchroniser et partager les notes tout en leur attribuant des mots-clés et des couleurs, et même y intégrer des tâches.
Dans la mesure où QOwnNotes fonctionne avec le module dédié aux Notes de Owncloud/Nextcloud, vous pouvez aussi retrouver vos données en utilisant les applications mobiles correspondantes.

