---
nom: "Rainmeter"
date_creation: "Dimanche, 20 mai, 2018 - 19:04"
date_modification: "Vendredi, 12 avril, 2019 - 17:20"
logo:
    src: "images/logo/Rainmeter.jpg"
site_web: "https://www.rainmeter.net/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de customisation pour Windows entièrement personnalisable."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rainmeter"
---

Avec Rainmeter vous pouvez personnaliser Windows avec de nombreux thèmes: 7 sont inclus de base et beaucoup d'autres sont disponibles sur deviantart par exemple. Honnêtement, il y a tellement de thèmes que vous pouvez vous faire plaisir, et si par hasard il n'y en a aucun qui vous convienne vous pouvez toujours en faire un.

