---
nom: "WeeChat"
date_creation: "mercredi, 27 décembre, 2023 - 22:01"
date_modification: "mercredi, 27 décembre, 2023 - 22:01"
logo:
    src: "images/logo/WeeChat.png"
site_web: "https://weechat.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Client de discussion extensible"
createurices: "Sébastien Helleu"
alternative_a: "mIRC"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "discussion"
    - "irc"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WeeChat"
lien_exodus: ""
identifiant_wikidata: "Q2000469"
mis_en_avant: "non"

---

WeeChat (Wee Enhanced Environment for Chat) est un client léger de discussion pour plusieurs systèmes d'exploitation. Il tourne dans un terminal et peut être entièrement piloté avec le clavier.

Fonctionnalités principales :

- protocole IRC (natif)
- protocoles Jabber, Slack, Matrix, etc. (avec des scripts additionnels)
- liste de pseudos
- liste d'activité intelligente
- découpage horizontal et vertical
- double jeu de caractères (encodage / décodage)
- tube FIFO pour un contrôle à distance
- 256 couleurs
- recherche incrémentale de texte
- filtrage dynamique des lignes
- support des scripts écrits en Perl, Python, Ruby, Lua, Tcl, Scheme et PHP
- gestionnaire de scripts intégré
- vérification de l'orthographe
- support souris (clics et gestes)
- hautement personnalisable et extensible
- et bien plus encore !
