---
nom: "Widelands"
date_creation: "Dimanche, 2 avril, 2017 - 01:25"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Widelands.png"
site_web: "https://wl.widelands.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Vous aimez The Settlers ? Widelands est fait pour vous ! Un jeu de stratégie temps réelle orienté gestion."
createurices: "Holger Rapp (SirVer)"
alternative_a: "The Settlers"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "stratégie"
    - "gestion"
    - "économie"
    - "jeu vidéo"
    - "multi-joueur"
lien_wikipedia: "https://en.wikipedia.org/wiki/Widelands"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/widelands"
---

Widelands est un jeu de stratégie / gestion économique avec une part de combat. 3 civilisations réellement différentes sont disponibles pour jouer en solo comme en multi-joueur.
La prise en main est grandement facilité par un tutoriel complet, en plusieurs scénarios thématiques.

