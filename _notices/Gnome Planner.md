---
nom: "Gnome Planner"
date_creation: "Mardi, 4 août, 2020 - 16:32"
date_modification: "vendredi, 10 janvier, 2025 - 11:28"
logo:
    src: "images/logo/Gnome Planner.png"
site_web: "https://wiki.gnome.org/Apps/Planner"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Outil simple de création de diagramme de GANTT pour la gestion de projet."
createurices: "Imendio"
alternative_a: "trello, Microsoft Project"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "gantt"
    - "gestion de projet"
    - "organisation"
    - "planning"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnome-planner"
---

Outil simple de création de diagramme de GANTT pour la gestion de projet.
Diagramme de GANTT : Ajustez les durées des tâches ou définissez les dépendances entre elles grâce à une interface intuitive de glisser-déposer.
Vue des tâches : Affichez l'avancement général de votre projet ainsi que les coûts et efforts estimés.
Utilisation des ressources : Détectez les conflits d'affectation pour réorganiser la planification.
Les projets sont stockés dans des fichiers XML et peuvent être imprimés au format PDF ou exportés au format HTML pour une visualisation facile depuis un navigateur Web.
