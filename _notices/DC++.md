---
nom: "DC++"
date_creation: "Vendredi, 6 mai, 2022 - 18:26"
date_modification: "Samedi, 14 mai, 2022 - 01:21"
logo:
    src: "images/logo/DC++.png"
site_web: "https://dcplusplus.sourceforge.io/index.html"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "DC++ est un client de partage de fichiers"
createurices: "Jacek Sieka."
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "partage de fichiers"
    - "p2p"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Direct_Connect"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dc"
---

DC++  peut être utilisé pour se connecter au réseau Direct Connect ou au protocole ADC. Il a été créé par Jacek Sieka. DC++ vous permet de partager des fichiers sur Internet sans aucune restriction ni limite. Pour rappel, Direct Connect est un protocole de discussion et de partage de fichiers en pair à pair, où l'on télécharge un fichier directement depuis une source unique.
L'application est totalement exempte de publicités et possède une interface agréable et facile à utiliser. La prise en charge du pare-feu et du routeur est intégrée à l'application et il est facile et pratique d'utiliser des fonctionnalités telles que les connexions multi-hubs, les connexions automatiques et la reprise des téléchargements...
DC++ est un logiciel hautement classé parmi les projets hébergés chez Sourceforge. Avec plus de cinquante millions de téléchargements, de nouveaux utilisateurs continuent de profiter chaque jour des avantages du logiciel.

