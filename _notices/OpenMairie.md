---
nom: "OpenMairie"
date_creation: "Mardi, 2 juin, 2020 - 08:19"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/OpenMairie.png"
site_web: "http://www.openmairie.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
description_courte: "Framework et ensemble d'applications libres modulaires, destinées aux collectivités locales."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "collectivités territoriales"
    - "environnement de développement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openmairie"
---

Depuis 2003, la communauté openMairie propose des logiciels aux collectivités locales principalement et à d'autres organismes qui sont articulés autour du framework openMairie.
On peut y trouver une trentaines de modules, dont les plus matures sont :
openElec : pour gérer l'inscription aux listes électorales
openRésultat : Résultats électoraux
openScrutin : composition des bureaux de vote
openADS (openFoncier) : Gestion des autorisations des droits du sol
openCourrier : Gestion du courrier
openCimetière : Gestion des concessions de cimetières
openARIA : Analyse du Risque Incendie et de l'Accessibilité pour les Établissements Recevant du Public (Marseille)
openEPM : Logiciel de police municipale (ICM Service)

