---
nom: "Our Shopping List"
date_creation: "Dimanche, 24 avril, 2022 - 21:38"
date_modification: "jeudi, 28 mars, 2024 - 09:12"
logo:
    src: "images/logo/Our Shopping List.png"
site_web: "https://github.com/nanawel/our-shopping-list"
plateformes:
    - "le web"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Web application collaborative et temps-réel de gestion de listes de courses et autres todo-lists."
createurices: "Anaël Ollier"
alternative_a: "Bring!, Out of Milk"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "édition collaborative"
    - "todo-list"
    - "todo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/our-shopping-list"
---

Application auto-hébergeable mettant à disposition une interface réactive et utilisable depuis un mobile (PWA) permettant la saisie et la gestion de listes. L'objectif principal est de faciliter la collaboration autour des listes de courses, mais tout autre type de liste est envisageable.
Les listes sont composées d'articles, ceux-ci étant volontairement simples et concis. Ils ne possèdent que 3 champs principaux : le nom, la quantité, et des détails optionnels (champ de texte libre).
Une fois ajoutés, les articles sont réutilisables ce qui évite les re-saisies régulières.
Les données sont stockées sur une base MongoDB, facile à manipuler et à sauvegarder.
