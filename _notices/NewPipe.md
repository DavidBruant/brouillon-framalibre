---
nom: "NewPipe"
date_creation: "Jeudi, 12 avril, 2018 - 10:07"
date_modification: "vendredi, 6 septembre, 2024 - 23:32"
logo:
    src: "images/logo/NewPipe.png"
site_web: "https://newpipe.net"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Client multimédia YouTube, PeerTube, SoundCloud & MediaCCC libre pour Android."
createurices: "Christian Schabesberger"
alternative_a: "Youtube, TubeMate, Soundcloud"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "téléchargement de youtube"
    - "android"
    - "lecteur vidéo"
    - "lecteur musique"
    - "PeerTube"
    - "client youtube"
lien_wikipedia: "https://fr.wikipedia.org/wiki/NewPipe"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.schabi.newpipe/latest/"
identifiant_wikidata: "Q42851406"
mis_en_avant: "oui"
redirect_from: "/content/newpipe"
---

Pas besoin des services Google installés sur votre téléphone.

Permet de lire les vidéos, de s'abonner à des chaînes (sans compte), de télécharger les vidéos et beaucoup d'autres choses encore !
Newpipe permet (entre autres) :
* d’accéder au contenu de Youtube sans passer par la case publicité ;
* de n’accéder qu’à l’audio si nécessaire ;
* d'accélérer (ou ralentir) la vitesse de lecture, et de supprimer les blancs.
* d’écouter le contenu écran verrouillé.

Disponible sur F-Droid.
