---
nom: "Thunderbird"
date_creation: "Dimanche, 22 février, 2015 - 22:55"
date_modification: "mercredi, 12 février, 2025 - 09:30"
logo:
    src: "images/logo/Thunderbird.png"
site_web: "https://www.thunderbird.net/fr/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla."
createurices: ""
alternative_a: "Windows Mail, Microsoft Outlook, IncrediMail, Application Courrier (Windows 10)"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "client mail"
    - "lecteur de flux rss"
    - "agenda"
    - "irc"
    - "xmpp"
lien_wikipedia: "http://fr.wikipedia.org/wiki/Mozilla_Thunderbird"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/thunderbird"
---

Thunderbird est un client d’e-mails mais pas seulement : il fait aussi messagerie instantanée, calendrier et lit les flux RSS.
Il gère plusieurs comptes de messagerie, supporte l’IMAP et le POP3, détecte les configurations des principaux fournisseurs d’adresses e-mail et pour ne rien gâcher, propose comme Firefox une multitude d’extensions et de thèmes.
Thunderbird peut être aussi sécurisé grâce aux filtres anti-spam ou grâce au chiffrement d’e-mails.
De plus il est possible d’utiliser des filtres pour automatiser des actions (déplacer les certains mails vers un dossier, les supprimer, mettre une étiquette, les transférer…).
