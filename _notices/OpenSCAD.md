---
nom: "OpenSCAD"
date_creation: "Samedi, 24 décembre, 2016 - 17:18"
date_modification: "Mercredi, 12 mai, 2021 - 15:53"
logo:
    src: "images/logo/OpenSCAD.png"
site_web: "http://www.openscad.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "OpenSCAD est un logiciel libre de modélisation paramétrique fonctionnant sous Linux, Mac OS et windows."
createurices: "openscad.org"
alternative_a: "Autodesk 3ds Max"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "3d"
    - "dessin"
    - "modélisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenSCAD"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openscad"
---

OpenSCAD offre la possibilité de créer des objets 2D (par exemple pour une découpe laser) ou 3D (par exemple à destination de l'impression 3D).
La conception des objets passe par un pseudo langage de programmation pouvant être écrit directement au sein du logiciel dans une sous-fenêtre, une autre sous-fenêtre affichant le résultat du code écrit.
L'ensemble de la documentation technique du logiciel est disponible sur wikibook.
Il utilise la bibliothèque OpenCSG pour un rendu rapide et CGAL pour la création de l'objet final exportable.
Son intérêt croît depuis l'apparition des imprimantes 3D et du site de partage de fichier Thingiverse qui permet l'importation de scripts au format SCAD et la possibilité de passer par l'outil "Customizer" permettant de paramétrer un objet sans connaissance en programmation ou sans modifier le script originel.
Il est capable d'exporter aux formats SVG, DXF,Geomview Object File Format (OFF), STL et CSG.

