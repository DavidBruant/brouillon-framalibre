---
nom: "Grsync"
date_creation: "Lundi, 9 janvier, 2017 - 11:25"
date_modification: "Lundi, 9 janvier, 2017 - 11:25"
logo:
    src: "images/logo/Grsync.png"
site_web: "http://www.opbyte.it/grsync/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Sauvegardes de fichiers locales ou distantes."
createurices: ""
alternative_a: "SyncBack"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "sauvegarde"
    - "synchronisation"
    - "transfert de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Grsync"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/grsync"
---

Grsync permet d'effectuer des sauvegardes de fichiers ou de répertoires, localement ou à distance. Il est en fait une interface graphique au logiciel rsync disponible en ligne de commande. Les sauvegardes peuvent être incrémentales. On peut sauvegarder les préférences de nos sauvegardes, les exporter en un fichier pour les partager, lancer plusieurs sessions en un coup, on peut les simuler avant de les lancer, etc.

