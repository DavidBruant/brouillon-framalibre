---
nom: "Thorium Reader"
date_creation: "jeudi, 7 novembre, 2024 - 15:18"
date_modification: "jeudi, 7 novembre, 2024 - 15:18"
logo:
    src: "images/logo/Thorium Reader.png"
site_web: "https://thorium.edrlab.org/fr/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Visualiseur de livres electroniques (EPUB 3) et Lecteur audio (MP3)"
createurices: "EDR Lab"
alternative_a: "Adobe Reader"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "visualiseur de livres électroniques"
    - "lecteur audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Visualiseur de électroniques (PDF, EPUB 3, TXT) protégé par des DRM Adobe ou par des DRM LCP.

Lecteur de livres audio au format MP3.
