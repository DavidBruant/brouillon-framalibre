---
nom: "Estimation"
date_creation: "dimanche, 31 mars, 2024 - 09:34"
date_modification: "dimanche, 31 mars, 2024 - 09:34"
logo:
    src: "images/logo/Estimation.svg"
site_web: "https://educajou.forge.apps.education.fr/estimation/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Estimer un nombre d'après sa position sur une droite non graduée"
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "numération"
    - "mathématiques"
    - "école"
    - "construction du nombre"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Estimation propose un exercice de repérage sur droite non graduée.
Trois niveaux d'aide :
- première graduation affichée
- bloc unité déplaçable
- toutes les graduations
Fonctionnalités :
- réglage des valeurs minimum / maximum
- statistiques sur l'activité de l'élève
