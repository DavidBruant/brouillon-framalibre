---
nom: "Jami"
date_creation: "Lundi, 8 avril, 2019 - 08:57"
date_modification: "Mercredi, 12 mai, 2021 - 16:16"
logo:
    src: "images/logo/Jami.png"
site_web: "https://jami.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de communication autonome, libre et sécurisé par vidéo, audio, message et partage de fichiers."
createurices: ""
alternative_a: "Skype, WhatsApp"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "communication"
    - "partage de fichiers"
    - "messagerie instantanée"
    - "visioconférence"
    - "p2p"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ring_(logiciel)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/67852/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/jami"
---

Jami (anciennement GNU Ring) est une alternative à Skype qui respecte la vie privée et permet dans une même interface d'effectuer des appels audio, vidéo et de partager des fichiers. Jami est principalement développé par l'entreprise québécoise « Savoir-faire Linux Inc ». Le nom du logiciel est issue de la langue swahili et signifie « communauté ». Un logiciel très utile à adopter et à soutenir. La forge est disponible à cette adresse : https://git.ring.cx/

