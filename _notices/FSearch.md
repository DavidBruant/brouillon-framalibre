---
nom: "FSearch"
date_creation: "vendredi, 7 juin, 2024 - 06:52"
date_modification: "vendredi, 7 juin, 2024 - 06:52"
logo:
    src: "images/logo/FSearch.png"
site_web: "https://cboxdoerfer.github.io/fsearch/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Fsearch est un utilitaire de recherche de fichiers"
createurices: ""
alternative_a: "Everything Search Engine"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "recherche"
    - "explorateur de fichier"
    - "fichier"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

FSearch est un utilitaire de recherche de fichiers très rapide, inspiré par le moteur de recherche Everything, pour les systèmes de type Unix. Il est écrit en C et basé sur GTK3.

## Une interface graphique pour la recherche de fichiers

Fsearch vous aide à trouver des fichiers et des dossiers aussi facilement et rapidement que possible. Tapez quelques lettres et les résultats de recherche s'afficheront presque instantanément.

De nombreuses fonctionalités rendent la recherche aussi efficace que puissante :

- Ignorer la casse (p. ex., la recherche de "fsearch" correspondra aussi à "FSearch")
- Expressions régulières
- Prise en charge des caractères génériques
- Prise en charge des filtres (p. ex., recherchez uniquement des fichiers audio)
- Exclure certains fichiers et dossiers
- Triez rapidement par nom, chemin d'accès, taille, heure de modification et extension de fichier

Source de ce descriptif: [Flatpak](https://flathub.org/fr/apps/io.github.cboxdoerfer.FSearch)
