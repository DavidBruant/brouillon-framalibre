---
nom: "Ancestris"
date_creation: "mercredi, 27 décembre, 2023 - 17:55"
date_modification: "dimanche, 22 septembre, 2024 - 18:55"
logo:
    src: "images/logo/Ancestris.gif"
site_web: "https://ancestris.org"
plateformes:
    - "Apple iOS"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de généalogie libre et gratuit respectant la norme GedCom"
createurices: "Ancestris team"
alternative_a: "Hérédis, Généatique, Brother Keeper, Ancestrologie, WinGenealogic, WinFamily, MyHeritage, Geneanet, Filae, Family Tree Builder"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "GedCom"
    - "Généalogie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Ancestris est un logiciel de généalogie libre et gratuit respectant la spécification GEDCOM version 5.5 et 5.5.1. C'est un logiciel libre sous licence GNU GPL 3.0 disponible pour Linux, BSD, Solaris, MAC et Windows. Il est écrit en langage Java et repose sur la plate-forme NetBeans d'Oracle.

## Fonctionnalités
Ancestris travaille directement sur le fichier GEDCOM, ce qui garantit la maîtrise des données, évite d'avoir à faire des exports, et facilite les échanges de données entre plusieurs personnes travaillant sur une même généalogie.

Ses principales fonctionnalités :
- logiciel gratuit et complet de généalogie ;
- permet de travailler sur plusieurs généalogies en même temps ;
- interface conviviale en multifenêtres ou par onglets ;
- nombreuses vues possibles : éditions, géographique, arborescente, chronologique, tables, graphe ;
- édition des lieux et géolocalisation automatique ;
- publication internet intégrée sur un site personnel de son choix ;
- import de fichiers GedCom, avec un outil de réparation du fichier importé ;
- export vers des sites web tels que GeneaNet et CousinsGenWeb ;
- saisie d'actes en masse lors du dépouillement de registres communaux, paroissiaux ou notariaux ;
- production de rapports variés : Liste Flash, Groupe familiaux, arbres, etc. ;
- partage d'arbres entre utilisateurs ;
- numérotation Sosa, d'Aboville et Sosa d'Aboville.

## Contrôle des données
Ancestris offre un maximum de possibilités pour le contrôle et la qualité des données généalogiques.
- Données conservées et disponibles 'en clair' directement dans le fichier Gedcom ;
- 100 % compatible et respectueux de la norme GEDCOM 5.5 et 5.5.1 (version 7 en cours d'implémentation) ;
- repérage des éléments à rechercher dans l'arbre généalogique ;
- utilitaire d'identification d'anomalies ou d'incohérences dans les données ;
- utilitaire de vérification du format Gedcom utilisé dans une généalogie ;
- gère tout l'Unicode.

## Support utilisateurs
Ancestris dispose d'un support utilisateurs reposant sur une communauté réactive et dynamique d'entraide, composée de spécialistes de la généalogie, des développeurs, et de nombreux amateurs de généalogie et utilisateurs, qui participent aux évolutions du logiciel.

## Langues
Ancestris est disponible en allemand, anglais, catalan, espagnol, finnois, français, grec moderne, hongrois, italien, néerlandais, norvégien, polonais, portugais et suédois.

## Contributions à la science
En 2009 et 2010, l'équipe d'Ancestris a collaboré avec Nadine Pellen, une chercheuse sur la mucoviscidose. Grâce à des algorithmes développés pour l'occasion, elle a pu réussir ses recherches en manipulant 250 arbres généalogiques et 258 000 individus. Le 18 mars 2013, elle publie « La mucoviscidose en héritage ».
