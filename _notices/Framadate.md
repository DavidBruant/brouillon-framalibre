---
nom: "Framadate"
date_creation: "Lundi, 29 octobre, 2018 - 11:43"
date_modification: "Mardi, 16 mars, 2021 - 00:06"
logo:
    src: "images/logo/Framadate.png"
site_web: "https://framadate.org/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Organiser des rendez-vous simplement, librement."
createurices: ""
alternative_a: "Doodle"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "sondage"
    - "travail collaboratif"
    - "emploi du temps"
    - "temps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/framadate"
---

Framadate est un outil de création de sondages et de planification de rendez-vous. Il est est utilisable sans inscription.
En mode date, il vous permet de déterminer la date qui convient au maximum de participant·es pour votre prochain événement.
En mode sondage, il permet déterminer le choix ou la solution qui remporte le plus de suffrages parmi un panel de possibilités.

