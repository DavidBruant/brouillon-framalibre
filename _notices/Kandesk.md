---
nom: "Kandesk"
date_creation: "Lundi, 7 septembre, 2020 - 09:49"
date_modification: "Lundi, 10 mai, 2021 - 14:57"
logo:
    src: "images/logo/Kandesk.png"
site_web: "https://github.com/seb3s/kandesk"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Kandesk est un outil collaboratif pour utiliser la méthode Kanban. Il remplace Trello pour les besoins simples"
createurices: "Sébastien Saint-Sevin"
alternative_a: "trello"
licences:
    - "Licence MIT/X11"
tags:
    - "organisation"
    - "kanban"
    - "gestion de projet"
    - "tableau interactif"
    - "auto-hébergement"
    - "todo-list"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kandesk"
---

Kandesk est un outil collaboratif pour utiliser la méthode Kanban. Il est destiné à remplacer l'utilisation de Trello pour les besoins simples.
L'application est multi-utilisateurs et multilingues. Les tableaux sont partageables entre les utilisateurs de l'application avec une gestion fine des droits d'accès et la mise à jour est en temps réel.
Kandesk est développé en Elixir en utilisant le framework Phoenix-LiveView. Les données sont stockées dans une base PostgreSQL. L'application tourne sur un raspberry pi3+ sans soucis particuliers pour les besoins personnels ou pour de petites équipes.

