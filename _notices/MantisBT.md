---
nom: "MantisBT"
date_creation: "Mercredi, 24 mai, 2017 - 00:15"
date_modification: "Jeudi, 25 mai, 2017 - 08:21"
logo:
    src: "images/logo/MantisBT.png"
site_web: "https://mantisbt.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "La référence en matière de gestion des faits techniques."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "gestion de projet"
    - "projet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mantis_Bug_Tracker"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mantisbt"
---

Mantis Bug Tracker est un gestionnaire de faits techniques. Les faits techniques sont des incidents ou des anomalies qui apparaissent lors de l'exécution d'un programme (bug). Les évolutions apportées à un système d'information font parties elles aussi des faits techniques.
MantisBT permet de recueillir et de mémoriser ces évènements dans une base de données, de les affecter à un projet et à un acteur de ce projet, puis de suivre la résolution de l'incident ou de l'anomalie jusqu'à sa résolution. Il dispose en outre d'un workflow de validation et permet d'éditer les tableaux de bord du projet.
Toutes les fonctionnalités sont disponibles à partir d'une interface web développée en PHP. L'installation est bien documentée sur le site de l'éditeur et elle ne présente pas de difficulté particulière.
MantisBT fonctionne sur un serveur web classique tel que Apache. L'application nécessite PHP et une base de donnée type Mysql, MariaDB ou PostGreSQL.

