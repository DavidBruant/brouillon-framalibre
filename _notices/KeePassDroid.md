---
nom: "KeePassDroid"
date_creation: "Dimanche, 3 décembre, 2017 - 18:46"
date_modification: "Lundi, 10 mai, 2021 - 13:39"
logo:
    src: "images/logo/KeePassDroid.png"
site_web: "http://www.keepassdroid.com/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "KeePassDroid est un portage du gestionnaire de mots de passe sécurisés KeePass pour la plate-forme Android."
createurices: ""
alternative_a: "lastpass, 1password"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keepassdroid"
---

KeePassDroid est un portage du gestionnaire de mots de passe sécurisés KeePass pour la plate-forme Android.
Pour simplifier son utilisation, une notification apparaît pour copier facilement le nom d'utilisateur et le mot de passe choisis.

