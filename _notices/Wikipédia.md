---
nom: "Wikipédia"
date_creation: "mercredi, 1 novembre, 2023 - 13:12"
date_modification: "mercredi, 1 novembre, 2023 - 13:12"
logo:
    src: "images/logo/Wikipédia.png"
site_web: "https://wikimediafoundation.org/fr/technology/"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Tout Wikipédia en accès direct sur votre téléphone"
createurices: "Wikimédia"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "encyclopédie"
lien_wikipedia: "https://www.mediawiki.org/wiki/Wikimedia_Apps/fr"
lien_exodus: "https://reports.exodus-privacy.eu.org/reports/org.wikipedia/latest"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Wikipédia est une encyclopédie en ligne collaborative, généraliste et multilingue sous licence libre, qui vous permet de vous renseigner sur plus de 6 millions et demi de sujets.

Pour y accéder rapidement depuis votre téléphone, il est possible de passer par le site web mobile ou par cette application.
