---
nom: "Liberaforms"
date_creation: "lundi, 3 juin, 2024 - 23:34"
date_modification: "lundi, 3 juin, 2024 - 23:34"


site_web: "https://liberaforms.org/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Outil pour créer des formulaires en ligne"
createurices: ""
alternative_a: "Google Forms"
licences:
    - "Licence MIT/X11 (MIT)"
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "Formulaire"
    - "Sondage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Liberaforms permet de créer des formulaires en ligne de façon plutôt intuitive.

Parmi les fonctionnalités :
- publier ou non un formulaire
- dupliquer un formulaire
- éditer un message de présentation
- éditer le message de remerciement
- ajouter des utilisateur⋅ices pour accéder à l'édition du formulaire et à ses réponses
