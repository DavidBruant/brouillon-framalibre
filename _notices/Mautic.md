---
nom: "Mautic"
date_creation: "Mercredi, 5 avril, 2017 - 21:32"
date_modification: "Mercredi, 5 avril, 2017 - 21:32"
logo:
    src: "images/logo/Mautic.png"
site_web: "http://mautic.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Mautic est un outil de Marketing Automation permettant de planifier et exécuter des campagnes marketings"
createurices: "Mautic"
alternative_a: "NetResult, SalesFusion, Marketo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mautic"
---

Publié en 2014, Mautic est devenu la référence de Marketing Automation Open Source.
Simple & Accessible, il permet en quelques clics de segmenter sa base de clients et de planifier et exécuter des campagnes marketings dynamiques.
A l'aide d'un outil de Marketing Automation, vous pouvez ainsi définir votre programme marketing et jouer des campagnes de nurtering adaptées à votre activité afin de pouvoir faire murir vos prospects et alimenter vos équipes commerciales en leads qualifiés et dont le potentiel et la prise de décision sont plus avancées

