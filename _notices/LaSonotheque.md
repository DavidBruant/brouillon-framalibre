---
nom: "LaSonotheque"
date_creation: "Vendredi, 21 août, 2020 - 14:55"
date_modification: "Mercredi, 12 mai, 2021 - 15:04"
logo:
    src: "images/logo/LaSonotheque.png"
site_web: "https://lasonotheque.org"
plateformes:

langues:
    - "Français"
    - "English"
description_courte: "Banque de sons totalement gratuits, libres de droits et de qualité, destinée à tous vos projets."
createurices: ""
alternative_a: ""
licences:
    - "Creative Commons Zero"
    - "Domaine public"
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "multimédia"
    - "son"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lasonotheque"
---

Banque de sons totalement gratuits, libres de droits et de qualité, destinée à tous vos projets, y compris commerciaux. Une sonothèque de bruitages, d'ambiances sonores, de sons seuls et de musiques aux formats MP3, WAV, BWF, AIFF, OGG, FLAC, AAC et M4A. Des centaines de fichiers sans traitement à écouter et télécharger sans modération. Pour l'audiovisuel, les podcasts, le cinéma, le théâtre, les livres audio, le jeu vidéo, les sagas audio, la radio, le développement, etc.

