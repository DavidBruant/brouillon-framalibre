---
nom: "Jbead"
date_creation: "jeudi, 22 août, 2024 - 13:29"
date_modification: "jeudi, 22 août, 2024 - 13:29"
logo:
    src: "images/logo/Jbead.png"
site_web: "https://www.jbead.ch/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Créer le diagramme préparatoire pour bâtir des perles en spirale"
createurices: "Damian Brunold"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "Tissage"
    - "Bricolage"
    - "Couture"
    - "Crochet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Ce logiciel permet de créer le diagramme permettant d'organiser le tissage au crochet. Vous pouvez créer la séquence de perles à enfiler, en fonction d'une représentation à plat du motif désiré. 

Le programme est écrit en java, il est simple et intuitif. Il est fourni avec des lanceur sous Windows et MacOs. Sous linux, utilisez `java -jar jbead.jar` pour lancer le programme.
