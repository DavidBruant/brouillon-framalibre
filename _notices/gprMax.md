---
nom: "gprMax"
date_creation: "Mercredi, 3 novembre, 2021 - 10:55"
date_modification: "Mercredi, 3 novembre, 2021 - 10:55"
logo:
    src: "images/logo/gprMax.png"
site_web: "https://www.gprmax.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Logiciel de simulation numérique de propagation d'onde généré par un radar de sol"
createurices: "Antonis Giannopoulos, Craig Warren"
alternative_a: "COMSOL Multiphysics"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "calcul scientifique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gprmax"
---

Ce logiciel permet de simuler la propagation des ondes électromagnétiques dans un milieu diélectrique en résolvant les équations de Maxwell à l'aide de la méthode des différences finies.
Logiciel sans interface graphique. Interfaçable avec Python ou fichier de commande. Comprend des visualisations des résultats.
Ce logiciel est financé par l'Union Européenne et Google.

