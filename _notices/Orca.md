---
nom: "Orca"
date_creation: "Vendredi, 21 juin, 2019 - 11:12"
date_modification: "Dimanche, 21 juillet, 2019 - 18:16"
logo:
    src: "images/logo/Orca.png"
site_web: "https://wiki.gnome.org/Projects/Orca"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Lecteur d'écran libre pour les environnements de bureau GNU/Linux"
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "lecteur d'écran"
    - "accessibilité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Orca_(accessibilit%C3%A9)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/orca"
---

Orca est un lecteur d'écran : il permet aux utilisateurs aveugles ou malvoyants d'utiliser pleinement les fonctionnalités d'un environnement de bureau GNU/Linux, grâce à une synthèse vocale, à un afficheur Braille ou à une loupe virtuelle, et à l'exploitation des outils d'accessibilité du système.
Il est disponible dans un grand nombre de langues, et un guide d'utilisation assez étendu et traduit est en ligne.
Conçu pour Gnome, il est également compatible avec les autres environnements de bureau répandus.
Le principal inconvénient est qu'il n'existe pour le moment qu'un seul synthétiseur vocal libre pour GNU/Linux, espeak, dont la voix est plutôt désagréable.

