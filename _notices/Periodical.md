---
nom: "Periodical"
date_creation: "mercredi, 1 novembre, 2023 - 13:31"
date_modification: "samedi, 15 juin, 2024 - 13:24"
logo:
    src: "images/logo/Periodical.png"
site_web: "https://arnowelzel.de/projekte/periodical"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une application de calendrier menstruel"
createurices: "Arno Welzel"
alternative_a: "Flo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "règles"
    - "menstruation"
    - "santé"
    - "calendrier"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/de.arnowelzel.android.periodical/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Periodical vous permet d'avoir un calendrier où vous pouvez indiquer les dates de vos dernières périodes de règles et vos symptômes. L'application estimera automatiquement quand sera la prochaine date de vos règles, vous permettant d'anticiper sur les 2 mois suivants.
