---
nom: "Abc-Map"
date_creation: "Jeudi, 26 août, 2021 - 14:17"
date_modification: "Dimanche, 26 novembre, 2023 - 22:12"
logo:
    src: "images/logo/Abc-Map.png"
site_web: "https://abc-map.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Abc-Map permet de créer des cartes géographiques simplement: importez, dessinez, visualisez des données et +"
createurices: ""
alternative_a: "ArcGIS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "cartographie"
    - "géographie"
    - "éducation"
    - "carte géographique"
    - "sig"
    - "statistique"
    - "osm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/abc-map"
---

Abc-Map permet de manipuler des données géographiques simplement et sans connaissances techniques avancées. Ce logiciel est disponible en ligne et s’utilise avec un navigateur web, sans installation.
Des vidéos  courtes expliquent le fonctionnement du logiciel (
