---
nom: "Brie.fi/ng"
date_creation: "Vendredi, 31 juillet, 2020 - 15:57"
date_modification: "Mercredi, 12 mai, 2021 - 14:42"
logo:
    src: "images/logo/Brie.fi-ng.png"
site_web: "https://brie.fi/ng/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "English"
description_courte: "Visioconférence sécurisée."
createurices: "Dirk Holtwick"
alternative_a: "Zoom, Skype, WhatsApp, Visio"
licences:
    - "Licence MIT/X11"
tags:
    - "visioconférence"
    - "communication chiffrée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/briefing"
---

Brie.fi/ng permet de rapidement et simplement créer un salon de visioconférence sécurisée.
Il est ensuite possible d'inviter d'autres participant·e·s grâce à un lien d'invitation ou un QR code.
Il n'est pas nécessaire de créer un compte et aucun cookie n'est utilisé.
WebRTC est utilisé pour connecter directement les participant·e·s entre elleux et le contenu des communications est chiffré.

