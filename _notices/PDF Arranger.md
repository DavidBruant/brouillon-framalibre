---
nom: "PDF Arranger"
date_creation: "Vendredi, 5 août, 2022 - 10:56"
date_modification: "jeudi, 3 octobre, 2024 - 15:36"
logo:
    src: "images/logo/PDF Arranger.png"
site_web: "https://github.com/pdfarranger/pdfarranger"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel simple pour extraire, assembler, pivoter et réordonner des pages de documents PDF."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pdf-arranger"
---

Un logiciel simple pour extraire, assembler, pivoter et réordonner des pages de documents PDF.
Les transformations peuvent être réalisées sur une ou plusieurs pages. Il est aussi possible d'annuler ou reprendre les modifications.
