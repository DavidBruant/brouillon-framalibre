---
nom: "OSS Document Scanner"
date_creation: "mardi, 11 juin, 2024 - 12:22"
date_modification: "mardi, 11 juin, 2024 - 12:22"
logo:
    src: "images/logo/OSS Document Scanner.png"
site_web: "https://apt.izzysoft.de/fdroid/index/apk/com.akylas.documentscanner"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Application pour numériser vos documents"
createurices: "Akylas"
alternative_a: "CamScanner"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "scanner"
    - "document"
    - "pdf"
    - "productivité"
    - "bureautique"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.akylas.documentscanner/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

L'OSS Document Scanner est une application Open Source pour numériser tous vos documents. Soit vous numérisez à l'aide de votre appareil photo, soit en important une image. L'application détectera automatiquement votre document dans la photo et recadrera l'image. Une fois le document créé, vous pouvez détecter le texte dans le document à l'aide de l'OCR. Vous pouvez également partager votre document au format PDF. Si vous le souhaitez, vous pouvez synchroniser les données de l'application avec un serveur webdav (comme nextloud) pour ne jamais rien perdre !