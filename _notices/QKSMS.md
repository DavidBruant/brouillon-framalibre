---
nom: "QKSMS"
date_creation: "Jeudi, 23 février, 2023 - 16:05"
date_modification: "mardi, 13 février, 2024 - 22:05"
logo:
    src: "images/logo/QKSMS.png"
site_web: "https://github.com/moezbhatti/qksms"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Application de SMS/MMS libre et open-source."
createurices: "Moez Bhatti"
alternative_a: "google messages, Facebook Messenger, Messenger"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "messagerie instantanée"
    - "sms"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.moez.QKSMS/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qksms"
---

QKSMS propose de remplacer le système de messagerie par défaut sur votre smartphone android.
Créée par Moez Bhatti sous la licence publique générale GNU v3.0 (GPLv3). Celui-ci ne révolutionne rien mais évite de se faire tracer ce qui est déjà pas mal !
-Thèmes personnalisable
-Mode nuit et noir profond
-Répondre aux messages directement via le pop-up
-Messagerie de groupe
-MMS
-Envoi de messages différés
-Recherche dans vos messages
Une version premium existe permettant d'ajouter quelques options qui ne sont pas indispensables.
