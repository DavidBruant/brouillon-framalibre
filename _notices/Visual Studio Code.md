---
nom: "Visual Studio Code"
date_creation: "Dimanche, 10 mars, 2019 - 10:44"
date_modification: "dimanche, 17 mars, 2024 - 19:49"
logo:
    src: "images/logo/Visual Studio Code.png"
site_web: "https://code.visualstudio.com/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un éditeur de code très complet et supportant de nombreux langages."
createurices: "Microsoft"
alternative_a: "Sublime Text"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "développement"
    - "code"
    - "programmation"
    - "traitement de texte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Visual_Studio_Code"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/visual-studio-code"
---

Visual Studio Code est un éditeur de code open-source développé par Microsoft supportant un très grand nombre de langages grâce à des extensions. Il supporte l’autocomplétion, la coloration syntaxique, le débogage, et les commandes git.
Il est à noter que si le code source est diffusé sous la licence libre MIT, l'exécutable est proposé sur le site officiel de Microsoft sous une licence privatrice.
Si vous souhaitez télécharger une version sous licence libre sans à avoir à compiler le code source vous-même, jetez un œil à [VSCodium](https://vscodium.com/). Il s'agit d'une version de VS Code compilée à partir des sources du dépôt GitHub du projet de Microsoft et disposant donc des mêmes fonctionnalités sans la présence de la licence privatrice de celui-ci, sans les fonctions de pistage des utilisateurs et sans le logo officiel (qui est propriétaire).
