---
nom: "Catima"
date_creation: "vendredi, 14 juin, 2024 - 08:17"
date_modification: "vendredi, 14 juin, 2024 - 08:17"
logo:
    src: "images/logo/Catima.png"
site_web: "https://catima.app"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un gestionnaire de cartes de fidélité pour Android"
createurices: "Sylvia van Os"
alternative_a: "Fidme, Stocard"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "android"
    - "carte de fidélité"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/me.hackerchick.catima/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

Catima est une application libre et open-source conçue pour offrir aux utilisateurs un moyen de stocker des cartes de fidélité et d'autres cartes basées sur des codes-barres sans vendre leurs données privées. Il est entièrement hors ligne et n'envoie vos données à aucun serveur.

Gagner de l'argent n'est pas un objectif du projet Catima mais vise à donner aux utilisateurs une alternative respectueuse de la vie privée aux autres applications de cartes de fidélité.

Catima ne nécessite pas d'accès à Internet, n'affiche aucune publicité et ne partage aucune donnée.