---
nom: "Grammalecte"
date_creation: "Mardi, 13 décembre, 2016 - 00:24"
date_modification: "Jeudi, 14 mai, 2020 - 14:12"
logo:
    src: "images/logo/Grammalecte.png"
site_web: "https://grammalecte.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Un correcteur grammatical et typographique équipé d'un module de suggestion."
createurices: "Olivier R."
alternative_a: "Antidote, Cordial, Prolexis"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "correcteur"
    - "orthographe"
    - "grammaire"
    - "langue"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Grammalecte"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/grammalecte"
---

Un très bel outil pour vous aider à rédiger en détectant les fautes et en vous suggérant comment les corriger.
Vous pourrez l'utiliser aussi bien avec votre traitement de texte (OpenOffice, LibreOffice) qu'avec votre navigateur Firefox pour lequel il se présente comme une extension. Que ce soit donc en ligne ou hors-ligne, vous pourrez profiter de son aide précieuse.
Outre des fonctions classique d'orthographe lexicale, il permet de détecter et de corriger (avec ses suggestions) des erreurs grammaticales et d'autres qui relèvent de la typographie.
Parmi les nombreuses fonctionnalités qui vous rendront service, le Formateur de texte est magique : d'un seul clic vous pouvez rectifier des petites erreurs, ce qui est précieux sur un document long qu'il s'agit de réviser !

