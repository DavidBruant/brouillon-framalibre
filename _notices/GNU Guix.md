---
nom: "GNU Guix"
date_creation: "lundi, 24 juin, 2024 - 16:42"
date_modification: "mercredi, 7 août, 2024 - 11:56"
logo:
    src: "images/logo/GNU Guix.png"
site_web: "https://guix.gnu.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Guix est une distribution du système d'exploitation GNU développé par le projet GNU"
createurices: "Projet GNU"
alternative_a: "Microsoft Windows"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "libre"
    - "distribution gnu/linux"
    - "linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Guix_System"
lien_exodus: ""
identifiant_wikidata: "Q19597382"
mis_en_avant: "non"

---

- Libérateur : Guix est une distribution du système d'exploitation GNU développé par le projet GNU — qui respecte la liberté des utilisateurs et utilisatrices d'ordinateurs.
- Fiable : Guix prend en charge les mises à jour et les retours transactionnels, la gestion des paquets non-privilégiée, et bien plus. Lorsqu'il est utilisé comme distribution GNU/Linux indépendante, Guix propose une approche déclarative à la gestion de la configuration pour construire des systèmes d'exploitations transparents et reproductibles.
- Bidouillable : Il fournit des API en Guile Scheme, en particulier des langages embarqués spécifiques au domaine (EDSL) pour definir des paquets et des configurations de systèmes complets.
