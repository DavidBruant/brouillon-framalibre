---
nom: "fre:ac"
date_creation: "Lundi, 26 novembre, 2018 - 13:35"
date_modification: "Lundi, 26 novembre, 2018 - 13:35"
logo:
    src: "images/logo/fre:ac.png"
site_web: "https://www.freac.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "fre:ac est un convertisseur audio et un ripper de CD qui prend en charge de nombreux formats audio."
createurices: "Robert Kausch"
alternative_a: "Exact Audio Copy, MediaCoder, Freemake Audio Converter, dBpoweramp"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "flac"
    - "ogg vorbis"
    - "cd ripper"
    - "convertisseur"
    - "audio"
    - "logiciel audio"
lien_wikipedia: "https://en.wikipedia.org/wiki/Fre:ac"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freac"
---

Convertisseur audio qui prend en charge les formats AAC, FLAC, MP3, Ogg Vorbis, MP4/M4A, WMA, WAV...
Ripper de CD avec prise en charge des bases de données CDDB / FreeDB.
Encodeurs multicoeurs optimisés pour accélérer les conversions sur les PC modernes.
Prise en charge Unicode complète pour les balises et les noms de fichiers.
Facile à apprendre et à utiliser, il offre toujours des options expertes quand vous en avez besoin.
Interface utilisateur multilingue disponible en 40 langues.
Également disponible sous forme d'application portable.

