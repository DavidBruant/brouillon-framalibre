---
nom: "eXist-db"
date_creation: "Samedi, 5 janvier, 2019 - 11:58"
date_modification: "Samedi, 5 janvier, 2019 - 11:58"
logo:
    src: "images/logo/eXist-db.gif"
site_web: "http://exist-db.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Base de données en XML pour construire une application"
createurices: "eXist"
alternative_a: "Installer"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "installation"
    - "java"
    - "linux"
    - "windows"
lien_wikipedia: "https://en.wikipedia.org/wiki/EXist"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/exist-db"
---

eXist-db (ou eXist) est un projet libre pour construire des bases de données NoSQL avec du XML C'est à la fois du document NoSQL orienté données et une base de données XML (Support de XML, JSON, HTML et documents binaires). Contrairement aux bases de données relationnelles (RDBMS) et NoSQL, eXist-db utilise XQuery et XSLT pour programmer les applications.
eXist-db utilise la version 2.1 de GNU LGPL.

