---
nom: "LaBoite"
date_creation: "Jeudi, 22 octobre, 2020 - 12:31"
date_modification: "dimanche, 3 novembre, 2024 - 14:56"
logo:
    src: "images/logo/LaBoite.png"
site_web: "https://gitlab.mim-libre.fr/alphabet/laboite"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "LaBoite permet d'avoir un portail collaboratif personnel et professionnel."
createurices: "Pôle Logiciels Libre"
alternative_a: ""
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "éducation"
    - "portail"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/laboite"
---

LaBoite est un portail personnalisable permettant aux utilisateur⋅ices d'avoir un point d'accès unique aux applications proposées par leur association, entreprise et aussi avec leur raccourcis paramétrable vers les services web personnels qu'ils/elles utilisent le plus couramment.
Un système de gestion des applications favorites permet à l'utilisateur⋅ice de personnaliser et de créer des espaces de rangement thématique, ainsi que d'utiliser un moteur de recherche interne.
Vous y trouverez la gestion des groupes qui vous permettent via les API de vos différents services d'interagir entre eux, la création du groupes dans LaBoite permet de créer le partage de groupe dans votre cloud ou et le projet dans le système de Kanban  Latelier.
Vous pourrez y gérer les mentions légales de votre site et ses conditions d'utilisations.
LaBoite est réalisé en MeteorJS, son design est "responsive" et l'authentification repose sur OpenID.
