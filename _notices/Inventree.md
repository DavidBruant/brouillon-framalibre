---
nom: "Inventree"
date_creation: "mardi, 26 décembre, 2023 - 11:08"
date_modification: "mardi, 26 décembre, 2023 - 11:08"
logo:
    src: "images/logo/Inventree.png"
site_web: "https://inventree.org"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gestion intuitive de l'inventaire"
createurices: "Inventree"
alternative_a: ""
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "docker"
    - "gestion du stock"
    - "entreprise"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

InvenTree est un système de gestion des stocks open-source qui permet une gestion intuitive des pièces et un contrôle des stocks. InvenTree est conçu pour être extensible et offre de multiples options d'intégration avec des applications externes ou l'ajout de plugins personnalisés.
