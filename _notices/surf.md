---
nom: "surf"
date_creation: "Mercredi, 22 mars, 2017 - 22:13"
date_modification: "Vendredi, 21 mai, 2021 - 09:54"
logo:
    src: "images/logo/surf.png"
site_web: "http://surf.suckless.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "English"
description_courte: "Un navigateur web ultra simple et léger."
createurices: "Christoph Lohmann"
alternative_a: "Google Chrome, Chrome, Internet Explorer, Microsoft Edge, Apple Safari"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "https://en.wikipedia.org/wiki/Surf_(web_browser)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/surf"
---

Surf est un navigateur web ultra léger basé sur Webkit2. Le maitre mot derrière la conception de ce logiciel est simplicité (la philosophie suckless). Il ne comporte ni onglets, ni favoris, mais il offre la possibilité d'activer ou de désactiver à souhait des fonctionnalités comme le javascript ou les cookies. Il est écrit en C et comporte moins de 2000 lignes de code. Il est donc destiné à des utilisateurs avancés, en effet la configuration du logiciel se fait uniquement en modifiant le code et en le recompilant.
C'est le navigateur idéal pour un vieil ordinateur peu puissant que l'on souhaite restorer ou pour un kiosque qui n'affichera qu'un seul site web.

