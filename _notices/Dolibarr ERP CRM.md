---
nom: "Dolibarr ERP CRM"
date_creation: "Mardi, 3 janvier, 2017 - 17:05"
date_modification: "Mardi, 17 janvier, 2023 - 17:02"
logo:
    src: "images/logo/Dolibarr ERP CRM.png"
site_web: "https://www.dolibarr.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Dolibarr ERP & CRM est une application web rapide à maîtriser de votre activité professionnelle ou associative"
createurices: "Rodolphe Quiédeville, Laurent Destailleur"
alternative_a: "Sage, EBP, Ciel Associations, CIEL, Odoo Enterprise Edition, Odoo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "erp"
    - "crm"
    - "facturation"
    - "devis"
    - "gestion commerciale"
    - "gestion du stock"
    - "agenda"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dolibarr"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/dolibarr-erp-crm"
---

Dolibarr ERP CRM est un logiciel libre de gestion pour les entreprises, auto-entrepreneurs ou associations.
Vous pouvez l'installer et l'utiliser comme application autonome, ou en ligne sur un serveur mutualisé ou dédié afin d'y accéder depuis un navigateur web.
Liste des principales fonctionnalités (non exhaustive)
- Catalogue de produits et services ;
- Annuaire de clients, prospects, fournisseurs et contacts
- Gestion des comptes bancaires/Caisses
- Gestion des propositions commerciales (devis)
- Gestion des commandes
- Gestion des contrats de services
- Gestion des factures
- Gestion de stock (prise en charge de plusieurs entrepôts)
- Gestion des Interventions
- Gestion de projets
- Ordres de fabrication
- Suivi des paiements
- Prélèvements
- Gestion des expéditions
- E-mailing
- Fonctions d'import et d'export
- Agenda
- Gestion des demandes de congés et notes de frais
- Gestion des adhérents
- Multi utilisateur, multi langue, multi-devise
...

