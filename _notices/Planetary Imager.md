---
nom: "Planetary Imager"
date_creation: "Jeudi, 27 juillet, 2017 - 03:49"
date_modification: "Mercredi, 12 mai, 2021 - 15:30"
logo:
    src: "images/logo/Planetary Imager.png"
site_web: "http://blog.gulinux.net/en/planetary-imager"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un logiciel d'acquisition vidéo planétaire pour l'astronomie"
createurices: "Marco Gulino (GuLinux)"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "astronomie"
    - "planète"
    - "capture vidéo"
    - "webcam"
    - "télescope"
    - "lunette astronomique"
    - "satellite"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/planetary-imager"
---

Planetary Imager est un logiciel d'acquisition vidéo destiné aux astronomes désireux de capturer des images planétaires.
De nombreux paramètres sont disponibles afin d'affiner au mieux la capture.
Il supporte le V4L2, le ZWO ainsi que le QHY.

