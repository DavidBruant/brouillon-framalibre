---
nom: "Csound"
date_creation: "Dimanche, 14 octobre, 2018 - 02:11"
date_modification: "Dimanche, 14 octobre, 2018 - 02:11"
logo:
    src: "images/logo/Csound.png"
site_web: "https://csound.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Un langage de programmation pour la création sonore, accompagné de nombreuses interfaces."
createurices: "John Fitch, université de Bath"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "création"
    - "mao"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Csound"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/csound"
---

Csound est à la base un langage de programmation pour créer de la musique: l'artiste écrit un fichier texte pour décrire les instruments, un second pour la partition, et Csound transforme tout cela en fichier musical.
C'est un vénérable logiciel développé depuis les années 90, qui peut fonctionner maintenant en temps réel, qui peut être utilisé comme bibliothèque logicielle, et pour lequel existent plusieurs interfaces graphiques.

