---
nom: "ELF"
date_creation: "samedi, 9 novembre, 2024 - 06:59"
date_modification: "samedi, 9 novembre, 2024 - 06:59"
logo:
    src: "images/logo/ELF.png"
site_web: "https://educajou.forge.apps.education.fr/elf/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Cette application lance un diaporama \"flash\"(identification rapide) aléatoire à partir d'une liste de mots. Au cours du diaporama on ajuste la fréquence de réapparition de chaque mot selon l'aisance des élèves."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "diaporama"
    - "lecture"
    - "cycle 2"
    - "fluence"
    - "flash"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

ELF (Éducajou Lecture Flash) permet de lancer instantanément un diaporama à partir d'une liste de mots, en adaptant leur fréquence d'apparition en fonction de la facilité de lecture.
