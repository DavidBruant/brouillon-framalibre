---
nom: "OpenBSD"
date_creation: "vendredi, 21 février, 2025 - 14:57"
date_modification: "vendredi, 28 février, 2025 - 00:23"
logo:
    src: "images/logo/OpenBSD.png"
site_web: "https://www.openbsd.org/"
plateformes:
    - "Autre"
langues:
    - "English"
description_courte: "OpenBSD est un BSD qui est dérivé de NetBSD."
createurices: "Théo de Raadt et l'OpenBSD project"
alternative_a: "Linux, NetBSD et FreeBSD"
licences:
    - "Autre"
tags:
    - "serveur"
    - "système d'exploitation (os)"
    - "bsd"
    - "unix"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenBSD"
lien_exodus: ""
identifiant_wikidata: "Q34215"
mis_en_avant: "non"

---

OpenBSD est un système d'exploitation libre de type Unix, dérivé de 4.4BSD. Créé en 1994 par Theo de Raadt, il est issu de la séparation avec NetBSD, le plus ancien des trois autres principaux systèmes d'exploitation de la famille des BSD aujourd'hui en activité. Trois décennies plus tard, en 2024, est annoncé qu'OpenBSD n'utilise plus aucune brique de NetBSD6. Le projet OpenBSD est réputé pour son intransigeance sur la liberté du logiciel et du code source, la qualité de sa documentation, et l'importance accordée à la sécurité et la cryptographie intégrée.OpenBSD inclut un certain nombre de mesures de sécurité absentes ou optionnelles dans d'autres systèmes d'exploitation. Ses développeurs ont pour tradition de réaliser des audits de code à la recherche de problèmes de sécurité et de bogues. Le projet suit des politiques strictes sur les licences et préfère sa propre licence open source ISC et autres variantes de la licence BSD : dans le passé, ceci a conduit à un audit exhaustif des licences et des remplacements, voire des suppressions de codes sous licences considérées comme moins acceptables.À l'instar de la plupart des systèmes d'exploitation basés sur BSD, le noyau d'OpenBSD et ses programmes utilisateurs, tels que le shell et les outils habituels comme cat et ps, sont développés dans un seul et même dépôt de sources géré avec CVS. Les logiciels tiers sont fournis sous forme de paquets binaires ou peuvent être compilés depuis leurs sources grâce à la collection des « ports ».Le projet est coordonné par Theo de Raadt de sa maison à Calgary, Alberta, Canada, et la mascotte du projet est Puffy, un diodon.
