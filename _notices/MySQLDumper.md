---
nom: "MySQLDumper"
date_creation: "Dimanche, 25 avril, 2021 - 18:19"
date_modification: "Lundi, 10 mai, 2021 - 13:55"
logo:
    src: "images/logo/MySQLDumper.jpg"
site_web: "https://www.forum.mysqldumper.de/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "MySQLDumper permet de sauvegarder et restaurer des bases de données MySQL."
createurices: "Daniel Schlichtholz"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "gestionnaire de base de données"
    - "mysql"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mysqldumper"
---

Il permet de sauvegarder et restaurer de grosses bases de données.
Quand la sauvegarde est terminée il est possible d'envoyer la sauvegarde par mail en pièce jointe ou vers maximum trois serveurs FTP différents.
En définissant le nombre de sauvegarde à garder, on économise de l'espace sur le serveur.
Et bien d'autres fonctionnalités.

