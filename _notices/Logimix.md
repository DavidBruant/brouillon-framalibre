---
nom: "Logimix"
date_creation: "mardi, 15 octobre, 2024 - 17:07"
date_modification: "mardi, 15 octobre, 2024 - 17:07"
logo:
    src: "images/logo/Logimix.png"
site_web: "https://ladigitale.dev/logimix/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logimix est un logiciel pour créer des mixages audio hors ligne. Les sons intégrés sont libres de droits et proviennent du site La Sonothèque (https://lasonotheque.org)."
createurices: "Emmanuel ZIMMERT // ez[at]ladigitale.dev"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "audio"
    - "enregistrement"
    - "mixage"
    - "podcast"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Grâce à Logimix, logiciel multiplateforme (Windows, macOS et Linux) utilisable sans connexion Internet, la création de capsules audio de qualité est facilitée.

Quelques fonctionnalités de Logimix :
- création de mixages audio (combinaison de sons de différentes natures) ;
- découpage de fichiers audio ;
- conversion de vidéos MP4 en fichiers audio ;
- import de fichiers audio à partir de votre ordinateur ;
- enregistrement de votre voix ;
- banque de sons intégrée avec plusieurs dizaines de fichiers audio sous licence Creative Commons répartis en 6 catégories : les animaux, les lieux, les personnes, les transports, les bruitages et les musiques.
