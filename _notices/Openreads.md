---
nom: "Openreads"
date_creation: "Mardi, 5 juillet, 2022 - 23:48"
date_modification: "Jeudi, 7 juillet, 2022 - 12:27"
logo:
    src: "images/logo/Openreads.png"
site_web: "https://f-droid.org/en/packages/software.mdev.bookstracker/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Le logiciel pour garder une trace de ses lectures."
createurices: "Mateusz Bak"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "livre"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/software.mdev.bookstracker/late…"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openreads"
---

Grâce à ce logiciel, on peut organiser ses livres qu'on peut ajouter manuellement, scanner avec un code-barres ou rechercher dans Open Library en quatre listes : fini, en cours, non terminé et pour plus tard. Aussi, on peut ajouter son avis, sa note, des étiquettes, le nombre de pages, sa date de début de lecture sur un livre. Après, on peut obtenir ses statistiques de lecture dont le nombre de pages et de livres lus par mois et par an et au total, le temps de lecture : le plus rapide, le plus long ; le livre le plus court et le plus long, le temps de lecture moyen.
Enfin, on peut choisir la couleur de l'interface du logiciel.

