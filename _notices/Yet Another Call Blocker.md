---
nom: "Yet Another Call Blocker"
date_creation: "mercredi, 1 novembre, 2023 - 13:27"
date_modification: "mercredi, 1 novembre, 2023 - 13:27"
logo:
    src: "images/logo/Yet Another Call Blocker.png"
site_web: "https://gitlab.com/xynngh/YetAnotherCallBlocker"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une application simple de blocage d'appels qui vous aidera à éviter de répondre aux appels indésirables en utilisant une base de données de numéros de téléphone participative."
createurices: "Xynngh"
alternative_a: "Should I Answer?"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "appels audio"
    - "démarchage téléphonique"
    - "protection"
    - "blocage"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/dummydomain.yetanothercallblocker/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Caractéristiques :

* Utilise une base de données hors ligne.
* Bloque automatiquement les appels avec une note négative (option).
* Liste noire locale avec prise en charge des caractères génériques.
* Affiche une notification avec un résumé du numéro de téléphone (note, nombre d'avis, catégorie) lors des appels entrants (option).
* Mises à jour automatiques de la base de données incrémentielle / delta (option).
* Vous pouvez consulter les avis en ligne pour le numéro de l'appelant (fournis par un service tiers).
* « Mode de blocage d'appel avancé » pour bloquer les appels sur Android 7+ avant que le téléphone commence à sonner (doit être activé manuellement dans les paramètres de l'application).
