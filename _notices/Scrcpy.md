---
nom: "Scrcpy"
date_creation: "vendredi, 29 novembre, 2024 - 11:52"
date_modification: "vendredi, 29 novembre, 2024 - 12:05"
logo:
    src: "images/logo/Scrcpy.svg"
site_web: "https://github.com/Genymobile/scrcpy"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
    - "Autres langues"
description_courte: "Permet de déporter l'affichage d'un appareil Android vers un ordinateur et d'interagir dessus"
createurices: "Genymobile, Romain Vimont"
alternative_a: "Phone Mirror, Airdroid"
licences:
    - "Licence Apache (Apache)"
tags:
    - "Android"
    - "Déport vidéo"
    - "Affichage"
    - "Smartphone"
    - "Tablette"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scrcpy"
lien_exodus: ""
identifiant_wikidata: "Q92566165"
mis_en_avant: "non"

---

Ce logiciel permet de répliquer l'affichage d'un appareil Android vers l'ordinateur. Ce clonage peut se faire avec ou sans interaction avec les logiciels sur le dispositif : on peut se contenter de l'affichage ou utiliser depuis l'ordinateur les logiciels présents sur la tablette ou le smartphone. Il est possible de réaliser l'enregistrement vidéo de l'écran, utiliser la webcam, utiliser le presse-papier, etc. La connexion peut se faire via USB ou wifi (il vaut mieux utiliser l'USB en première intention pour s'assurer du bon fonctionnement). Scrcpy possède un lanceur mais l'utiliser en ligne de commande permet de lancer le programme avec différentes options expliquées sur le site officiel ou [ici](https://doc.ubuntu-fr.org/scrcpy).
