---
nom: "Hubzilla"
date_creation: "Mercredi, 24 juillet, 2019 - 20:49"
date_modification: "Jeudi, 25 juillet, 2019 - 20:08"
logo:
    src: "images/logo/Hubzilla.png"
site_web: "https://zotlabs.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Connecter des communautés libres et indépendantes sur le web"
createurices: "Mike Macgirvin"
alternative_a: "Facebook"
licences:
    - "Licence MIT/X11"
tags:
    - "fediverse"
    - "agenda"
    - "wiki"
    - "activitypub"
    - "réseau social"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/hubzilla"
---

Plateforme web multifonctionnelle permettant d'avoir une identité nomade. Hubzilla peut être utilisé comme un réseau social, un blog, un agenda, un wiki ou un cloud pour partager ses fichiers.
Le protocole utilisé est Zot. Ce dernier permet d'avoir un très haut niveau de sécurité et des paramétrages granulaires, mais aussi de pouvoir cloner son canal très facilement depuis un serveur vers un autres. Cela correspond à une identité nomade.
Il est compatible avec le protocole ActivityPub et Diaspora en activant un plugin. Ce qui lui permet d'être fédéré avec la plupart des réseaux sociaux décentralisés.

