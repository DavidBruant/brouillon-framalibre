---
nom: "Wekan"
date_creation: "Vendredi, 25 janvier, 2019 - 13:40"
date_modification: "Lundi, 10 mai, 2021 - 14:57"
logo:
    src: "images/logo/Wekan.png"
site_web: "https://wekan.github.io/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Un gestionnaire de tâches collaboratif riche en fonctionnalités !"
createurices: ""
alternative_a: "trello"
licences:
    - "Licence MIT/X11"
tags:
    - "kanban"
    - "memo"
    - "gestion de projet"
    - "todo-list"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wekan"
---

Wekan est un gestionnaire de tâches collaboratif en ligne. Il facilite la gestion de projets par l'élaboration de todo listes.
Wekan propose une grande diversité de fonctionnalités, ce qui en fait un service très complet et adapté à des projets complexes. Si vous cherchez un outil kanban plus simple, vous pouvez utiliser Kanboard

