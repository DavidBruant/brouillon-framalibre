---
nom: "Deltacms"
date_creation: "Jeudi, 21 avril, 2022 - 08:35"
date_modification: "lundi, 17 février, 2025 - 11:58"
logo:
    src: "images/logo/Deltacms.gif"
site_web: "https://deltacms.fr"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Deltacms, un CMS sans base de données, libre, simple à installer et à utiliser."
createurices: "Sylvain L., Lionel C."
alternative_a: "Wordpress, Joomla"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "gestionnaire de contenus"
    - "création de site web"
    - "flat-file"
    - "blog"
    - "agenda"
    - "diaporama"
    - "libre"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/deltacms"
---

Deltacms est un gestionnaire de contenus (CMS) sans base de données, qui permet de créer et de maintenir un site internet facilement. Il est complètement dégooglisé et respecte les recommandations W3C HTML5.
Responsif : Deltacms s'adapte automatiquement à toutes les tailles d'écran.
Multilingue :  administration du site en français, anglais ou espagnol, rédaction dans votre langue, traduction rédigée facilitée.
Multi utilisateurs :  visiteur, membre pour l'accès à des pages privées, éditeur, modérateur ou administrateur pour des droits étendus.
Personnalisable: Vous pouvez choisir votre thème à l'installation, le personnaliser, le sauvegarder, importer un thème.
Modulaire : Deltacms dispose de nombreux modules qui permettent d'enrichir vos pages ( blog, album photo, agenda, diaporama, statistiques,...).
