---
nom: "dokuwiki"
date_creation: "Mercredi, 4 janvier, 2017 - 02:03"
date_modification: "Mercredi, 7 septembre, 2022 - 12:34"
logo:
    src: "images/logo/dokuwiki.png"
site_web: "https://www.dokuwiki.org/dokuwiki"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "DokuWiki est un moteur de wiki Libre, modulable, multilingue et sans base de données propulsé par PHP."
createurices: "Andreas Gohr"
alternative_a: "Mediawiki"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "wiki"
    - "documentation"
    - "lms"
    - "auto-hébergement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/DokuWiki"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dokuwiki"
---

DokuWiki vous permettra de mettre en place une documentation facile d'accès. Ce CMS gère les utilisateurs, l'historique des versions, la gestion des médias et/ou documents ainsi qu'un système de plugins très performant.
DokuWiki s'installe sur tout environnement supportant PHP (serveur en ligne, poste personnel pour un partage sur le réseau domestique) sans avoir besoin de gérer une base de données car tout est stocké sur des pages au format 'txt' : cet aspect en fait un des CMS Wiki les plus simple à installer et à utiliser.
La syntaxe spécifique (proche du MarkDown) permet également de lire les pages hors-ligne, avec un simple éditeur de texte.
DokuWiki est une excellente alternative à MediaWiki.

