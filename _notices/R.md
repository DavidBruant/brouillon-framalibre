---
nom: "R"
date_creation: "Mardi, 10 février, 2015 - 14:18"
date_modification: "lundi, 6 janvier, 2025 - 20:44"
logo:
    src: "images/logo/R.png"
site_web: "https://www.r-project.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "R est un logiciel de traitement de données et d'analyse statistique"
createurices: "Robert Gentleman, Ross Ihaka"
alternative_a: "SAS, eviews, Microsoft Excel, Stata"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "analyse statistique"
    - "traitement de données"
    - "sociologie"
    - "géographie"
    - "économie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/R_%28langage_de_programmation_et_environnement_sta…"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/r"
---

R est un logiciel et un langage de programmation, qui permet de traiter des données et de procéder à une analyse statistique.
Sa popularité au sein des centres de recherche en fait un logiciel dont le développement est très actif. Ses très nombreux paquets que vous pouvez facilement installer, vous offriront tout ce dont vous pouvez rêver de faire, de l'importation de formats exotiques aux méthodes statistiques des plus complexes !
Une fois installé, vous pouvez installer une autre interface comme Rstudio afin de bénéficier de la facilité d'un Environnement de développement (IDE).
