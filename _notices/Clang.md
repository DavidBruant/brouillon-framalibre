---
nom: "Clang"
date_creation: "Mercredi, 23 janvier, 2019 - 20:17"
date_modification: "Lundi, 10 mai, 2021 - 13:17"
logo:
    src: "images/logo/Clang.png"
site_web: "https://clang.llvm.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Clang est un compilateur multi-langage."
createurices: ""
alternative_a: "MSVC"
licences:
    - "Licence Open Source NCSA/Université de l'Illinois (NCSA)"
tags:
    - "développement"
    - "compilateur c"
    - "compilateur c++"
    - "compilation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Clang"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/clang"
---

Clang est un compilateur multi-langage (C, C++, Objective C/C++, OpenCL, CUDA, RenderScript) et multi-platforme compatible avec GCC et Clang utilisant LLVM pour l’optimisation et la génération du binaire final.

