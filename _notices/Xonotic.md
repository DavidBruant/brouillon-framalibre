---
nom: "Xonotic"
date_creation: "Dimanche, 2 avril, 2017 - 00:08"
date_modification: "Vendredi, 7 mai, 2021 - 11:06"
logo:
    src: "images/logo/Xonotic.png"
site_web: "https://www.xonotic.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Le meilleur d'Unreal Tournament avec le dynamisme de Quake III. Un FPS à tester de toute urgence !"
createurices: ""
alternative_a: "Unreal Tournament, Quake, Doom, Nexuiz par Illfonic"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "fps"
    - "multi-joueur"
    - "tir"
    - "action"
lien_wikipedia: "https://en.wikipedia.org/wiki/Xonotic"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xonotic"
---

Xonotic, fork de Nexuiz, propose de nombreux modes de jeu et modificateurs, pour une variété de gameplay très appréciable. Lançable par réseau sans installation, c'est un incontournable des LAN, ou des pause café entre collègues.

