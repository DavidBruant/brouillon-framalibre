---
nom: "Swiftnotes"
date_creation: "Mercredi, 4 janvier, 2017 - 12:26"
date_modification: "Samedi, 30 octobre, 2021 - 16:02"
logo:
    src: "images/logo/Swiftnotes.png"
site_web: "https://github.com/adrianchifor/Swiftnotes"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Application de prise de note simple et efficace. N'est plus maintenu depuis 2018."
createurices: "Adrian Chifor"
alternative_a: "Google Keep"
licences:
    - "Licence Apache (Apache)"
tags:
    - "bureautique"
    - "notes"
    - "prise de notes"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/swiftnotes"
---

Application Android de prise de note aux fonctionnalités très simples et ergonomique :
titre + Corps de note,
organiser ses notes (couleurs, favoris),
recherche,
sauvegarde et restauration,
changer la taille de police.

