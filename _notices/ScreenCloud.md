---
nom: "ScreenCloud"
date_creation: "Samedi, 12 août, 2017 - 16:56"
date_modification: "Samedi, 12 août, 2017 - 16:56"
logo:
    src: "images/logo/ScreenCloud.png"
site_web: "https://screencloud.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Réalisez des captures d'écran grâce à ScreenCloud."
createurices: "Olav Sortland Thoresen"
alternative_a: "Snagit, WinSnap"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "capture d'écran"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/screencloud"
---

ScreenCloud est un logiciel de capture d'écran multi-plateforme. Il s'agit du client du service web du même nom.
ScreenCloud permet de capturer la totalité de l'écran, la fenêtre en premier plan, ou une zone précise, le tout grâce à des raccourcis clavier. Le logiciel offre la possibilité d'héberger les captures sur son portail, à l'aide d'un compte mais fournit aussi d'autres plateformes comme Imgur, FTP, etc. Il est possible d'ajouter d'autres services tiers comme Nextcloud/Owncloud, Lutim, etc.

