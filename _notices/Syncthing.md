---
nom: "Syncthing"
date_creation: "Dimanche, 8 janvier, 2017 - 17:00"
date_modification: "mardi, 11 février, 2025 - 09:23"
logo:
    src: "images/logo/Syncthing.png"
site_web: "https://syncthing.net/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Syncthing est un logiciel libre de synchronisation de fichiers respectant la vie privée"
createurices: "Jakob Borg"
alternative_a: "Google Drive, Dropbox, Microsoft OneDrive"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "synchronisation"
    - "sauvegarde"
    - "manipulation de fichier"
    - "vie privée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Syncthing"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/syncthing"
---

Syncthing est un logiciel libre permettant de synchroniser les répertoires de plusieurs dispositifs (PC, tablette, smartphone, etc.) à travers Internet au moyen d'une connexion pair à pair sécurisée en TLS.
L’intérêt de Syncthing est que vous n'avez pas à faire confiance à un tiers, puisque vos fichiers sont hébergés uniquement sur vos dispositifs, et non sur l'ordinateur de quelqu'un d'autre (à l'inverse de Dropbox et ses équivalents, où vos fichiers sont stockés sur leurs serveurs).
