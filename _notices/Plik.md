---
nom: "Plik"
date_creation: "Dimanche, 26 décembre, 2021 - 19:47"
date_modification: "Dimanche, 26 décembre, 2021 - 19:51"
logo:
    src: "images/logo/Plik.png"
site_web: "https://plik.root.gg"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "le web"
langues:
    - "English"
description_courte: "Plik est une application web de partage de fichiers avec un délai d'expiration."
createurices: "Mathieu Bodjikian, Charles-Antoine Mathieu"
alternative_a: "ImageShack, Imgur, Mega"
licences:
    - "Licence MIT/X11"
tags:
    - "transfert de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/plik"
---

Plik permet de déposer des fichiers arbitraires sur un serveur (pas uniquement des images) avec un délai d'expiration. Chaque dépot donne une URL unique qui peut être partagée facilement.
Chaque dépot peut contenir plusieurs fichiers. On peut télécharger un zip des fichiers d'un dépot. Attention : les fichiers ne sont pas chiffrés sur le serveur ; si vous voulez les protéger, utilisez GPG avant de déposer des fichiers.
Une instance de plik peut être configurée pour n'autoriser que certains utilisateurs à verser de nouveaux fichiers, ainsi que d'autres options. Plik est destiné à être installé sur un serveur web, mais des instances publiques existent.

