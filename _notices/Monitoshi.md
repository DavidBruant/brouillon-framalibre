---
nom: "Monitoshi"
date_creation: "Mercredi, 22 mars, 2017 - 11:18"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo:
    src: "images/logo/Monitoshi.png"
site_web: "http://lexoyo.me/monitoshi-website/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Apple iOS"
langues:
    - "English"
description_courte: "Soyez le premier à savoir quand votre site est hors ligne."
createurices: "Alex Hoyau"
alternative_a: "Pingdom, Monitis"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "monitoring"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/monitoshi"
---

Monitoshi est un outil très simple et efficace. Pas besoin de créer un compte ni de paramétrer quoi que ce soit. Vous entrez l'adresse de votre site et votre adresse mail, et voila! Monitoshi commence à observer votre site et vous envoie un email dès qu'il tombe :)

