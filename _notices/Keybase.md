---
nom: "Keybase"
date_creation: "Lundi, 4 novembre, 2019 - 22:19"
date_modification: "Lundi, 4 novembre, 2019 - 22:19"
logo:
    src: "images/logo/Keybase.png"
site_web: "https://keybase.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "English"
description_courte: "Keybase est un service qui tend à simplifier  l'utilisation du chiffrement de vos correspondances grâce à PGP,"
createurices: "Chris Coyne, Max Krohn"
alternative_a: "Slack"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "chat"
    - "sécurité"
    - "réseau social"
lien_wikipedia: "https://en.wikipedia.org/wiki/Keybase"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/keybase"
---

Il vous propose d'authentifier vos divers comptes sociaux et sites web, que ce soit twitter, reddit, Mastodon , assurant à vos contacts l'authenticité de votre profil Keybase, ils pourront signer votre clé publique en suivant votre profil keybase, tout ça le plus simplement du monde.
Le site en lui-même Keybase.io est un client (logiciel) pour Keybase, vous permettant de faire en ligne certaines actions, tel que chiffrer/déchiffrer vos messages ainsi que de partager à la manière d'un dropbox ou d'un google drive, vos fichiers avec vos tiers toujours en assurant la provenance de ceux-ci.
Faisons le tour de ce service qui a aujourd'hui bien évolué et qui se destine à devenir une référence dans la préservation de votre vie privée et l'accessibilité pour tous au chiffrement.

