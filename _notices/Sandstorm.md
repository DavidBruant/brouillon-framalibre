---
nom: "Sandstorm"
date_creation: "Mercredi, 28 décembre, 2016 - 11:45"
date_modification: "Samedi, 7 janvier, 2017 - 03:28"
logo:
    src: "images/logo/Sandstorm.png"
site_web: "https://sandstorm.io/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "English"
description_courte: "Sandstorm est une plate-forme d’auto-hébergement d’applications web virtualisées et sécurisées."
createurices: "Kenton Varda, Jade Wang"
alternative_a: "G Suite, Google Docs, Google Apps"
licences:
    - "Licence Apache (Apache)"
tags:
    - "auto-hébergement"
    - "partage"
    - "réseau"
    - "décentralisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sandstorm"
---

Sandstorm repose sur une architecture originale proposant un système d'identification unique et l'installation en 1 clic d'une soixantaine d'applications web tournant sous Linux comme EtherCalc, GitLab, Etherpad, Wekan, Rocket.Chat, etc.
Chacune peut être instanciée en un ou plusieurs grains — documents ou services — isolés les uns des autres. Si l'un d'entre eux dysfonctionne les autres ne sont pas affectés. Sandstorm propose un système de partage des grains entre usagers et de liaison entre ces grains.
A noter que la politique de sécurité drastique limite les échanges réseaux avec l'extérieur. Sandstorm cantonne la publication web à des sites statiques (Wordpress est proposé avec un cache et sans gestion des mises à jours, des extensions et des commentaires). Il est surtout utile pour des activités personnelles et de collaborations.
La documentation est de bonne qualité. L'installation du système est aisée et il est relativement facile de créer de nouvelles applications.

