---
nom: "Q Light Controller+"
date_creation: "Dimanche, 29 décembre, 2019 - 17:09"
date_modification: "mardi, 27 août, 2024 - 23:42"
logo:
    src: "images/logo/Q Light Controller+.png"
site_web: "https://www.qlcplus.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un pupitre lumière très simple à utiliser, et très complet. Il est compatible DMX, ArtNet, OSC, MIDI."
createurices: "Massimo Callegari"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "lumière"
    - "DMX"
    - "VJ"
    - "VJing"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/q-light-controller"
---

Un pupitre lumière très simple à utiliser, et très complet. Il est compatible DMX, ArtNet, OSC, MIDI.
Il peut utiliser pleins d'interfaces DMX différentes. Il est utilisable via un pupitre traditionnel, via des séquences ou via un pupitre entièrement personnalisable.
