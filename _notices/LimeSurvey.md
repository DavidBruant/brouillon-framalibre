---
nom: "LimeSurvey"
date_creation: "Mardi, 28 novembre, 2017 - 14:28"
date_modification: "Lundi, 29 octobre, 2018 - 11:34"
logo:
    src: "images/logo/LimeSurvey.png"
site_web: "https://www.limesurvey.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Le premier logiciel open source pour la construction de questionnaires en ligne."
createurices: "LimeSurvey Gmbh et contributeurs"
alternative_a: "Google Docs, SurveyMonkey"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "enquête"
    - "sondage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LimeSurvey"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/limesurvey"
---

LimeSurvey permet de construire n‘importe quel type de questionnaire en ligne, du sondages le plus simple au formulaire complexe avec contrôle des données.
LimeSurvey est
multilingue (plus de 80 langues supportées), 
permet une gestion fine des droits d‘administration,
28 types de questions intégrées,
paramètres avancés sur chaque questions pour un meilleur contrôle des données,
gestion fines des filtres et conditions d‘affichage de chaque questions,
texte et calcul dynamique durant le passage du questionnaire,
gestion de quotas,
système d‘évaluation et quizz intégré,
accès complet à la présentation publique du questionnaire (thème),
système de statistiques intégrées,
possibilité d‘export au formats les plus courant (csv, R, stata, xls …)
extensible par l‘ajout de plugins,
…

