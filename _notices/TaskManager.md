---
nom: "TaskManager"
date_creation: "Vendredi, 20 décembre, 2019 - 12:40"
date_modification: "jeudi, 4 avril, 2024 - 13:49"
logo:
    src: "images/logo/TaskManager.png"
site_web: "https://www.mentdb.org/index.jsp?app_page=app_task_manager"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Gestionnaire de tâche collaboratif"
createurices: "Jimmitry Payet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "tâche"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/taskmanager"
---

TasKManager est un gestionnaire de tâche complet et gratuit fait pour vous. Cette application vous permettra de créer des clients, des projets et d'affecter vos projets à vos clients. Une gestion des utilisateurs est disponible et vous pourrez décider si un utilisateur aura accès à un client ou un projet. La fonctionnalité principale est bien sûr la création de tâche. Une tâche peut être en cours, en pause, terminé... et bien d'autres états. Les tâches font partie des projets, et donc partageable entre utilisateur. C'est aussi un gestionnaire de tâche collaboratif! En effet si une autre entreprise possède ce même gestionnaire de tâche, vous pourrez intégrer dans votre gestionnaire de tâche, des projets en provenance d'un autre gestionnaire de tâche identique à ce TaskManager. Il devient simple de partager des projets entre entreprise.
