---
nom: "Ghost"
date_creation: "Dimanche, 15 juillet, 2018 - 14:36"
date_modification: "Mardi, 11 mai, 2021 - 23:18"
logo:
    src: "images/logo/Ghost.png"
site_web: "https://ghost.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Ghost est un moteur de blog"
createurices: "John O'Nolan"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "blog"
    - "microblog"
    - "publication"
    - "hébergement"
lien_wikipedia: "https://en.wikipedia.org/wiki/Ghost_(blogging_platform)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ghost"
---

L'idée de Ghost a été écrite pour la première fois début novembre 2012 dans un billet de blog par le fondateur John O'Nolan, ancien responsable de l'équipe interface utilisateur de WordPress, à la suite de sa frustration face à la complexité d'utilisation de WordPress en tant que moteur de blog plutôt qu'en tant que système de gestion de contenu.
À la suite d'une demande considérable et de retours positifs de la part de la communauté sur le billet de blog initial, O'Nolan recrute son amie de longue date Hannah Wolfe pour l'aider à créer un premier prototype.

