---
nom: "OpenVPN for Android"
date_creation: "Jeudi, 5 janvier, 2017 - 09:34"
date_modification: "Mardi, 10 janvier, 2017 - 03:40"
logo:
    src: "images/logo/OpenVPN for Android.png"
site_web: "http://ics-openvpn.blinkt.de/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "OpenVPN for Android est un client Android pour OpenVPN permettant de paramétrer un serveur OpenVPN."
createurices: "Arne Schwabe"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "vpn"
    - "réseau"
    - "sécurité"
    - "tunnel"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openvpn-android"
---

OpenVPN for Android est un port Android du projet libre OpenVPN.
Il permet de paramétrer une connexion à un serveur OpenVPN (auto-hébergé ou fournisseur externe) sans être obligatoirement titulaire des droits "root".
Il est téléchargeable sur différentes plateformes (F-Droid, Google Play, Amazon Store)

