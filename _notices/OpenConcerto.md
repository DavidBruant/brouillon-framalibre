---
nom: "OpenConcerto"
date_creation: "Mercredi, 28 décembre, 2016 - 08:18"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/OpenConcerto.png"
site_web: "http://www.openconcerto.org/fr/index.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Solutions open source de gestion d’entreprise intégrées et modulaires."
createurices: ""
alternative_a: "EBP, CIEL"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "crm"
    - "point de vente"
    - "gestion commerciale"
    - "comptabilité"
    - "commerce"
    - "devis"
    - "caisse enregistreuse"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenConcerto"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openconcerto"
---

OpenConcerto vous offre une solution modulaire professionnelle vous permettant de gérer vos clients, vos fournisseurs et vos prospects, mais aussi de générer vos Devis, Facture.
Un module de comptabilité complet (y compris analytique) est disponible.
Un module vous permet aussi de générer les fiches de paie (gestion de profil, écriture automatique du journal de paie en comptabilité).
Un module « Point de Vente » vous permet d’avoir un module de caisse (gestion des ventes et tickets de caisse).

