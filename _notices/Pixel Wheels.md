---
nom: "Pixel Wheels"
date_creation: "samedi, 30 décembre, 2023 - 10:53"
date_modification: "samedi, 30 décembre, 2023 - 12:18"
logo:
    src: "images/logo/Pixel Wheels.png"
site_web: "https://agateau.com/projects/pixelwheels"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Jeu de course vu de dessus, avec des graphismes en pixel-art"
createurices: "Aurélien Gâteau"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence Apache (Apache)"
tags:
    - "course"
    - "jeu"
    - "arcade"
    - "jeu de voiture"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.agateau.tinywheels.android/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

Pixel Wheels est un jeu de course d'arcade vu de dessus, avec des graphismes pixel-art rappelant l'époque des machines 16 bits. Faites la course pour arriver le premier sur des pistes variées, ramassez des bonus pour améliorer votre classement ou ralentir les autres concurrents !

