---
nom: "Index-AI"
date_creation: "Vendredi, 14 août, 2020 - 08:29"
date_modification: "Vendredi, 14 août, 2020 - 08:30"
logo:
    src: "images/logo/Index-AI.png"
site_web: "https://www.mentdb.org/index_ai.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Le moteur de recherche orienté professionnels"
createurices: "Jimmitry Payet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/index-ai"
---

Index-AI est un moteur de recherche un peu à la façon Google Search. Il vous permet de rechercher avec une petite puissance algorithmique: des entreprises, des services ou autre en configurant le moteur. Le système est complètement multi-thread, et optimiser pour travailler en mémoire. Vous pourrez y intégrer 10.000.000 de questions dans seulement 7Go de RAM. Le système dispose aussi d'API vous permettant  d'intégrer l'index dans vos applications WEB de manière sécurisé. Il n'y a pas que Google qui sait faire de puissante recherche ... Vous pourrez aussi le faire!

