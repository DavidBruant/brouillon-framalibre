---
nom: "OCS Inventory"
date_creation: "Lundi, 20 septembre, 2021 - 10:41"
date_modification: "Lundi, 20 septembre, 2021 - 15:12"
logo:
    src: "images/logo/OCS Inventory.png"
site_web: "https://ocsinventory-ng.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Open Computers and Software Inventory est une solution libre de gestion technique de parc informatique."
createurices: "Erwan Goalou, Pascal Danek, Didier Liroulet, Jean-Côme Estienney"
alternative_a: "SCCM, Samanage, Apple MDM"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "gestion de parc informatique"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OCS_Inventory"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ocs-inventory"
---

OCS Inventory utilise un agent qui lance un inventaire sur les ordinateurs clients, et un serveur de gestion qui centralise les résultats des inventaires. La console d’administration web permet de visualiser ces résultats d’inventaires, les matériels réseau détectés et de pouvoir créer des paquets de déploiement.
Le logiciel vous assiste également dans la connaissance des failles de sécurité subsistant sur vos machines ainsi que dans la bonne gestion de la conformité de vos licences.

