---
nom: "WordPress"
date_creation: "Dimanche, 26 mars, 2017 - 16:01"
date_modification: "mardi, 2 avril, 2024 - 11:26"
logo:
    src: "images/logo/WordPress.png"
site_web: "https://fr.wordpress.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Le CMS le plus utilisé au monde"
createurices: "Matthew Mullenweg, Mike Little"
alternative_a: "wix, Blogspot, blogger"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "gestionnaire de contenus"
    - "création de site web"
    - "blog"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WordPress"
lien_exodus: ""
identifiant_wikidata: "Q13166"
mis_en_avant: "oui"
redirect_from: "/content/wordpress"
---

WordPress est un CMS - ou gestionnaire de contenus - écrit en PHP. Il permet de créer plusieurs types de site différents : blog, vitrine, portfolio, e-commerce, etc. Son intérêt principal réside dans l'importance de la communauté qui lui est rattachée, du nombre des extensions et des thèmes que vous pouvez vous procurer gratuitement.
