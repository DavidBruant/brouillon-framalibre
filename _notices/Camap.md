---
nom: "Camap"
date_creation: "Samedi, 17 juin, 2017 - 01:08"
date_modification: "mardi, 10 octobre, 2023 - 16:39"
logo:
    src: "images/logo/Camap.png"
site_web: "http://camap.amap44.org/"
plateformes:
    - "Web"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "L'application libre de gestion d'AMAP"
createurices: "AMAP de Bordeaux Nansouty"
alternative_a: "La ruche qui dit oui"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "amap"
    - "circuit court"
    - "économie"
    - "agriculture"
    - "gestion"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

__Camap__ est la version libre (license GPL) et dédiée aux AMAP de l'application cagette.net.

Camap propose: 

- La gestion des membres,

- La gestion des adhésions,

- Des contrats AMAP “Classique”: une même commande chaque semaine durant tout le contrat (panier de légume par exemple),

- Des contrats AMAP “Variable": une commande pouvant être différente à chaque distribution avec gestion d'une commande par défaut (pour le pain ou les produits laitiers par exemple),

- La gestion des permanences: calendrier de permanence, inscription des adhérents, rappels automatiques,

- Une messagerie intégrée avec des listes de diffusion dynamiques (pas ex: adhérents contrat légumes, ou membres sans commande etc),

- La gestion des périodes d'absence des adhérents dans les contrats AMAP,

- La gestion des soldes et l'enregistrement des paiements contrat par contrat,

- L'édition des feuilles d'émargement,

- Des droits d'accès paramétrables (Administrateur, Gestion d'un ou des contrats, Messagerie, Membre, …),

- …

Bref, toutes les problématiques de fonctionnement d'une AMAP peuvent être gérées avec Camap, afin de simplifier la vie des producteurs, coordinateurs/référents et des mangeurs.

Le développement de Camap est suivi par l'association InterAMAP44 et se fait de manière collaborative, en fonction des besoins et suggestions des utilisateurs. Le code source est publié sur [GitHub](https://github.com/CAMAP-APP). L'InterAMAP44 propose une [offre d'hébergement mutualisée](https://amap44.org/camap) aux AMAP.
