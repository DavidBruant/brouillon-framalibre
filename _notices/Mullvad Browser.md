---
nom: "Mullvad Browser"
date_creation: "mardi, 25 juin, 2024 - 20:36"
date_modification: "mardi, 25 juin, 2024 - 20:36"
logo:
    src: "images/logo/Mullvad Browser.webp"
site_web: "https://mullvad.net/fr/browser"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:

description_courte: "Mullvad Browser est un navigateur Web doté de la qualité de confidentialité du navigateur Tor"
createurices: "Mullvad VPN AB"
alternative_a: "Google Chrome"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "navigateur web"
    - "vie privée"
    - "firefox"
    - "VPN"
lien_wikipedia: "https://en.wikipedia.org/wiki/Mullvad#Browser"
lien_exodus: ""
identifiant_wikidata: "Q125401073"
mis_en_avant: "non"

---

Mullvad Browser est développé en collaboration entre Mullvad VPN et le projet Tor, pour minimiser le suivi et le profilage. Il est à utiliser avec un VPN digne de confiance

Le Projet Tor a fait ses preuves dans la construction d'un navigateur axé sur la protection de la vie privée. Mullvad Browser dispose de la même protection contre le profilage que le navigateur Tor et propose des réglages prêts à l'emploi. Le mode privé est activé ainsi que el blocage des traceurs et les cookies de tiers.

