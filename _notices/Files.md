---
nom: "Files"
date_creation: "Jeudi, 29 juin, 2023 - 20:38"
date_modification: "Jeudi, 29 juin, 2023 - 22:49"
logo:
    src: "images/logo/Files.png"
site_web: "https://files.community/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un gestionnaire de fichiers moderne"
createurices: "Marcel Wagner & Michael West"
alternative_a: "Explorateur de fichiers windows"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "explorateur de fichier"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/files"
---

Files, l'application de gestion de fichiers ultime pour Windows. Avec son design élégant et intuitif, naviguer dans vos fichiers n'a jamais été aussi facile. Les fichiers comportent des onglets pour basculer facilement entre différents dossiers, une vue en colonnes pour une navigation rapide des fichiers et une prise en charge du double volet pour une gestion efficace des fichiers. De plus, vous pouvez facilement créer et extraire des archives en quelques clics, ce qui facilite la compression et la décompression des fichiers.
Files offre également des fonctionnalités avancées telles que le balisage de fichiers pour une organisation facile, la prise en charge de QuickLook po

