---
nom: "Yorg"
date_creation: "Lundi, 15 janvier, 2018 - 10:44"
date_modification: "mardi, 11 juin, 2024 - 00:35"
logo:
    src: "images/logo/Yorg.png"
site_web: "https://ya2.itch.io/yorg"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Yorg (Yorg's an Open Racing Game) est un jeu de course open source adoptant une vue de dessus."
createurices: "ya2"
alternative_a: "Micro Machines"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "course"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/yorg"
---

Yorg est un jeu de course en vue du dessus utilisant le moteur de jeu Panda3D. On y trouve de nombreux style de pilotes, voitures et circuits. Le but est de terminer une course en se situant en première position. Pour cela,  vous pourrez utiliser des objets bénéfiques ou des armes ayant pour but de déstabiliser sur un court temps les autres pilotes. À vos volants ou manettes puisque le support des joysticks est aussi de la partie.
