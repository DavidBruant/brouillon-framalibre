---
nom: "wallabag"
date_creation: "Samedi, 10 décembre, 2016 - 21:38"
date_modification: "Samedi, 23 septembre, 2017 - 13:18"
logo:
    src: "images/logo/wallabag.png"
site_web: "https://www.wallabag.org/"
plateformes:
    - "Windows"
    - "Android"
    - "Windows Mobile"
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "wallabag permet de sauvegarder des articles puis de les lire plus tard, sur n'importe quel appareil."
createurices: "Nicolas Lœuillet, Thomas Citharel, Jérémy Benoist"
alternative_a: "Pocket, Instapaper, Readability"
licences:
    - "Licence MIT/X11"
tags:
    - "stockage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Wallabag"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/wallabag"
---

wallabag est une application libre à héberger sur serveur permettant de sauvegarder des articles puis de les lire plus tard, sur n'importe quel appareil (navigateur, smartphone, tablette ou liseuse), que vous soyez connecté ou non.
wallabag extrait le contenu — et uniquement le contenu — des articles et l'affiche de manière agréable et épurée. Il est possible de lire ses articles sauvegardés dans son wallabag depuis son smartphone et sa tablette (Android, iOS et Windows) ou même certains modèles de liseuses.
Vous pouvez organiser vos articles avec tags, les filtrer par métadonnées, ou les rechercher. Il est également possible d'annoter les articles dans le texte.
Il est possible d'importer ses données depuis Pocket, Readability, Instapaper, Pinboard, Firefox ou Chrome.

