---
nom: "LessPass"
date_creation: "Mercredi, 22 mars, 2017 - 01:37"
date_modification: "Mercredi, 12 mai, 2021 - 15:44"
logo:
    src: "images/logo/LessPass.png"
site_web: "https://lesspass.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
langues:
    - "English"
description_courte: "Retenez un seul mot de passe pour gérer tout les autres, depuis n'importe où, sans rien avoir à synchroniser."
createurices: "Guillaume Vincent, Édouard Lopez"
alternative_a: "1password, lastpass, dashlane"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "mot de passe"
    - "chiffrement"
    - "extension"
    - "add-on"
    - "navigateur web"
    - "gestionnaire de mots de passe"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lesspass"
---

LessPass est un Gestionnaire de mot de passe à part, car il ne stocke pas vos mots de passe mais vous permet de générer des mots de passe uniques pour chaque service, et de les régénérer à l'identique pour votre prochaine visite, à partir de votre mot de passe principal.
Plus de perte de temps à synchroniser votre trousseau crypté. Retenez un mot de passe maître et accédez à tous vos mots de passes de n'importe où, quand vous voulez, même sans connexion internet si vous avez téléchargé l’application.
Comment ça marche techniquement

