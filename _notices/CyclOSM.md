---
nom: "CyclOSM"
date_creation: "vendredi, 7 juillet, 2023 - 11:58"
date_modification: "dimanche, 5 janvier, 2025 - 15:19"
logo:
    src: "images/logo/CyclOSM.png"
site_web: "https://www.cyclosm.org/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Rendu cartographique se basant sur les données OpenStreetMap facilitant la visualisation des aménagements et équipements cyclables (voies, magasins, arceaux…)."
createurices: ""
alternative_a: "Geovelo"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "OpenStreetMap"
    - "cyclable"
    - "cartographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

CyclOSM est un rendu cartographique mettant en avant les aménagements et équipements cyclables et se basant sur les données [OpenStreetMap](https://www.openstreetmap.org/). Il valorise notamment :
- Tout chemin utilisable par un cycliste, avec un style combinant :
  - la nature du chemin (route résidentielles, chemin agricole...)
  - celui de l'aménagement (des traitillés fin pour les bandes cyclables, traitillés plus épais pour les voies vertes, des variations de couleurs…)
  - les restrictions (les chemins inaccessibles sont grisés, ceux dont la vitesse y est faible sont colorés).
- Les équipements :
  - les utilitaires : parkings pour vélos, stations de gonflage ;
  - les commodités : toilettes, tables de pique-nique, magasins de cycle, aires de camping ;
- Les parcours de randonnées, les courbes et l'ombrage topographique.


Le service ne propose pas de routage. Sa priorité est la visualisation, mais d'autres outils qui s'appuient aussi sur les données OpenStreetMap le permettent.

Un rendu supplémentaire est proposé, « CyclOSM lite », une simple couche transparente sur laquelle n'apparaissent que les voies cyclables.
