---
nom: "Teeworlds"
date_creation: "Samedi, 24 décembre, 2016 - 03:36"
date_modification: "Vendredi, 24 novembre, 2017 - 04:15"
logo:
    src: "images/logo/Teeworlds.png"
site_web: "https://www.teeworlds.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un petit shooter en 2D cartoonesque très simple à prendre en main en LAN ou via le net."
createurices: "Magnus Auvinen"
alternative_a: ""
licences:
    - "Licence Zlib"
tags:
    - "jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Teeworlds"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/teeworlds"
---

Teeworlds, anciennement Teewars, est un jeu de tir TPS (third person shooter) multijoueur en 2D. 
Le joueur y incarne une petite créature ronde, le tee, qui utilise un grappin pour se déplacer. 
À l'aide de plusieurs armes différentes, le joueur doit parcourir les différentes cartes en équipe ou seul, pour voler le drapeau à ses adversaires, ou tous les tuer, selon le mode de jeu !
Le jeu incorpore un éditeur de carte qui s’ouvre (et se ferme) avec Ctrl+Maj+E.
Le serveur (inclus) permet d’héberger des parties CTF (capture de drapeau), DM (combat chacun pour soi) et TDM (combat par équipe). 
Le client par défaut, lui, permet en plus de se connecter à des serveurs alternatifs (le plus souvent libres aussi) proposant d’autres modes de jeu : DDRace (jeu d’adresse – type parkour – en binôme), zCatch (jouer à chat), infclass (survivants contre zombies), OpenFNG (congelez puis sacrifiez vos adversaires) et de nombreux autres.

