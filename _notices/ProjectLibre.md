---
nom: "ProjectLibre"
date_creation: "Mercredi, 5 avril, 2017 - 13:23"
date_modification: "Mercredi, 24 mai, 2017 - 00:32"
logo:
    src: "images/logo/ProjectLibre.gif"
site_web: "http://www.projectlibre.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de gestion de projet"
createurices: ""
alternative_a: "Microsoft Project"
licences:
    - "Common Public License (CPL)"
tags:
    - "métiers"
    - "gestion de projet"
    - "gantt"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ProjectLibre"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/projectlibre"
---

ProjectLibre est un outil de gestion de projet. Il permet de créer des diagrammes de Gantt et de les voir sous plusieurs formats. Il gère les ressources et permet plusieurs représentations de ces dernières. Fort de son succès (plus de 2 millions de téléchargements), la société éditrice prépare une offre « cloud ».
ProjectLibre est compatible avec Microsoft Project, il lit les formats MS Project version 2003, 2007 et 2010.

