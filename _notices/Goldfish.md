---
nom: "Goldfish"
date_creation: "Mardi, 12 juillet, 2022 - 22:56"
date_modification: "Mardi, 9 août, 2022 - 13:56"
logo:
    src: "images/logo/Goldfish.png"
site_web: "https://goldfish.social/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "English"
description_courte: "L'alternative libre à TikTok."
createurices: "Stux"
alternative_a: "TikTok"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/goldfish"
---

Goldfish est un logiciel libre écrit en Laravel qui permet de publier ees vidéos courtes comme sur TikTok disponible sur le Web. Aussi, on peut les commenter, liker et les partager. Enfin, on peut y trouver des nouveaux utilisateurs.

