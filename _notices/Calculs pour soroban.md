---
nom: "Calculs pour soroban"
date_creation: "Dimanche, 14 janvier, 2018 - 16:59"
date_modification: "Lundi, 10 mai, 2021 - 14:14"
logo:
    src: "images/logo/Calculs pour soroban.png"
site_web: "https://gitlab.adullact.net/zenjo/Calculs_pour_soroban"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Obtenir de grandes feuilles de calcul pour soroban sur ordinateur ou des petites sur tablette ou smartphone."
createurices: "Robert Sebille"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "calcul"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/calculs-pour-soroban"
---

Qu’est que le soroban ?
Le soroban est un abaque, plus précisément le boulier compteur japonais. Le soroban mène à l’anzan, c-à-d au calcul mental sur base d’une représentation mentale du soroban. Soroban et anzan sont redoutablement efficaces et permettent avec de l’entraînement d’atteindre des vitesses impressionnantes. Pour les même calculs, les personnes entraînées dépasseront nettement la vitesse d’une personne équipée d’une calculatrice électronique. Au japon, le soroban est un sport ou un art qui est enseigné dans de nombreuses écoles spécialisées, et donne lieu à des concours. Le logiciel a été construit à la fois pour pouvoir produire de grandes feuilles avec de nombreux calculs sur ordinateur, et néanmoins être convivial avec les smartphones et les tablettes
Soroban - All in the mind
Soroban japonais démonstration

