---
nom: "ESUP Pod"
date_creation: "Vendredi, 17 décembre, 2021 - 20:16"
date_modification: "lundi, 18 mars, 2024 - 09:10"
logo:
    src: "images/logo/ESUP Pod.svg"
site_web: "https://pod.esup-portail.org"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Esup Pod est une plate-forme de diffusion de vidéos adaptée spécialement pour l'accès au savoir."
createurices: ""
alternative_a: "Youtube, Dailymotion, Vimeo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "vidéo"
    - "université"
    - "base de connaissances"
    - "savoir libre"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/esup-pod"
---

Esup Pod est une plateforme web qui permet de visionner des vidéos en ligne. Il y a des fonctions de recherche par discipline et par mots-clefs. On peut également les découvrir par canaux de diffusion et par types de cours. Il est également possible de visionner des flux vidéos en temps réel (streaming).
Le but de la plateforme est d’encourager l’utilisation de vidéos dans le cadre de l’enseignement et de la recherche.
Esup Pod est à la fois un logiciel web que tout un chacun peut installer et une plateforme. Le projet a été initié par l'Université de Lille et est aujourd'hui également soutenu par le Ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation.
