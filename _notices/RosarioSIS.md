---
nom: "RosarioSIS"
date_creation: "Mardi, 6 juin, 2017 - 23:21"
date_modification: "Lundi, 18 avril, 2022 - 00:04"
logo:
    src: "images/logo/RosarioSIS.png"
site_web: "https://www.rosariosis.org/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "RosarioSIS est une webapp (PHP/SQL) pour la gestion d'établissement scolaire (école)."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "erp"
    - "école"
    - "collège"
    - "université"
    - "élève"
    - "enseignement"
    - "notes"
    - "emploi du temps"
    - "comptabilité"
    - "cantine"
    - "communication"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rosariosis"
---

RosarioSIS est une application web libre (open-source) conçue pour faciliter l'administration et la communication des écoles, collèges et pouvant être adapté pour les instituts ou universités (Student Information System en anglais).
Modulaire, RosarioSIS regroupe les outils nécessaire pour la gestion de l'établissement scolaire, ses enseignants, les élèves, leurs emplois du temps, absences, notes, mais aussi les activités scolaires, la discipline, la cantine, la comptabilité et la communication interne.
RosarioSIS propose de nombreux rapports et graphiques pour l'aide à la décision, permet la génération des bulletins de notes en PDF, est traduit en plusieurs langues et peut se connecter au LMS Moodle.

