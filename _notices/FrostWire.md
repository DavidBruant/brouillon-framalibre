---
nom: "FrostWire"
date_creation: "Lundi, 16 janvier, 2017 - 01:57"
date_modification: "Mercredi, 12 mai, 2021 - 15:41"
logo:
    src: "images/logo/FrostWire.png"
site_web: "http://www.frostwire.com/home"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Recherche et téléchargement de torrents, youtube, internet archive, SoundCloud,…"
createurices: ""
alternative_a: "µTorrent, Vuze"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "p2p"
    - "téléchargement"
    - "téléchargement de musique"
    - "téléchargement de youtube"
    - "tracker"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FrostWire"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/frostwire"
---

FrostWire permet de chercher et de télécharger des fichiers provenant de plusieurs sources: bittorrent, via des sites dédiés, Youtube, SoundClound, Archive.org, … Il permet également de prévisualiser les fichiers avant la fin de leur téléchargement, et de naviguer dans sa bibliothèque de fichiers comme un lecteur multimédia.
Il promeut des artistes qui publient leurs œuvres sous licences libres via le site frostclick.com.

