---
nom: "Kohinos"
date_creation: "samedi, 30 mars, 2024 - 08:04"
date_modification: "samedi, 30 mars, 2024 - 08:04"
logo:
    src: "images/logo/Kohinos.png"
site_web: "https://gitlab.com/federation-kohinos/kohinos"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Outil de gestion de Monnaie Locale Complémentaire et Citoyenne"
createurices: "Fédération Kohinos"
alternative_a: "Odoo, Cyclos"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "Monnaie locale"
    - "MLCC"
    - "Économie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Kohinos est une solution numérique Open Source pour les MLCC par les MLCC. 
Bâti sur l'architecture des banques, siège émetteur, régions, agences, il permet :
- d'enregistrer des adhérents et leurs cotisations, 
- de gérer des groupes locaux et des comptoirs d'échange
- d'effectuer le suivi des billets dans la structure
- d'effectuer des transactions numériques, B2B, C2B, C2C
Il possède des interfaces publiques et privées, associées à un site CMS avec templates.
Sont intégrés des modules : de dons, de cotisation et d'achat de monnaie par HelloAsso et par CB (Payzen), d'une caisse de SSA,...
Basé sur le framework Symfony, le logiciel évolue en continu au gré des demandes des monnaies locales.
