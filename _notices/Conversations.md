---
nom: "Conversations"
date_creation: "Dimanche, 15 janvier, 2017 - 22:15"
date_modification: "lundi, 16 septembre, 2024 - 19:55"
logo:
    src: "images/logo/Conversations.png"
site_web: "https://conversations.im/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Conversations est un client android XMPP/Jabber"
createurices: "Daniel Gultsch"
alternative_a: "Messenger, Google Hangouts, Facebook Messenger, WhatsApp"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "vie privée"
    - "android"
    - "xmpp"
    - "jabber"
    - "openpgp"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/conversations"
---

Conversations est un client Android de messagerie instantanée, sécurisé, respectant la vie privée grâce à un chiffrement de bout-en-bout (OMEMO, OTR ou OpenPGP).
Il supporte également l'envoi d'images et de tous types d'objets.
