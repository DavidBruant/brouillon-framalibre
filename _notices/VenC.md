---
nom: "VenC"
date_creation: "lundi, 5 février, 2024 - 11:29"
date_modification: "lundi, 5 février, 2024 - 11:29"
logo:
    src: "images/logo/VenC.svg"
site_web: "https://venc.software"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Générateur de site statique écrit en Python"
createurices: "Denis Salem"
alternative_a: "Hugo, Pelican, Nikola, Jekyll"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "python"
    - "site web statique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

VenC est un logiciel libre écrit en Python conçu pour GNU/Linux, dont le développement a commencé en 2012 pour gérer et créer vos blogs statiques via la console. Avec VenC, tout est fichier texte, il n'y a pas de base de données. La configuration de chaque blog repose sur un unique et petit fichier YAML et les thèmes consistent en une poignée de templates HTML à modifier ou créer soi-même. Les publications quant à elles, se présentent sous la forme d'une partie YAML et d'une autre en Markdown, AsciiDoc ou reStructuredText.

Les blogs statiques sont adaptés aux réseaux de type Deepweb ou pour les personnes qui veulent un contrôle total sur leur site sans s'embarrasser de CMS lourds et potentiellement vulnérables en termes de sécurité. Par ailleurs, l'extrême simplicité de l'organisation des sources des blogs issues de VenC garantit une prise en main rapide et efficace.

---

- [Site et documentation du projet](https://venc.software)
- [Github](https://github.com/DenisSalem/VenC)
- [Framagit](https://framagit.org/denissalem/VenC/)
