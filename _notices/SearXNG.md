---
nom: "SearXNG"
date_creation: "mercredi, 27 novembre, 2024 - 17:20"
date_modification: "mercredi, 27 novembre, 2024 - 17:20"
logo:
    src: "images/logo/SearXNG.svg"
site_web: "https://searxng.org/"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "SearXNG est un métamoteur de recherche libre et décentralisé."
createurices: "SearXNG Team"
alternative_a: "Google Search, Bing, Yahoo!, DuckDuckGo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métamoteur de recherche"
    - "recherche"
    - "recherche web"
    - "décentralisation"
lien_wikipedia: "https://en.wikipedia.org/wiki/SearXNG"
lien_exodus: ""
identifiant_wikidata: "Q107341994"
mis_en_avant: "non"

---

SearXNG est un métamoteur de recherche libre et décentralisé. Celui-ci succède à Searx dont le développement a été arrêté en 2023. SearXNG agrège des résultats de plusieurs moteurs de recherche tout en anonymisant l'utilisateur·rice. Étant décentralisé, SearXNG peut être auto hébergé. Une liste des instances publiques est disponible à l'adresse : https://searx.space/.
