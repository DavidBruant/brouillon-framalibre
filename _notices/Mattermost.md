---
nom: "Mattermost"
date_creation: "Jeudi, 30 août, 2018 - 10:14"
date_modification: "mercredi, 1 janvier, 2025 - 19:21"
logo:
    src: "images/logo/Mattermost.png"
site_web: "https://mattermost.org/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Mattermost est un logiciel décentralisé de communication en équipe."
createurices: ""
alternative_a: "Slack, discord"
licences:
    - "Autre"
tags:
    - "communication"
    - "chat"
    - "irc"
    - "entreprise"
    - "travail collaboratif"
    - "markdown"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mattermost"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/com.mattermost.rn/"
identifiant_wikidata: "Q55478510"
mis_en_avant: "oui"
redirect_from: "/content/mattermost"
---

Mattermost est un  logiciel décentralisé de communication en équipe. Pour les adeptes de Slack, l'importation de vos données est possible, permettant de faire la transition vers cet outil libre en douceur.
La plate-forme permet :
- Le partage de messages et de fichiers sur différents appareils : PC, ordiphones et tablettes grâce aux applications dédiées.
- La mise en forme des messages se fait avec le langage Markdown.
- L'archivage continu et la recherche instantanée, et la prise en charge les notifications et les intégrations avec vos outils existants.
