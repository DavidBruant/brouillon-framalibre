---
nom: "Tux Math"
date_creation: "Vendredi, 28 octobre, 2022 - 09:34"
date_modification: "Vendredi, 28 octobre, 2022 - 09:40"
logo:
    src: "images/logo/Tux Math.png"
site_web: "https://tuxmath.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pour s'entraîner aux additions, soustractions, multiplications et divisions en jouant."
createurices: "Julien MARIN"
alternative_a: "Math Land, Dragon Whiz, logicieleducatif.fr"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "école"
    - "calcul"
    - "enseignement"
    - "pédagogie"
    - "apprentissage"
    - "élève"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tux-math"
---

Ce jeu est une réécriture en JavaScript/web du célèbre logiciel libre Tux math un logiciel éducatif très populaire pour PC.
Une pluie d'astéroïdes s'abat sur la ville et vous seul pouvez l’arrêter. Armé d'un canon laser, il vous faudra effectuer correctement les calculs indiqués sur les astéroïdes pour pouvoir les viser correctement et les détruire.
Le jeu dispose de nombreux niveaux de difficultés, permettant de s'entraîner avec les additions, soustractions, multiplications, divisions et enfin les nombres relatifs. Il conviendra parfaitement pour les écoliers qui doivent réviser leurs tables, tout comme les adultes qui voudraient se mettre à l'épreuve avec des calculs plus difficiles.
Cette réécriture de Tux Math apporte quelques nouveautés: Une option "niveau automatique" pour s'adapter automatiquement au niveau de l'élève, un malus pour ceux qui essaient toutes les réponses possibles, des niveaux avec des opérations à 3 nombres ou plus, et plusieurs thèmes graphiques.

