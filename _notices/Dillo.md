---
nom: "Dillo"
date_creation: "Dimanche, 25 décembre, 2016 - 01:45"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/Dillo.png"
site_web: "http://www.dillo.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "English"
description_courte: "Dillo est un navigateur web minimaliste, incroyablement léger et réactif, mais ne supportant pas le JS."
createurices: "Jorge Arellano Cid"
alternative_a: "Internet Explorer, Opera, Google Chrome"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dillo"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dillo"
---

Jorge Cid pensait que personne ne devait acheter un nouvel ordinateur ou payer un accès haut-débit pour profiter du web. C'est pour cela qu'il conçut Dillo pour qu'il soit léger, rapide et capable d'afficher une page sur un processeur Intel 486 avec une connexion 56k.
Le projet commença en fin 1999, écrit totallement en C basé sur un autre navigateur nommé Armadillo - d'où Dillo. Jorge Arellano Cid est toujours le développeur principal à ce jour.
Le projet est financé par des donations privées ; les efforts pour obtenir des subventions publiques ou des sponsors privés ont été un echec. Le manque de soutien financier amena à un ralentissement du développement en 2006, pour complètement s'arrêter en 2007. Le projet repart en 2008 et reçoit deux mois plus tard €115 de donations de DistroWatch.
Ce projet est un nain comparé à firefox. Mais quiconque l'a utilisé sur une vieille bécane sur des sites sans Javascript sait sa puissance. Anglophones : lisez le lien "philosophie" 
