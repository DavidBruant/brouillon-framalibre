---
nom: "Typst"
date_creation: "Dimanche, 26 mars, 2023 - 20:21"
date_modification: "jeudi, 29 février, 2024 - 16:40"
logo:
    src: "images/logo/Typst.jpg"
site_web: "https://github.com/typst/typst"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Typst, en moins de 20 MO, est une alternative moderne à LaTeX,  désormais disponible en open source."
createurices: "laurmaedje (Laurenz Mädje)"
alternative_a: "LaTeX"
licences:
    - "Licence Apache (Apache)"
tags:
    - "bureautique"
    - "latex"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q117858460"
mis_en_avant: "non"
redirect_from: "/content/typst"
---

Typst est un nouveau logiciel portable de composition, basé sur le balisage, conçu pour être aussi puissant que LaTeX tout en étant beaucoup plus facile à apprendre et à utiliser.
Typst possède :

Un balisage intégré pour les tâches de formatage les plus courantes
Des fonctions flexibles pour tout le reste
Un système de script étroitement intégré
Composition mathématique, notes en bas de pages, bibliographies, etc.
Tables des Matières (outline), index ...
Des temps de compilation rapides grâce à la compilation incrémentale
Des messages d'erreur conviviaux en cas de problème

Documentation complète disponible : tutoriel, et référence :
[https://typst.app/docs/](https://typst.app/docs/)

Fichiers exécutables binaires à télécharger :
[https://github.com/typst/typst/releases](https://github.com/typst/typst/releases)

Pour débuter : des conseils et un exemple complet  (FR) .
[https://typstexa.tuxfamily.org/typst_tuto_exemp_fr/typst_emploi_et_exemple.html](https://typstexa.tuxfamily.org/typst_tuto_exemp_fr/typst_emploi_et_exemple.html)

Exemples et modèles :
[https://github.com/qjcg/awesome-typst](https://github.com/qjcg/awesome-typst)
