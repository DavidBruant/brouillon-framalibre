---
nom: "Zettlr"
date_creation: "Jeudi, 2 mai, 2019 - 14:41"
date_modification: "jeudi, 3 octobre, 2024 - 15:12"
logo:
    src: "images/logo/Zettlr.png"
site_web: "https://www.zettlr.com/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Zettlr a été créé par un chercheur pour gérer et organiser la prise de notes, la rédaction et l’édition."
createurices: "Hendrik Erz"
alternative_a: "Evernote, OneNote, The Archive, Ulysses, Atom, nvALT"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "markdown"
    - "notes"
    - "organisation"
    - "recherche"
    - "rédaction"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/zettlr"
---

L'objectif principal de ce logiciel est de rendre possible tous les types de flux de travail. Donc, peu importe si vous voulez l'utiliser comme une simple application de prise de notes ou pour écrire des livres entiers, ou même écrire en JavaScript, tout est possible. Différents guides vous montrent comment utiliser Zettlr quels que soient vos besoins, vos objectifs, vos méthodes ou votre façon de faire.
Zettlr en tant qu'application de prise de notes, remplace des applications telles que Evernote, OneNote, Obsidian (ou autre logiciel markdown .md) ou nvALT.
Zettlr en tant que Zettelkasten, système d’organisation de la pensée, remplace des applications telles que zkn, nvALT ou The Archive.
Zettlr en tant qu’environnement de développement intégré (IDE), remplace des applications telles que Ulysses, Atom ou Sublime Text.
Mais on peut aussi : écrire un livre, gérer des listes, préparer des cours, etc.
