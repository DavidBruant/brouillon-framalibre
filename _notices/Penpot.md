---
nom: "Penpot"
date_creation: "Lundi, 3 juillet, 2023 - 14:36"
date_modification: "jeudi, 10 octobre, 2024 - 10:45"
logo:
    src: "images/logo/Penpot.svg"
site_web: "https://penpot.app/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Penpot est un outil de prototypage qui permet de concevoir des interfaces UX/UI de sites web et d’applications"
createurices: "Kaleidos"
alternative_a: "Adobe XD, balsamiq, Figma, Sketch, Canva"
licences:
    - "Licence Publique Mozilla (MPL)"
    - "Multiples licences"
tags:
    - "création"
    - "prototypage"
    - "design"
    - "maquette"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q111586053"
mis_en_avant: "non"
redirect_from: "/content/penpot"
---

La plateforme est open source et alimentée par une communauté dont les contributions sont nombreuses (modules et plugins complémentaires, fonctionnalités supplémentaires…).
Les principales fonctionnalités de Penpot sont les suivantes :

Conception de prototypes : la plateforme propose de créer des prototypes de sites web ou d’applications interactifs grâce à des outils intuitifs de création de composants tels que des boutons.
Partage de vos prototypes : vous pouvez partager les prototypes avec les parties prenantes, présenter plusieurs propositions à votre équipe et lancer des tests utilisateurs avec vos conceptions, directement depuis l’outil.
Collaboration : tous les membres d’une équipe de projet peuvent travailler en simultané et en temps réel sur Penpot. L’outil permet de déposer facilement des commentaires directement sur les prototypes.
