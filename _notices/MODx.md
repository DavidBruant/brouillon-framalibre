---
nom: "MODx"
date_creation: "Vendredi, 13 janvier, 2017 - 00:54"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/MODx.png"
site_web: "https://modx.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Un atelier de développement pour créer de simples sites plaquette à de complexe applications web."
createurices: "Raymond Irving et Ryan Thrash"
alternative_a: "eZpublish, TYPO, Contao"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "web"
    - "cms"
    - "environnement de développement"
    - "développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MODx"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/modx"
---

Modx est un CMS (Content Management Syste) ou système de gestion de contenu (SGC).
Deux versions existent:
Evolution : basé sur l'ancien CMS Etomite, est une evolution de la première version. Peut être plus simple à prendre en main pour un débutant.
Révolution : Refonte complète du CMS. Conçu pour des développeurs avancés de sites.
L'installation est très simple copier les fichiers dans votre répertoire, puis d'accéder avec votre navigateur pour suivre les écrans d'installation.
MODX possèdent plusieurs centaine d'extension permettant aussi bien d'ajouter un texte de copyright qu'un module de versionning ou de gestion multilingue.
En natif MODX est multisite.
Grace à XPDO, sa couche d'abstraction de base de donnée il tourne sur Mysql ou MsSql en toute sécurité.

