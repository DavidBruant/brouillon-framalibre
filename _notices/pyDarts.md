---
nom: "pyDarts"
date_creation: "Mercredi, 19 avril, 2017 - 09:52"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/pyDarts.png"
site_web: "http://pydarts.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "pyDarts ! De bons jeux de fléchettes, même virés du pub ;)"
createurices: "Diego, Poilou & contributeurs !"
alternative_a: "BullsEye, Darts connect, Radikal Darts"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "board"
    - "usb"
    - "diy"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pydarts"
---

pyDarts est un jeu de fléchettes électronique complet combinant un logiciel libre et une cible connectée dont les plans sont aussi libres :
Transformez une vieille cible électronique en cible USB avec un Arduino et les plans fournis.
Branchez votre cible puis jouez à vos jeux préférés avec le logiciel pyDarts.
pyDarts permet de jouer jusqu'à 12 joueurs, et inclut une fonction "jeu en ligne" afin de rencontrer des joueurs du monde entier.
pyDarts est  personnalisable et a été testé avec les 3 principaux systèmes d'exploitation.

