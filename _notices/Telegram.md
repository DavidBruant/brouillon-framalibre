---
nom: "Telegram"
date_creation: "Jeudi, 9 février, 2017 - 22:07"
date_modification: "jeudi, 23 janvier, 2025 - 20:05"
logo:
    src: "images/logo/Telegram.png"
site_web: "https://telegram.org/"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un client de messagerie instantanée qui permet d'échanger des messages sécurisés."
createurices: "Nikolaï Dourov, Pavel Dourov"
alternative_a: "WhatsApp, Viber, Facebook Messenger"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "messagerie instantanée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Telegram_%28application%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/telegram"
---

Telegram Messenger est une application de messagerie sécurisée hébergée sur le Cloud. Les utilisateurs peuvent échanger des messages, photos, vidéos et documents d'une taille allant jusqu'à 1,5 Go. Il est aussi possible d'envoyer des messages chiffrés de bout en bout qui ne sont pas stockés sur les serveurs de Telegram. Telegram est une association à but non lucrative, basée à Berlin.
Attention, seuls les applications sont libres, le serveur est encore sous licence propriétaire.
Contre L'Application dépend de Google Voice pour la portion VOIP.
