---
nom: "Apache OpenOffice Writer"
date_creation: "jeudi, 27 février, 2025 - 20:12"
date_modification: "jeudi, 27 février, 2025 - 20:12"
logo:
    src: "images/logo/Apache OpenOffice Writer.png"
site_web: "http://www.openoffice.org/fr/Documentation/Writer/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Apache OpenOffice Writer, l'alternative libre à Word !"
createurices: "Apache Software Foundation"
alternative_a: "Microsoft Word"
licences:
    - "Licence Apache (Apache)"
tags:
    - "Bureautique"
    - "Traitement de texte"
    - "format"
    - "open document"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Writer_(traitement_de_texte)"
lien_exodus: ""
identifiant_wikidata: "Q718390"
mis_en_avant: "non"

---

Apache OpenOffice Writer est un logiciel de traitement de texte appartenant à la suite bureautique Apache OpenOffice, proche cousine de LibreOffice, cette dernière étant également basée sur le projet OpenOffice.org de Sun Microsystems. Etant l'une des alternatives principales à Microsoft Word, ce logiciel libre permet l’édition complète de documents courts ou longs. Il est possible de sauvegarder ses documents en de multiples formats (dont le .doc, mais aussi .odt, .rtf, .html…), lire les formats de Microsoft Word (doc ou docx), exporter au format PDF avec plusieurs options, gérer le suivi de modifications, faire du publipostage, et toutes les autres fonctionnalités que possèdent les logiciels de traitement de texte sur le marché, y compris les plus chers.
