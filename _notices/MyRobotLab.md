---
nom: "MyRobotLab"
date_creation: "Lundi, 9 mai, 2022 - 10:47"
date_modification: "Vendredi, 13 mai, 2022 - 23:27"
logo:
    src: "images/logo/MyRobotLab.jpg"
site_web: "http://myrobotlab.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "MyRobotLab est un cadriciel libre (sous licence Apache) écrit en Java permettant de lancer des scripts Python."
createurices: "Gael Langevin"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "éducation"
    - "programmation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/InMoov"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/myrobotlab"
---

MyRobotLab est un cadriciel écrit en Java sous licence libre Apache pouvant lancer des scripts écrits en Python pour programmer le robot InMoov conçu par le sculpteur français Gael Langevin à partir de janvier 2012.
Les fichiers *.STL permettant de créer les pièces composant le robot InMoov sont disponibles dans la galerie du site Web d'InMoov.
Le robot InMoov peut être commandé vocalement en français (ou en anglais) par l'intermédiaire d'un Kinect.

