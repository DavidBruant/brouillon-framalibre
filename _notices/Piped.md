---
nom: "Piped"
date_creation: "Samedi, 4 juin, 2022 - 16:09"
date_modification: "mardi, 27 août, 2024 - 12:01"
logo:
    src: "images/logo/Piped.png"
site_web: "https://piped.kavin.rocks/"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Invidious, mais avec des fonctionnalités supplémentaires."
createurices: ""
alternative_a: "Youtube"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "vidéo"
    - "streaming"
    - "client youtube"
    - "bloqueur de publicité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q107565255"
mis_en_avant: "non"
redirect_from: "/content/piped"
---

Piped fonctionne de la même manière qu'Invidious . Mais, il ajoute l'API de SponsorBlock qui permet d'éviter de visionnner les rappels d'interaction ou les placements de produits sur les vidéos.

Liste des instances publiques Piped : https://github.com/TeamPiped/Piped/wiki/Instances
