---
nom: "LosslessCut"
date_creation: "Lundi, 16 janvier, 2017 - 09:20"
date_modification: "Mardi, 18 mai, 2021 - 22:44"
logo:
    src: "images/logo/LosslessCut.png"
site_web: "https://github.com/mifi/lossless-cut"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "LosslessCut permet de couper ses vidéos facilement"
createurices: ""
alternative_a: ""
licences:
    - "Creative Commons -By"
    - "Licence MIT/X11"
tags:
    - "multimédia"
    - "vidéo"
    - "éditeur vidéo"
    - "utilitaire"
    - "montage vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/losslesscut"
---

LosslessCut est un petit utilitaire très simple d’utilisation pour couper vos vidéos sans perte de qualité. Il permet de les découper en autant de fichiers distincts ou de les associer en un même fichier. Il le fait sans ré-encoder la vidéo, ce qui est très rapide et sans perte de qualité.
Il permet également de faire une capture (jpeg) de votre vidéo.LosslessCut prend en charge également certains fichiers audios (Ogg, Wav, Mp3, Aac)

