---
nom: "Librewolf"
date_creation: "mardi, 25 juin, 2024 - 20:27"
date_modification: "mercredi, 7 août, 2024 - 19:20"
logo:
    src: "images/logo/Librewolf.svg"
site_web: "https://librewolf.net/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une version personnalisée de Firefox, axée sur la confidentialité, la sécurité et la liberté."
createurices: "LibreWolf Community"
alternative_a: "Google Chrome"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "navigateur web"
    - "firefox"
    - "vie privée"
lien_wikipedia: "https://en.wikipedia.org/wiki/LibreWolf"
lien_exodus: ""
identifiant_wikidata: "Q105623664"
mis_en_avant: "non"

---

Ce projet est une version personnalisée et indépendante de Firefox, dont les principaux objectifs sont la protection de la vie privée, la sécurité et la liberté de l'utilisateur.

LibreWolf est conçu pour augmenter la protection contre les techniques de traçage et d'empreintes digitales, tout en incluant quelques améliorations de sécurité. Ces objectifs sont atteints grâce à des paramètres et des correctifs axés sur la protection de la vie privée et la sécurité. LibreWolf vise également à supprimer toutes les télémétries, les collectes de données et les désagréments, ainsi qu'à désactiver les fonctionnalités anti-liberté telles que les DRM.
