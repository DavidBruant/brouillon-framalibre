---
nom: "strongSwan"
date_creation: "mercredi, 27 décembre, 2023 - 00:22"
date_modification: "mercredi, 27 décembre, 2023 - 00:22"
logo:
    src: "images/logo/strongSwan.svg"
site_web: "https://strongswan.org/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "English"
description_courte: "strongSwan est un client VPN IPsec multiplateforme."
createurices: "Fork de FreeS/WAN"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "VPN"
    - "sécurité"
lien_wikipedia: "https://en.wikipedia.org/wiki/StrongSwan"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.strongswan.android/latest/"
identifiant_wikidata: "Q309893"
mis_en_avant: "non"

---

**strongSwan** est un client VPN multiplateforme pour le standard [IPSec](https://fr.wikipedia.org/wiki/IPsec). Il peut être utilisé pour construire des réseaux privés virtuels sécurisés. Le projet est centré sur les mécanismes d'authentification utilisant des certificats de clé publique [X.509](https://fr.wikipedia.org/wiki/X.509) et le stockage optionnel de clés privées et de certificats sur des cartes à puce via une interface [PKCS#11](https://fr.wikipedia.org/wiki/Public_Key_Cryptographic_Standards) et sur [TPM 2.0](Trusted Platform Module).
