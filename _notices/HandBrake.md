---
nom: "HandBrake"
date_creation: "Lundi, 26 décembre, 2016 - 16:07"
date_modification: "Lundi, 10 mai, 2021 - 13:29"
logo:
    src: "images/logo/HandBrake.png"
site_web: "https://handbrake.fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "HandBrake est un convertisseur de contenu vidéo et ripper des DVDs."
createurices: "John Stebbins"
alternative_a: "Formatfactory"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "dvd"
    - "vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/HandBrake"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/handbrake"
---

HandBrake permet de convertir facilement du contenu vidéo d’un format vers un autre.
Il propose des formats cibles prédéterminés qui évitent de se poser des questions quand il s’agit, par exemple, de publier une vidéo sur un site web dans le format le plus adéquat. Mais il reste possible d’ajuster tous les paramètres d’encodage à la main.
Il permet aussi d’obtenir une vidéo à partir d’un DVD.

