---
nom: "RawTherapee"
date_creation: "Vendredi, 9 février, 2018 - 07:26"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/RawTherapee.png"
site_web: "http://rawtherapee.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "RawTherapee : un logiciel de développement de photos RAW"
createurices: ""
alternative_a: "Adobe Lightroom, Capture One Pro"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "photo"
    - "raw"
    - "photothèque"
lien_wikipedia: "https://fr.wikipedia.org/wiki/RawTherapee"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/rawtherapee"
---

RawTherapee est un logiciel libre , publié sous licence GNU General Public License Version 3, de traitement d'images photographiques numériques, permettant de traiter des images au format brut (format RAW), tels que fournis par le capteur photographique, et que l'on trouve sur appareil photographique reflex numérique, et parfois sur certains hybrides ou compacts, et dont le but est de minimiser les pertes de qualité lors du traitement.
Il a été écrit à l'origine par Gábor Horváth de Budapest, et le développement a été repris en 2010 par une équipe de personnes du monde entier.

