---
nom: "co-ment"
date_creation: "Jeudi, 8 juin, 2017 - 11:06"
date_modification: "Lundi, 4 mars, 2019 - 02:48"
logo:
    src: "images/logo/co-ment.png"
site_web: "http://www.co-ment.com/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Annotation de textes et édition collaborative"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "édition collaborative"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/co-ment"
---

Co-ment permet principalement de commenter des portions de texte. Quelqu'un écrit un texte, le partage à d'autres personnes qui pourront surligner des mots et les commenter, ou continuer à écrire. Le texte final peut être transformé en fichier odt (pour LibreOffice) ou doc.
Co-ment est un logiciel web. Le site officiel propose un service payant et une version gratuite pour un seul texte limité dans le temps.

