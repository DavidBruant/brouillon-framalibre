---
nom: "Superpowers"
date_creation: "Vendredi, 6 janvier, 2017 - 16:46"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/Superpowers.png"
site_web: "http://superpowers-html5.com/index.fr.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Développement de jeux vidéos multi-plateformes avec possibilité de collaboration"
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "développement"
    - "jeu"
    - "html5"
    - "travail collaboratif"
    - "javascript"
    - "développement de jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/superpowers"
---

Vous avez toujours eu envie de créer un jeu vidéo multi-plateformes mais les lignes de codes vous font (un peu) peur ? Superpower est fait pour vous.
Superpower est un logiciel crée par des français de chez Sparklin Labs et leur communauté. Elle vous propose de créer vos jeux vidéos sur toutes les plateformes grâce à la technologie HTML5.  Elle permet d'avoir accès, d'une part, à la partie graphique (sprites, texte ...) et, d'autre part, la gestion des évènements. Pour se faire, Superpower s'appuie sur le langage TypeScript, très proche de Javascript. L'auto-complétion dynamique vous aidera à rédiger votre code.
Son grand atout, le logiciel permet à plusieurs personnes de travailler collaborativement.
La documentation en ligne et l'API sont assez fournies et la communauté pourra répondre à toutes vos questions. De plus, vous pouvez avoir accès lors du téléchargement du logiciel à un pack de sprites en licences CC0 pour bien commencer.
Bon  jeu ;)

