---
nom: "Exodus Privacy"
date_creation: "mercredi, 1 novembre, 2023 - 15:24"
date_modification: "mercredi, 1 novembre, 2023 - 15:24"
logo:
    src: "images/logo/Exodus Privacy.png"
site_web: "https://exodus-privacy.eu.org/fr/"
plateformes:
    - "Web"
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "εxodus analyse les applications Android dans le but de lister les pisteurs embarqués."
createurices: "Exodus Privacy"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "pisteur"
    - "éducation populaire"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Exodus_Privacy"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.eu.exodus_privacy.exodusprivacy/latest/"
identifiant_wikidata: "Q60348651"
mis_en_avant: "oui"
---

εxodus - la plateforme d’audit de la vie privée des applications Android

εxodus analyse les applications Android dans le but de lister les pisteurs embarqués. Un pisteur est un bout de logiciel dont le but est la collecte de données à propos de vous et de vos usages. Ainsi, les rapports d’εxodus vous révèlent les ingrédients du gâteau. εxodus ne décompile pas les applications, sa technique d’analyse est légale.

Le site web d’εxodus vous permet de :

- rechercher le rapport d’une application Android, grâce à son moteur de recherche
- analyser une application Android en soumettant son identifiant
- d’obtenir une liste d’outils et pratiques destinés à mieux maîtriser votre intimité sur mobile

εxodus est une association française à but non-lucratif, régie par la loi de 1901. Cette organisation est animée par des hacktivistes dont le but est la protection de la vie privée.
