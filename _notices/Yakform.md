---
nom: "Yakform"
date_creation: "lundi, 25 décembre, 2023 - 22:22"
date_modification: "lundi, 25 décembre, 2023 - 22:22"
logo:
    src: "images/logo/Yakform.png"
site_web: "https://yakforms.org/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Yakforms vous permet de créer simplement et rapidement des formulaires en ligne."
createurices: "Framasoft"
alternative_a: "Google Forms"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "formulaire"
    - "sondage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Concevez vos enquêtes en ligne facilement tout en respectant votre public.

- Création de formulaires simple et rapide

Créez vos formulaires par simple glisser-déposer. Le grand nombre de champs disponibles permet de créer des formulaires couvrant un large éventail de besoins.

- Partagez vos formulaires

Une fois votre formulaire publié, vous pourrez le partager par courriel ou sur les réseaux sociaux. Vous pourrez même l’embarquer sur votre site web et autoriser son clonage afin de permettre à d’autres de gagner un temps précieux.

- Traitez vos résultats

Vous pourrez aisément accéder aux réponses en ligne, les visualiser sous forme de graphiques ou les exporter en vue de traitements plus complexes.



Vous pouvez utiliser Yakforms chez Framasoft grâce à [Framaforms](https://framaforms.org/), ou en héberger un chez vous !

Yakforms est un logiciel (en mode SaaS) que vous pouvez installer sur le serveur de votre administration, entreprise ou autre organisation.

En hébergeant une instance de Yakforms, vous maîtrisez totalement vos données et vos usages et vous participez à la décentralisation des services sur Internet. Yakforms s'adapte facilement à vos propres besoins.
