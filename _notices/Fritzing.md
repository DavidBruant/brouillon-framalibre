---
nom: "Fritzing"
date_creation: "Mercredi, 21 décembre, 2016 - 20:56"
date_modification: "Mercredi, 21 décembre, 2016 - 20:56"
logo:
    src: "images/logo/Fritzing.jpg"
site_web: "http://fritzing.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Fritzing est un logiciel de CAO pour l’électronique."
createurices: "FH Potsdam"
alternative_a: "Eagle, Altium Designer, DesignSpark PCB"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cao"
    - "électronique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Fritzing"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fritzing"
---

Fritzing est un logiciel de CAO pour l’électronique.
Il permet de concevoir de façon simple et intuitive des circuits électroniques autant de façon schématique que physique (routage, typon), ainsi que la programmation d’un Arduino ou d’un Picaxe à partir du logiciel.
Fritzing permet de couvrir tous les camps de la conception mais il ne permet pas de faire de simulation.
Il faut également savoir que ce projet est peut-être en version bêta mais il est très stable et mis à jour de temps à autre.

