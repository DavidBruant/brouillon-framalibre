---
nom: "Rapidétik"
date_creation: ""
date_modification: "dimanche, 31 mars, 2024 - 09:07"
logo:
    src: "images/logo/Rapidétik.png"
site_web: "https://educajou.forge.apps.education.fr/rapidetik/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Créer en deux clics des étiquettes à partir d'une phrase ou d'une série de mots."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "école"
    - "éducation"
    - "étiquettes"
    - "lecture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

RapidÉtik est un outil permettant de créer rapidement un exercice de type "phrase en désordre".

Il suffit de saisir une phrase et de cliquer sur "créer l'exercice".

Les mots et la phrase peuvent être prononcés par la synthèse vocale. Celle-ci utilise les ressources de votre appareil. Selon votre configuration il est possible que toutes les langues ne soient pas fonctionnelles ; dans ce cas, installez les langues manquantes.

Les mots doivent ensuite être positionnés dans l'ordre sur les emplacements dédiés. L'élève valide avec le bouton vert en bas à droite.

Il est possible de créer un lien partageable. En envoyant ce lien à quelqu'un d'autre, par exemple par e-mail, il ou elle démarrera directement RapidÉtik avec cette phrase. Les liens créés n'ont pas de limite de durée, rien n'est stocké sur le serveur, les mots sont contenus dans le lien lui-même.
