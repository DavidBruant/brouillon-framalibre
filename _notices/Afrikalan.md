---
nom: "Afrikalan"
date_creation: "Mardi, 19 mars, 2019 - 23:06"
date_modification: "Jeudi, 20 mai, 2021 - 16:27"
logo:
    src: "images/logo/Afrikalan.png"
site_web: "http://www.afrikalan.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Des logiciels libres éducatifs sur un Raspberry Pi pour apprendre dans les écoles d'Afrique."
createurices: "Julien MARIN"
alternative_a: "Microsoft Windows"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "école"
    - "raspberry pi"
    - "distribution gnu/linux"
    - "enfant"
    - "apprentissage"
    - "encyclopédie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/afrikalan"
---

Afrikalan est une adaptation de la distribution Linux Raspbian pour y intégrer des logiciels libres éducatifs, les encyclopédies hors ligne Vikidia et Wikipedia.
Des activités de lecture sont traduites en langue bambara.
Les images, les fonds d'écran sont des photos du pays dans lequel vivent les enfants.
L'utilisation d'une carte raspberry Pi 3, équipée de Wifi, permet de copier les applications éducatives sur un smartphone Android.

