---
nom: "Writefreely"
date_creation: "Samedi, 17 avril, 2021 - 15:50"
date_modification: "Samedi, 7 août, 2021 - 14:11"
logo:
    src: "images/logo/Writefreely.png"
site_web: "https://writefreely.org/"
plateformes:
    - "Android"
    - "Autre"
    - "le web"
langues:
    - "English"
    - "Autres langues"
description_courte: "Moteur de blog minimaliste pour le Fediverse"
createurices: "Matt Baer"
alternative_a: "Medium, blogger"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "blog"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.abunchtell.writeas/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/writefreely"
---

Writefreely est un moteur de blog minimaliste écrit en Go. Il est connecté au Fediverse via le protocole ActivityPub, ce qui permet d'être accessible via d'autres services comme Mastodon ou Pixelfed. Il permet d'utiliser les langages html et markdown. On peut décider de rendre notre blog public ou pas.
Elle est disponible soit par la plateforme commerciale Write.as avec une version premium qui permet de mettre notre propre nom de domaine,d'ajouter une newsletter ou par les différentes instances faites par différentes communautés.

