---
nom: "Tarot Confiné"
date_creation: "Mercredi, 30 septembre, 2020 - 13:10"
date_modification: "Samedi, 30 octobre, 2021 - 16:07"
logo:
    src: "images/logo/Tarot Confiné.png"
site_web: "https://github.com/mapariel/tarot_confine/tree/wsversion"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "Pour jouer au tarot, comme en vrai, avec ses amis pendant le confinement, et après..."
createurices: "Martin Moritz"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tarot-confin%C3%A9"
---

Jeu de tarot en ligne. Pour jouer à trois, quatre ou à cinq ,avec appel du roi ou avec un "mort". Le jeu permet de lancer un serveur ou bien de se connecter en tant que client.
Si on lance le serveur et qu'on veut jouer à plusieurs, il faut connaître son adresse IP, ou bien avoir un nom de domaine, et activer la redirection des ports sur son routeur. Il faudra ensuite communiquer cette adresse IP ainsi que le port utilisé aux autres joueurs. Sinon on peut aussi joueur tout seul ce qui permet de tester l'interface.
Le jeu est programmé en Python (version3.7.7), disponible sur de multiples plateformes. On peut aussi télécharger une version exécutable pour Windows.
Enfin, pour la convivialité, mieux vaut jouer en visioconférence (sur la plateforme de son choix), sinon on risque de s'ennuyer !

