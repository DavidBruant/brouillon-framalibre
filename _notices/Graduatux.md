---
nom: "Graduatux"
date_creation: "lundi, 21 octobre, 2024 - 16:57"
date_modification: "lundi, 21 octobre, 2024 - 16:57"
logo:
    src: "images/logo/Graduatux.svg"
site_web: "https://educajou.forge.apps.education.fr/graduatux/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Graduatux est une application qui permet de travailler le repérage sur la droite numérique."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "numération"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

## Présentation

Graduatux est une application libre qui permet de travailler le repérage sur la droite numérique.
Une droite décimale est affichée, avec seulement deux nombres apparents, et cinq à renseigner par l'élève.

## Compétences travaillées

### Attendus de fin de cycle 2
- Utiliser diverses représentations des nombres (écritures en chiffres et en lettres, noms à
l’oral, graduations sur une demi-droite, constellations sur des dés, doigts de la main...)
- Associer un nombre entier à une position sur une demi-droite graduée, ainsi qu’à la
distance de ce point à l’origine.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

### Attendus de fin de cycle 3
- Comparer, ranger, encadrer des grands nombres entiers, les repérer et les placer sur une
demi-droite graduée adaptée.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

## Accès à l'application

### En ligne

Une instance fonctionnelle peut être utilisée sur https://educajou.forge.apps.education.fr/graduatux

### Hors ligne

- Télécharger l'application **[📦 graduatux-main.zip](https://forge.apps.education.fr/educajou/graduatux/-/archive/main/graduatux-main.zip)**
- Extraire le ZIP
- Ouvrir le fichier **index.html**

### Sur Primtux

Graduatux est intégrée au système d'exploitation [Primtux](https://primtux.fr/).
Pour l'utiliser, après l'installation de Primtux, installer les applications complémentaires.