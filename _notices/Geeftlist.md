---
nom: "Geeftlist"
date_creation: "dimanche, 27 octobre, 2024 - 15:16"
date_modification: "mardi, 29 octobre, 2024 - 16:44"
logo:
    src: "images/logo/Geeftlist.png"
site_web: "https://codeberg.org/nanawel/geeftlist/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Geeftlist est une application web conçue pour simplifier et coordonner le partage, la réservation et l'achat de cadeaux entre proches."
createurices: "Anaël Ollier"
alternative_a: "GiftList"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "édition collaborative"
    - "cadeau"
    - "liste"
    - "Noël"
    - "famille"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

La gestion des listes de cadeaux peut parfois être un vrai casse-tête, surtout quand approchent les fêtes de fin d'année.
Geeftlist vise à simplifier ça en permettant de créer des idées cadeaux pour ses proches ou soi-même, de les partager, et d'en gérer les réservations afin d'éviter les doublons et autres erreurs de choix.
Les parents peuvent ainsi proposer des idées pour les enfants et les mettre à disposition des autres membres de la famille qui pourront ainsi éviter le faux pas sous le sapin. Mais tout le monde y gagne et il est ainsi possible de signaler "discrètement" qu'une idée serait très appréciée, que ce soit pour soi-même ou pour un proche.
