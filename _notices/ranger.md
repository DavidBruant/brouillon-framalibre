---
nom: "ranger"
date_creation: "Jeudi, 18 juin, 2020 - 15:50"
date_modification: "Jeudi, 18 juin, 2020 - 15:53"
logo:
    src: "images/logo/ranger.jpg"
site_web: "https://ranger.github.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
langues:
    - "English"
description_courte: "Ranger est un gestionnaire de fichier en ligne de commande (ncurses) écrit en python."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "explorateur de fichier"
    - "cli"
    - "python"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ranger"
---

Ranger est un gestionnaire de fichier en ligne de commande (ncurses) écrit en python.
Sa particularité est qu'il utilise les mêmes raccourcis clavier que vim (h,j,k,l,ctrl+u,ctrl+d) ce qui le rend facile à utiliser pour qui à l'habitude de vim. Son interface peut fonctionner en mode onglet (par défaut) ou double panneau (avec la touche ~) à la façon de midnight commander. Il intègre également un mode de renommage de mass (commande bulkrename), des dossiers favoris, des filtres d'affichage, un mode de sélection visuelle, une copie par lien physique ou symbolique...
Faire la liste complète des fonctionnalités serait trop long, car il y a en a vraiment beaucoup. Je vous renvoie au man ou au site web.

