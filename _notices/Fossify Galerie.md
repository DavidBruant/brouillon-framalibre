---
nom: "Fossify Galerie"
date_creation: "mercredi, 30 octobre, 2024 - 15:22"
date_modification: "mercredi, 30 octobre, 2024 - 15:22"
logo:
    src: "images/logo/Fossify Galerie.png"
site_web: "https://www.fossify.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Parcourez vos photos librement avec Fossify Galerie"
createurices: "Fossify"
alternative_a: "Google Photos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "galerie photo"
    - "photo"
    - "photothèque"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le collectif **Fossify** développe une gamme d'applications libres élégantes et fonctionnelles.
Voici leur application de galerie photo qui fait tout ce qu'on attend d'une telle appli, et même plus !
Choisissez les dossiers à inclure, ou exclure, choisissez de protéger vos photos par un mot de passe ou par empreinte digitale. L'application permet aussi de faire quelques retouches mineures avec un petit éditeur de photo intégré...
Des réglages et paramètres d'apparence sont disponibles, y compris la possibilité de choisir la couleur de l'icône (vert par défaut) de l'application !
