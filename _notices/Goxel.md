---
nom: "Goxel"
date_creation: "Dimanche, 3 mai, 2020 - 11:59"
date_modification: "Jeudi, 22 juillet, 2021 - 14:32"
logo:
    src: "images/logo/Goxel.png"
site_web: "https://goxel.xyz"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Éditeur de voxel (pixel en 3D) multiplateforme."
createurices: "Guillaume Chereau"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "3d"
    - "pixel"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/goxel"
---

Éditeur de voxel (pixel en 3D) multiplateforme.
Fonctionnalités
Couleurs RVB 24 bits.
Taille de scène illimitée.
Tampon d'annulation illimité.
Couches.
Rendu du cube de marche.
Rendu procédural.
Exporter vers obj, pyl, png, magica voxel, qubicle.
Tracé laser.
Logiciel non libre en mobile.

