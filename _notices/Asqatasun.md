---
nom: "Asqatasun"
date_creation: "Mardi, 21 mars, 2017 - 21:38"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo:
    src: "images/logo/Asqatasun.png"
site_web: "http://asqatasun.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Outil d'audit automatique pour les tests d'accessibilité (RGAA, WCAG) sur une page, un site web ou une webapp."
createurices: "Matthieu FAURE"
alternative_a: "tenon.io"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "développement"
    - "web"
    - "création de site web"
    - "wcag"
    - "rgaa"
    - "accessibilité"
    - "java"
    - "analyse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/asqatasun"
---

Le logiciel Asqatasun permet de commencer rapidement un audit d'accessibilité  pour une page, un site web ou une webapp.
Asqatasun détecte sur une page web les règles (RGAA, WCAG) :
non applicable,
respectées (validé),
avec des erreurs (invalidé),
à vérifier manuellement (Pré-qualifié).Points forts :
fiabilité de ses résultats,
détecte de nombreuses erreurs d'accessibilité, 
audit possible jusqu'à 10 000 pages (ou plus...),
rapport détaillé pour améliorer chaque page web,
intégration continue avec Jenkins.
La démo d'Asqatasun est disponible pour vos audits de page.
Ce logiciel s'installe (sous Linux ou Windows) avec un serveur Tomcat et une base de donnée MySQL. Une image Docker permet de le tester rapidement sur votre ordinateur.

