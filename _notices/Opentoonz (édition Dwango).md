---
nom: "Opentoonz (édition Dwango)"
date_creation: "Vendredi, 4 mai, 2018 - 20:42"
date_modification: "Mercredi, 18 octobre, 2023 - 11:18"
logo:
    src: "images/logo/Opentoonz (édition Dwango).png"
site_web: "https://opentoonz.github.io/e/index.html"
plateformes:
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Programme de production d'animation."
createurices: ""
alternative_a: "Toonz"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "création"
    - "animation"
    - "dessin animé"
    - "éditeur"
    - "créativité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenToonz"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opentoonz-%C3%A9dition-dwango"
---

Programme de production d'animation.
Basé sur le code libéré de Toonz qui fut le logiciel du Studio Ghibli.
Le site web du projet propose aussi de télécharger l'outil GTS de scan (compatible Windows uniquement).
License d'OpenToonz : New BSD Licence.
Il existe une variante plus riche et qui elle est disponible sous Linux : l'édition Morevna.

