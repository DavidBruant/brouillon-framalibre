---
nom: "Gramps"
date_creation: "Jeudi, 20 avril, 2017 - 22:38"
date_modification: "dimanche, 15 décembre, 2024 - 15:16"
logo:
    src: "images/logo/Gramps.png"
site_web: "https://gramps-project.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Gramps (Genealogical Research and Analysis Management Programming System) est un logiciel de généalogie."
createurices: ""
alternative_a: "Heredis, Généatique, Ancestrologie, WinGenealogic, WinFamily"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "Généalogie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gramps"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/gramps"
---

Gramps est très pratique pour élaborer et maintenir un arbre généalogique. Compatible avec de nombreux formats spécialisés il permet notamment d'échanger des fichiers avec des membres de sa famille présents en Amérique du Nord par exemple. La prise en main est simple et rapide. Logiciel très efficient ; une référence dans le domaine de la généalogie.
