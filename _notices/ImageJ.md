---
nom: "ImageJ"
date_creation: "Mardi, 10 janvier, 2017 - 13:30"
date_modification: "Mardi, 10 janvier, 2017 - 19:03"
logo:
    src: "images/logo/ImageJ.jpg"
site_web: "https://imagej.nih.gov/ij/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Logiciel de traitement et d'analyse d'images"
createurices: "Wayne Rasband"
alternative_a: "Adobe Photoshop"
licences:
    - "Domaine public"
tags:
    - "science"
    - "image"
    - "analyse"
    - "java"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ImageJ"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/imagej"
---

ImageJ est un logiciel de traitement d'image et d'analyse d'image, initialement développé pour des applications biomédicales.
Il a la particularité de fonctionner avec beaucoup de fenêtres flottantes, et l'utilisation de macro permet d'automatiser efficacement les taches.
ImageJ permet de travailler avec des images, des stacks (pile d'images) de différentes dynamiques (8-bits, 16-bits, 32-bits, RGB) et de nombreux formats (TIFF, PNG, GIF, JPEG, BMP, DICOM, FITS, ou raw). Enfin, il peut inclure des dimensions supplémentaires comme la gestion du temps, de l'espace (axe Z)

