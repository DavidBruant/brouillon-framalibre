---
nom: "FreeFileSync"
date_creation: "Lundi, 24 avril, 2017 - 17:29"
date_modification: "lundi, 18 novembre, 2024 - 21:27"
logo:
    src: "images/logo/FreeFileSync.png"
site_web: "https://www.freefilesync.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "FreeFileSync vous aide à synchroniser fichiers et dossiers entre deux support grâce à une interface intuitive."
createurices: "zenju@freefilesync.org"
alternative_a: "SyncToy"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "synchronisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeFileSync"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freefilesync"
---

FreeFileSync est un logiciel gratuit et open source qui vous aide à synchroniser fichiers et dossier pour Windows, Linux, et macOs. Il est conçu pour vous faire gagner du temps lors de la préparation et de l'exécution d'une sauvegarde tout en ayant une jolie interface.

Nota: les dernières versions de freefilesync gratuite est limitée en termes de fonctionnalités, les autres fonctions étant maintenant soumises à une participation.
