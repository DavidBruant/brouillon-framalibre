---
nom: "Breezy Weather"
date_creation: "jeudi, 6 juin, 2024 - 20:30"
date_modification: "jeudi, 6 juin, 2024 - 20:30"
logo:
    src: "images/logo/Breezy Weather.png"
site_web: "https://github.com/breezy-weather/breezy-weather"
plateformes:
    - "Android"
langues:
    - "English"
    - "Español"
description_courte: "Breezy Weather est une application libre de météo pour Android avec beaucoup de fonctionnalités"
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "Météo"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/en/reports/org.breezyweather/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

Breezy Weather est une application météo Android gratuite et open-source, dérivée de Geometric Weather, ajoutant de nouvelles fonctionnalités, de nouvelles sources, modernisant le code, corrigeant les bugs, mettant à jour les dépendances pour des raisons de sécurité, etc., tout en gardant à l'esprit une expérience utilisateur et développeur fluide.

Dans l'application, vous trouverez :
- Conditions météorologiques en temps réel (température, ressenti, vent, indice UV, humidité, point de rosée, pression atmosphérique, visibilité, couverture nuageuse, plafond)
- Prévisions quotidiennes et horaires jusqu'à 16 jours (température, qualité de l'air, vent, indice UV, précipitations, ensoleillement, ressenti)
- Prévisions horaires détaillées (humidité / point de rosée, pression, couverture nuageuse, visibilité)
- Précipitations dans l'heure à venir
- Informations sur la qualité de l'air, les allergènes et les éphémérides
- Alertes de conditions météorologiques sévères et de précipitations

L'application se concentre fortement sur le design, avec une expérience utilisateur simple et épurée, des animations fluides et le Material Design partout, ainsi que de nombreuses options de personnalisation :
- Mode sombre automatique
- Packs d'icônes personnalisés
- Large sélection de widgets pour l'écran d'accueil pour des informations en un coup d'œil
- Fond d'écran animé

Elle n'est pas encore traduite en Français (contributions bienvenues!), mais elle intègre des sources telles que Météo France, ATMO AuRA et RecoSanté (de manière optionnelle)
