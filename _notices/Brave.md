---
nom: "Brave"
date_creation: "Jeudi, 16 avril, 2020 - 00:29"
date_modification: "Vendredi, 21 mai, 2021 - 09:54"
logo:
    src: "images/logo/Brave.png"
site_web: "https://brave.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Brave est navigateur rapide qui essaye de concilier le logiciel libre et la publicité choisie."
createurices: "Brendan Eich"
alternative_a: "Edge, Apple Safari, Chrome, Opera"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "web"
    - "tor"
    - "publicité"
    - "anonymat"
    - "cryptomonnaie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Brave_(navigateur_web)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.brave.browser/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/brave"
---

Brave, initié par Brendan Eich, le créateur de la Mozilla Foundation, est un navigateur web basé sur le moteur de rendu optimisé de Chromium. Rapide, il se distingue aussi par l'originalité de son modèle économique. Il propose aux personnes l'utilisant, selon leur souhait, de recevoir (ou pas) des publicités choisies par le navigateur et respectant la confidentialité de leur navigation. La navigation avec pub est rémunérée en dons redistribués des revenus publicitaires vers des producteurs de contenus choisis.
Le navigateur se distingue aussi par son respect de la navigation confidentielle, notamment en intégrant un mode de connexion via le réseau TOR. La navigation n'est alors pas seulement privée (sans trace sur votre ordi), mais aussi anonyme (sans les traces de votre adresse IP sur les serveurs et le réseau).

