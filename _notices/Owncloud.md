---
nom: "Owncloud"
date_creation: "Jeudi, 5 février, 2015 - 21:06"
date_modification: "Samedi, 21 janvier, 2017 - 16:01"
logo:
    src: "images/logo/Owncloud.png"
site_web: "https://owncloud.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "OwnCloud offre une plate-forme de services de stockage et d’applications diverses dans les nuages."
createurices: "Frank Karlitschek"
alternative_a: "Google Docs, Google Drive, One Drive, Dropbox, Apple iCloud"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "synchronisation"
    - "partage"
    - "stockage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OwnCloud"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/owncloud"
---

Le logiciel libre ownCloud offre une plate-forme de services de stockage et d’applications diverses dans les nuages. Il peut être installé sur n’importe quel serveur, même mutualisé. L’idée du projet est de rendre aux utilisateurs le contrôle de leurs données dans le nuage (le cloud) via une interface claire et facile d’utilisation. Il permet aussi la synchronisation de fichiers, contacts, agenda., etc. avec différents client, dont des applications mobiles.

