---
nom: "Salome"
date_creation: "Mercredi, 10 avril, 2019 - 15:25"
date_modification: "Jeudi, 11 avril, 2019 - 00:50"
logo:
    src: "images/logo/Salome.jpg"
site_web: "https://www.salome-platform.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Salome est une plateforme générique de pré et post processing pour simulation numérique."
createurices: "EDF, CEA, OPEN CASCADE"
alternative_a: "ANSYS Workbench"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "calcul scientifique"
    - "éléments finis"
    - "ingénierie"
    - "post processing"
    - "cad"
    - "cae"
    - "physique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Salome_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/salome"
---

Salome est une plateforme générique de pré et post processing pour simulation numérique. Elle peut être utilisée seule ou intégrer un solveur comme Code_Aster (mécanique des solides, thermique) ou Code_Saturne (mécanique des fluides).
Salome permet de faire de la conception assisté par ordinateur, du maillage (la plateforme intègre Netgnen) et du post traitement des résultats (avec une version de Paraview).
Salome permet d'utiliser les formats BREP, STEP, IGES, STL et XAO pour la géométrie, UNV, STL, CGNS, SAUV et GMF pour le maillage ainsi que le format de données co-développé avec l'INRIA MED.
Le logiciel a été développé par l'industrie nucléaire française afin de servir de plateforme de calcul générique. De nombreuses applications métiers sont basées dessus. Si la plupart sont des applications propriétaires, certaines sont libres (salome_meca, salome_CFD).
A titre de comparaison, l'équivalent commercial de Salome vaut une dizaine de milliers d'euros par utilisateur.

