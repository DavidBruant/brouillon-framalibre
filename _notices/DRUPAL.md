---
nom: "DRUPAL"
date_creation: "Mardi, 3 janvier, 2017 - 14:46"
date_modification: "Dimanche, 26 mars, 2017 - 17:03"
logo:
    src: "images/logo/DRUPAL.jpg"
site_web: "https://www.drupal.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un des CMS parmi les plus fiables et les plus puissants."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Drupal"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/drupal"
---

DRUPAL-7 est un CMS réputé pour sa fiabilité et ses fonctionnalités parmi les plus puissantes de sa catégorie. Il permet entre autre de gérer le multi-site avec un seul moteur.
Les capacités de paramétrage et la possibilité d'introduire du code PHP au sein des pages permettent d'offrir une excellente flexibilité. Les possibilités de mise en page sont sympathiques, les thèmes sont agréables et gratuits pour l'essentiel. L'administration des sites sous Drupal-7 n'est pas très compliquée et on trouve sur le net suffisamment de documentation française pour débuter et prendre en main l'outil.
Drupal-7 est disponible en version allégée (moteur), en version de base (comprend les modules d'extension qui couvrent les fonctions courantes) ainsi que des distributions prêtes à l'emploi sur certains secteurs d'activités.
L'installation est relativement simple et demande peu de ressources (serveur web, base de données, php).
Drupal-7 devrait à terme être supplanté par Drupal-8.

