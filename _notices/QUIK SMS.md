---
nom: "QUIK SMS"
date_creation: "jeudi, 26 septembre, 2024 - 19:49"
date_modification: "vendredi, 27 septembre, 2024 - 19:29"
logo:
    src: "images/logo/QUIK SMS.png"
site_web: "https://github.com/octoshrimpy/quik"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Application de SMS/MMS élégante, libre et open-source."
createurices: "Moez Bahtti, Octoshrimpy"
alternative_a: "Google Messages, Textra, QKSMS,"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sms"
    - "messagerie instantanée"
    - "discussion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Un fork pour faire revivre [QKSMS](https://framalibre.org/notices/qksms.html) !
QUIK SMS propose de remplacer le système de messagerie par défaut sur votre smartphone Android. Créée par Moez Bhatti sous la licence publique générale GNU v3.0 (GPLv3) et repris par Octoshrimpy en 2024. Celui-ci ne révolutionne rien mais évite de se faire tracer, ce qui est déjà pas mal !
- Thèmes et couleurs personnalisables
- Mode nuit et noir profond
- Répondre aux messages directement via une pop-up
- SMS groupés
- MMS
- Envoi de messages en différé
- Recherche dans vos messages
- Disponible sur [F-Droid](https://f-droid.org/packages/dev.octoshrimpy.quik/)
