---
nom: "Césium"
date_creation: "Mercredi, 5 avril, 2017 - 10:23"
date_modification: "Lundi, 7 décembre, 2020 - 15:52"
logo:
    src: "images/logo/Césium.png"
site_web: "https://cesium.app/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Application web de gestion de compte (en monnaie libre Ğ1) pour le logiciel Duniter."
createurices: "Benoit Lavenier"
alternative_a: "Lydia, Monnaie dette : Euro, euro"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cryptomonnaie"
    - "blockchain"
lien_wikipedia: "https://fr.wikipedia.org/wiki/%C4%9E1"
lien_exodus: "https://reports.exodus-privacy.eu.org/en/reports/fr.duniter.cesium/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/c%C3%A9sium"
---

Application web de gestion de compte (de monnaie libre) pour le logiciel Duniter.
Césium est un client web développé en JavaScript.
Il est aussi bien utilisable en tant qu’application web qu’application de bureau ainsi que sur les ordinateurs de poche tel Android, Ubuntu Touch et Firefox OS.

