---
nom: "Draw.io"
date_creation: "vendredi, 7 juin, 2024 - 12:37"
date_modification: "vendredi, 7 juin, 2024 - 12:37"
logo:
    src: "images/logo/Draw.io.png"
site_web: "https://www.drawio.com/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Réalisez des diagrammes ou des organigrames"
createurices: "Gaudenz Alder, JGraph Ltd"
alternative_a: "Microsoft Visio"
licences:
    - "Licence Apache (Apache)"
tags:
    - "diagramme"
    - "graphisme"
    - "organigramme"
    - "infographie"
lien_wikipedia: "https://en.wikipedia.org/wiki/Diagrams.net"
lien_exodus: ""
identifiant_wikidata: "Q95734044"
mis_en_avant: "oui"

---

Pour réaliser des diagrammes (ou des organigrammes),  Draw.io propose tout un ensemble d'outils vectoriels fonctionnant un mode similaire à l'élaboration d'un diaporama. Les outils (formes, icônes...) sont classés par thèmes, il y a toute une base de dessins prêts à l'emploi. D'autres outils permettent la mesure et une mise en page précise. Il est même possible d'utiliser des variables. On peut exporter la réalisation dans de multiples formats (images, page web, PDF, etc.). L'application est en ligne mais peut aussi être téléchargée. Cette version locale se présentant comme un paquet utilisable sous différents systèmes d'exploitation. Il est aussi possible de configurer l'application pour héberger ses réalisations sur un service distant comme Gitlab ou un *drive dans les nuages.

Note : Diagrams.net est le nom de domaine hébergeant Draw.io.
