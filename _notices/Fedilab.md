---
nom: "Fedilab"
date_creation: "Mercredi, 28 mars, 2018 - 22:04"
date_modification: "Lundi, 29 avril, 2019 - 15:14"
logo:
    src: "images/logo/Fedilab.png"
site_web: "https://fedilab.app/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Un client polyvalent pour accéder au fediverse sur Android."
createurices: "tom79"
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "mastodon"
    - "peertube"
    - "réseau social"
    - "décentralisation"
    - "microblog"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/68670/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/fedilab"
---

Fedilab est un client polyvalent disponible sur Android. Il permet d'accéder aux principaux réseaux du fediverse : Mastodon, Peertube, Pixelfed, Pleroma, GNU Social, et Friendica.
Principales fonctionnalités :
Ajout de plusieurs comptes
Lecture des fils d'actualités (personnel, local, public) et affichage de la conversation lors du clic sur un statut ayant des réponses
Écriture de statuts (ajout de médias et de liens, programmation de la publication, contrôle de la diffusion : privé, local, public)
Actions sur les statuts (retoot, réponse, mise en favoris ou en signet, traduction, filtrer par mots-clés, etc.
Actions sur les comptes (suivre, bloquer, afficher les détails, accepter ou refuser l'abonnement si compte privé)
Recherche (par mots-clés ou en cliquant sur un tag)
Notifications
Navigateur intégré

