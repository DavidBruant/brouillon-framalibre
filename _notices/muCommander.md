---
nom: "muCommander"
date_creation: "Jeudi, 19 mars, 2020 - 22:33"
date_modification: "Lundi, 10 mai, 2021 - 14:35"
logo:
    src: "images/logo/muCommander.jpg"
site_web: "http://www.mucommander.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "muCommanderEst léger ,gestionnaire de fichier multi-plateforme avec une interface à 2 panneaux et des onglets."
createurices: "Maxence Bernard, Nicolas Rinaudo, Arik Hadas, Mariusz Jakubowski"
alternative_a: "FreeCommander  Exploreur Nautilus"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "fichier"
    - "java"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mucommander"
---

Voici une liste non exhaustive de ce que vous trouverez :
 _System de fichier Virtuel : avec prise en charge des volumes locaux, FTP, SFTP, SMB, NFS, HTTP, Amazon S3, Hadoop HDFS
_ Navigation par onglet
 _Copier rapidement, déplacer, renommer des fichiers, créer des répertoires, fichiers e-mail...
_ Parcourir, créer et décompresser les archives ZIP, RAR, 7z, TAR, Gzip, Bzip2, ISO/NRG, AR/Deb et LST
  les fichiers ZIP peuvent être modifiés à la volée, sans avoir à recompresser l’archive entière
_ Prise en charge de plusieurs fenêtres et Onglets
_ Accès complet au clavier
_ Hautement configurable
_ Disponible en 27 langues : anglais, français, allemand, espagnol, tchèque, chinois simplifié et traditionnel, polonais, hongrois, russe, slovène, roumain, italien, coréen, brésilien

