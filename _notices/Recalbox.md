---
nom: "Recalbox"
date_creation: "Jeudi, 5 janvier, 2017 - 22:58"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/Recalbox.png"
site_web: "http://www.recalbox.com/"
plateformes:
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Recalbox est un système d'emulation de vielles consoles sur Raspberry Pi."
createurices: ""
alternative_a: "Uplay, Steam, Origin"
licences:
    - "Multiples licences"
tags:
    - "jeu"
    - "raspberry pi"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/recalbox"
---

Vous êtes nostalgique de la NES, l'Atari, de la Sega, la GameBoy ? Vous allez pouvoir rejouer à tous les jeux présents sur ces consoles avec une simple Raspberry Pi.
RecalBox est un système d'exploitation pour RaspberryPi emulant une quarantaine de systèmes de vieilles consoles. La librairie dispose de plus de 40 000 jeux, de quoi passer de noubreuses heures de jeux en famille.
RecalBox est très simple à utiliser et permet aussi de faire un serveur Kodi sur la Framboise, faisant ainsi un media center pour votre salon.
Spécificités :
-Utilisez de nombreux types de manettes (Bluetooth, XBox, PS3, ...)
-Rembobinez votre jeu
-Sauvegardez des parties en cours
Une grande communauté se cache derrière RecalBox permettant d'améliorer la compatibilités des systèmes, jeux, manettes et différentes langues.

