---
nom: "Spring Engine"
date_creation: "Samedi, 24 décembre, 2016 - 01:45"
date_modification: "Mardi, 5 mars, 2019 - 00:06"
logo:
    src: "images/logo/Spring Engine.png"
site_web: "https://springrts.com/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
description_courte: "Développé par des fans de Total Annihilation à l'origine, ce moteur de RTS a énormement évolué depuis..."
createurices: "Stefan Johansson et Robin Westberg"
alternative_a: "Total Annihilation, Planetary Annihilation, War Commander"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "rts"
    - "développement de jeu vidéo"
lien_wikipedia: "https://en.wikipedia.org/wiki/Spring_Engine"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/spring-engine"
---

Spring est un moteur de jeu 3D de RTS : on ne joue pas "à spring" à proprement parler, mais plus à l'un de ses mods (comme Zero-K, Balanced Annihilation, Tech Annihilation, Spring 1994...).
Pour jouer en réseau on passe généralement par un client (liste ici : https://springrts.com/wiki/GamesDownloads), généralement springlobby, pour pouvoir ensuite rencontrer des joueurs en ligne et lancer des parties rapidement.

