---
nom: "LocalSend"
date_creation: "mardi, 11 juin, 2024 - 16:04"
date_modification: "mardi, 11 juin, 2024 - 16:04"
logo:
    src: "images/logo/LocalSend.png"
site_web: "https://localsend.org/fr"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Solution de partage de fichiers multiplateforme via WiFi"
createurices: "Tien Do Nam"
alternative_a: "AirDrop, Nearby Sharing, WeTransfer"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "transfert de fichiers"
    - "partage de fichiers"
    - "partage d'image"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.localsend.localsend_app/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

LocalSend est une application open source permettant de partager des fichiers et des messages entre des appareils à proximité utilisant le réseau wifi local. La communication entre les appareils est entièrement cryptée de bout en bout via HTTPS. Pas d'Internet requis. Pas de localisation*. Pas de pubs. Actuellement, cette application est disponible sur Android, iOS, macOS, Windows et Linux. Vous pouvez trouver toutes les options de téléchargement sur la page d'accueil officielle.

*Le système d'exploitation peut toujours collecter des données d'utilisation.
