---
nom: "Openverse"
date_creation: "Mercredi, 28 avril, 2021 - 15:08"
date_modification: "jeudi, 11 avril, 2024 - 17:31"
logo:
    src: "images/logo/Openverse.png"
site_web: "https://openverse.org/fr"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Un méta-moteur pour rechercher facilement des ressources multimédia sous licence libre sur le Web"
createurices: "Creative Commons"
alternative_a: ""
licences:
    - "Multiples licences"
tags:
    - "multimédia"
    - "image"
    - "vidéo"
    - "audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openverse"
---

Openverse est un méta-moteur de recherche d'images, d'audios et de vidéos libres de droits. Il est possible de filtrer les résultats selon la licence des images (tous les licences Creative Commons) et leur source. Il agrège les ressources des sites comme Flickr, Wikimedia Commons ou Youtube.
