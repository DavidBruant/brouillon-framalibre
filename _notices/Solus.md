---
nom: "Solus"
date_creation: "Mercredi, 30 octobre, 2019 - 11:56"
date_modification: "Dimanche, 10 septembre, 2023 - 19:20"
logo:
    src: "images/logo/Solus.png"
site_web: "https://getsol.us"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Distribution Linux indépendante, Install Today. Updates Forever !"
createurices: "Ikey Doherty"
alternative_a: "Microsoft Windows, MacOS, Solaris"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Solus"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/solus"
---

Solus est une distribution Linux indépendante qui propose un système d'exploitation moderne  avec une interface élégante pour nos ordinateurs personnels.
Elle est livrée avec les environnement de bureau GNOME, Plasma de KDE et MATE, mais surtout Budgie, un environnement créé et maintenu par la communauté Solus, qui offre une expérience de bureau alternative.
Dédié à tout type d'utilisateur :

Pour la bureautique : LibreOffice, Firefox, Thunderbird, MPlayer...
Pour les développeurs :

IDE : Atom, Idea, Qt Creator, Visual Studio…
Language : Go, Rust, PHP, Node.js, Ruby…
Outil : Git, SVN, Mercurial…

Pour les créateurs de contenu : Synfig Studio, GIMP, Inkscape, Avidemux…
Pour les joueurs : Itch.io, Lutris et Steam…


Nommé meilleures distributions Linux de 2017 par OMG! Ubuntu.
Classé 6e rang des distributions les plus consultées en 2018.

