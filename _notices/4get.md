---
nom: "4get"
date_creation: "lundi, 24 juin, 2024 - 18:05"
date_modification: "mercredi, 7 août, 2024 - 19:38"
logo:
    src: "images/logo/4get.png"
site_web: "https://4get.ca"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Un métamoteur qui respecte votre vie privée."
createurices: "lolcat"
alternative_a: "Google Search"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métamoteur de recherche"
    - "recherche web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q122602765"
mis_en_avant: "non"

---

Ce métamoteur fournit aux utilisateurs un moyen de recherche de documents sur Internet qui respecte la vie privée, qui soit extrêmement léger, sans publicité et gratuit, avec un minimum de code javascript optionnel. La page "settings" vous permet entre autre de choisir les moteurs vers lesquels diriger vos requêtes.

Pour les administrateurs,  le [dépôt](https://git.lolcat.ca/lolcat/4get) du projet contient une documentation pour l'installer vous-mêmes.
