---
nom: "Code_Saturne"
date_creation: "Mardi, 20 novembre, 2018 - 12:59"
date_modification: "mercredi, 5 février, 2025 - 14:43"
logo:
    src: "images/logo/Code_Saturne.png"
site_web: "https://www.code-saturne.org/cms/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un logiciel de modélisation pour la mécanique des fluides."
createurices: ""
alternative_a: "ANSYS Fluent, ANSYS CFX, StarCCM+"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "éléments finis"
    - "solveur"
    - "simulation"
    - "physique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Code_Saturne"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/codesaturne"
---

Code_Saturne est un solveur utilisant la méthode des éléments finis dans le domaine de la mécanique des fluides. Il est développé depuis 1997 par EDF.
Il permet de modéliser les écoulements incompressibles ou dilatables, avec ou sans turbulence ou transfert de chaleur. Différents modules complémentaires peuvent être agrégés. Des couplages sont possibles avec d'autres logiciels libres de modélisation : Code_Aster (mécanique des structures) ou SYRTHES (thermique).
Code_Saturne peut être intégré dans la plateforme Salome qui propose un environnement complet incluant modélisation de la géométrie, maillage et post-traitement des résultats.
