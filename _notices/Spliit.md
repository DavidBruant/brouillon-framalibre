---
nom: "Spliit"
date_creation: "lundi, 3 juin, 2024 - 23:48"
date_modification: "lundi, 3 juin, 2024 - 23:48"
logo:
    src: "images/logo/Spliit.webp"
site_web: "https://spliit.app/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Application web permettant de gérer un budget partagé entre plusieurs personnes."
createurices: ""
alternative_a: "Tricount"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "Comptabilité"
    - "Finances"
    - "Gestion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cette application web permet de gérer les dépenses entre plusieurs personnes, sans avoir besoin de se connecter, et donne une synthèse de qui doit rembourser qui.
