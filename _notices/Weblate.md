---
nom: "Weblate"
date_creation: "Mercredi, 28 février, 2018 - 10:53"
date_modification: "Mardi, 15 mai, 2018 - 12:02"
logo:
    src: "images/logo/Weblate.png"
site_web: "https://weblate.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Weblate est un système libre de gestion de traductions en mode Web."
createurices: "Michal Čihař"
alternative_a: "Transifex, locize"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "traduction"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Weblate"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/weblate"
---

Weblate est un outil de traduction en mode web fortement lié aux systèmes de contrôle de versions. A travers une interface utilisateur claire et simple, Weblate permet la propagation de traductions entre sous-projets, peut vérifier la qualité des traductions et est automatiquement lié aux fichiers sources.
C'est une application web que l'on peut installer et auto-héberger, et il existe aussi une offre payante hébergée si on ne souhaite pas l'installer.
Weblate fonctionne très bien avec un serveur GitLab, Github, Bitbucket. Il est possible d'importer les traductions déjà existantes d'un projet, de permettre à tout le monde de contribuer à la traduction d'un projet via son interface web, et ensuite pousser les traductions sur le repository.
Il y a un système de revue, de suggestions, de commentaires sur une traduction.
On peut installer une authentification tiers, par exemple on peut se connecter avec son compte Github. Notre compte permet ensuite de garder un historique.

