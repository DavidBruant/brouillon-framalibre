---
nom: "Sengi"
date_creation: "mercredi, 13 décembre, 2023 - 18:43"
date_modification: "mercredi, 13 décembre, 2023 - 18:43"
logo:
    src: "images/logo/Sengi.png"
site_web: "https://nicolasconstant.github.io/sengi/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un client pour Mastodon et Pleroma surpportant les comptes multiples"
createurices: "Nicolas Constant"
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "mastodon"
    - "fediverse"
    - "Pleroma"
    - "réseau social"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q118740392"
mis_en_avant: "non"


---

Sengi est un client de bureau simple et léger pour Mastodon et Pleroma. Il permet de regrouper ses comptes et ses timelines de manière intuitive. Il dispose de plusieurs options permettant d'arranger ses affichages. Dans l'ensemble, son interface est très facile pour une utilisation quotidienne. Il dispose aussi d'un correcteur orthographique basique mais pratique.
