---
nom: "searx"
date_creation: "Jeudi, 20 décembre, 2018 - 16:07"
date_modification: "Lundi, 19 décembre, 2022 - 11:19"
logo:
    src: "images/logo/searx.png"
site_web: "https://searx.github.io/searx/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un méta-moteur de recherche respectueux de votre vie privée !"
createurices: "Adam Tauber"
alternative_a: "Google Search, Bing, Qwant, Ecosia, Lilo, DuckDuckGo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métamoteur de recherche"
    - "recherche"
    - "recherche web"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Searx"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/searx"
---

Searx est un métamoteur de recherche libre et décentralisé. Les requêtes effectuées ne sont pas enregistrées par searx afin de protéger la vie privée des utilisateur·rice·s.
En tant que métamoteur, searx collecte et affiche les résultats issus de plusieurs dizaines de moteurs de recherche. Les moteurs utilisés (ou non) sont paramétrables dans les préférences.
En tant que logiciel décentralisé, searx est installé sur différentes instances (différents serveurs). En France, des associations comme Framasoft, Aquilenet ou La Quadrature du Net l'ont installé sur leurs serveurs.

