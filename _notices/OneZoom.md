---
nom: "OneZoom"
date_creation: "Samedi, 25 mars, 2017 - 13:10"
date_modification: "Samedi, 25 mars, 2017 - 13:10"
logo:
    src: "images/logo/OneZoom.png"
site_web: "http://www.onezoom.org/life.html"
plateformes:

langues:
    - "Français"
    - "English"
description_courte: "OneZoom est une carte interactive qui permet d'explorer toutes les formes de vie sur Terre."
createurices: "James Rosindell, Yan Wong, Luke Harmon"
alternative_a: ""
licences:
    - "Licence MIT/X11"
    - "Multiples licences"
tags:
    - "science"
    - "encyclopédie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/onezoom"
---

OneZoom est un site web permettant de naviguer entre toutes les espèces à travers les âges.
Les espèces, représentées par des feuilles, sont reliées par des branches en fonction de leur évolution. En cliquant sur les feuilles, on peut retrouver toutes les informations (wikipedia, Encyclopedia of Life, IUCN Red List, NCBI) des espèces.
Les données viennent principalement de l'Open Tree Of Life, et la branche des (eu)bactéries est volontairement exclue.

