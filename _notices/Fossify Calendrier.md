---
nom: "Fossify Calendrier"
date_creation: "mercredi, 30 octobre, 2024 - 15:27"
date_modification: "mercredi, 30 octobre, 2024 - 15:27"
logo:
    src: "images/logo/Fossify Calendrier.png"
site_web: "https://www.fossify.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gérez votre agenda en toute liberté avec Fossify Calendrier !"
createurices: "Fossify"
alternative_a: "Google Agenda"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "agenda"
    - "calendrier"
    - "organisation"
    - "emploi du temps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le collectif **Fossify** développe une gamme d'applications libres élégantes et fonctionnelles.
Voici leur application d'agenda qui fait tout ce qu'on attend d'une telle appli !
De nombreuses vues du calendrier sont possibles (hebdomadaire, mensuelle, quotidienne, etc.)
L'application permet d'ajouter les jours fériés pour de nombreux pays (dont la France) dans l'affichage de son calendrier.
Il est possible d'ajouter des calendriers synchronisés via le protocole CalDAV.
Des réglages et paramètres d'apparence sont disponibles, y compris la possibilité de choisir la couleur de l'icône (vert par défaut) de l'application !
