---
nom: "Miro"
date_creation: "Samedi, 26 janvier, 2019 - 22:32"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/Miro.png"
site_web: "http://www.getmiro.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Centralisez les sites web de vidéos sur un seul logiciel."
createurices: "Participatory Culture Foundation"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "logiciel de gestion de vidéos"
    - "vidéos"
    - "lecteur de flux rss"
    - "flux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Miro_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/miro"
---

Miro (anciennement appelé Democracy Player ou DTV) est une application de télévision par Internet développée par l'association Participatory Culture Foundation (en). Le logiciel peut automatiquement télécharger des vidéos à partir de « chaînes » basées sur le format RSS, ainsi que trier et jouer ces chaînes. Il est basé sur XULRunner et est libre et open source.
wikipedia

