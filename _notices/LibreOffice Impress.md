---
nom: "LibreOffice Impress"
date_creation: "Vendredi, 30 décembre, 2016 - 14:29"
date_modification: "jeudi, 27 février, 2025 - 20:15"
logo:
    src: "images/logo/LibreOffice Impress.png"
site_web: "http://www.libreoffice.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de création de diaporamas issu de la suite LibreOffice."
createurices: ""
alternative_a: "Microsoft PowerPoint, Google Slides"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "diaporama"
    - "présentation"
    - "diapositive"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LibreOffice"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/libreoffice-impress"
---

LibreOffice Impress permet de composer des diaporamas qui intègrent (ou pas) des éléments des autres logiciels de la suite LibreOffice. Il possède un mode normal destiné à la composition, un mode plan et un mode « notes » pour annoter les présentations afin de servir de support à l'orateur. Il dispose d'outils de dessin et de diagrammes, des effets 2D et 3D pour apporter du style à la production finale.
