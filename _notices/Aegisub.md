---
nom: "Aegisub"
date_creation: "Jeudi, 12 janvier, 2017 - 12:39"
date_modification: "vendredi, 28 février, 2025 - 18:10"
logo:
    src: "images/logo/Aegisub.png"
site_web: "https://github.com/TypesettingTools/Aegisub"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un éditeur de sous-titres libre et complet"
createurices: "Rodrigo Braz Monteiro, Niels Martin Hansen, Thomas Goyne"
alternative_a: "Subs Factory"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "multimédia"
    - "sous-titres"
    - "montage vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Aegisub"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/aegisub"
---

Aegisub permet de créer facilement des fichiers de sous-titres pour vos vidéos et karaokés. Il supporte de très nombreux formats de sous-titres, vidéo et audio.
Le logiciel dispose d'outils puissants pour mettre en forme, styliser et synchroniser les sous-titres avec la vidéo.
Info temporaire : le développement est en cours de redémarrage et le domaine officiel est en reconstruction, on trouvera les versions à télécharger dans les "releases" de la forge de développement ou dans les dépôts des principales distributions GNU/Linux
