---
nom: "Freenet"
date_creation: "Samedi, 18 mars, 2017 - 11:04"
date_modification: "jeudi, 16 janvier, 2025 - 19:29"
logo:
    src: "images/logo/Freenet.png"
site_web: "https://hyphanet.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Freenet est une plateforme p2p de publication résistante à la censure et protégeant l'anonymat."
createurices: "Hyphanet Development Team"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "réseau"
    - "anonymat"
    - "vie privée"
    - "p2p"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Freenet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freenet"
---

Freenet a été crée suite à la déclaration de Mike Godwin en 1996 de l'"Electronic Frontier Foundation" qui s’inquiétait de l'avenir d'internet et de sa disparition sous sa forme actuelle.
Freenet est un logiciel libre qui vous permet de partager anonymement des fichiers, parcourir et publier des "freesites" qui sont des sites internets seulement accessibles via Freenet. Le logiciel est décentralisé afin de le rendre moins vulnérables aux attaques. Une option "Darknet" activable permet à Freenet de ne se connecter qu'aux personnes de confiances (amis) ce qui rajoute une épaisse couche de sécurité si nécessaire.
Les communications sont chiffrées et transmises à différents nœuds sur le réseau ce qui les rends difficiles à tracer. L'utilisateur fournit à Freenet un peu de sa bande passante et stocke des fichiers du réseau sur son disque dur. Les fichiers sont automatiquement conservés ou supprimés suivant leur degré de popularité. Ces fichiers sont également cryptés.
