---
nom: "BunkerWeb"
date_creation: "mardi, 25 juin, 2024 - 11:41"
date_modification: "mardi, 25 juin, 2024 - 11:41"
logo:
    src: "images/logo/BunkerWeb.png"
site_web: "https://www.bunkerweb.io"
plateformes:
    - "le web"
    - "GNU/Linux"
    - "Autre"
langues:
    - "English"
description_courte: "BunkerWeb est un pare-feu applicatif (Web Application Firewall - WAF) open-source permettant de sécuriser vos services web (ex : sites internet, applicatifs web, APIs, ...)."
createurices: "Bunkerity"
alternative_a: "CloudFlare"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "cybersecurite"
    - "pare-feu"
    - "firewall"
    - "administration système"
    - "réseau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q122929604"
mis_en_avant: "non"

---

BunkerWeb est un pare-feu d'applications Web (WAF) de nouvelle génération et open source.

Étant un serveur Web complet (basé sur NGINX), il protégera vos services Web pour les rendre « sécurisés par défaut ». BunkerWeb s'intègre parfaitement à vos environnements existants (Linux, Docker, Swarm, Kubernetes, …) et est entièrement configurable (pas de panique, il existe une superbe interface Web si vous n'aimez pas la ligne de commande) pour répondre à vos propres cas d'utilisation . En d’autres termes, la cybersécurité n’est plus un problème.

BunkerWeb contient des fonctionnalités de sécurité principales dans le cadre du noyau, mais peut être facilement étendu avec des fonctionnalités supplémentaires grâce à un système de plugins.
