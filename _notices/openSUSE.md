---
nom: "openSUSE"
date_creation: "Samedi, 21 janvier, 2017 - 12:32"
date_modification: "Lundi, 10 mai, 2021 - 14:06"
logo:
    src: "images/logo/openSUSE.png"
site_web: "https://www.opensuse.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "openSUSE est une distribution GNU/Linux stable, verte et fun !"
createurices: "Communauté openSUSE"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
    - "linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Opensuse"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opensuse"
---

openSUSE est une distribution GNU/Linux communautaire, sponsorisée par la société allemande SUSE.
Elle existe aujourd'hui sous deux formes :
openSUSE Leap, la version classique d'openSUSE, visant stabilité et durée de vie ;
openSUSE Tumbleweed, la version en rolling release, intégrant les dernières versions stables des logiciels pour GNU/Linux.
Une des forces du projet est son foisonnement d'outils, développés pour faciliter la vie des utilisateurs comme des contributeurs. Le centre de contrôle YaST permet par exemple de configurer facilement de nombreux aspects du système, sans avoir à éditer des fichiers texte. L'Open Build Service permet quant à lui de construire et distribuer facilement des paquets logiciels via une interface web.

