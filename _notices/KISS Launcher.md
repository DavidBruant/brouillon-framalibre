---
nom: "KISS Launcher"
date_creation: "Mercredi, 22 mars, 2017 - 10:51"
date_modification: "dimanche, 13 octobre, 2024 - 18:36"
logo:
    src: "images/logo/Kisslauncher.png"
site_web: "http://kisslauncher.com/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un lanceur d'applications alternatif pour Android"
createurices: "Sandoche & Neamar & Bénilde"
alternative_a: "OpenLauncher, Trebuchet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "lanceur d'applications"
    - "android"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kisslauncher"
---

KISS Launcher (KISS pour **K**eep **I**t **S**imple and **S**tupid : faites que ce soit stupidement simple) est un lanceur d'applications alternatif très simple d'utilisation se basant sur un système de recherche.
La barre de recherche permet de chercher dans les contacts, dans les applications, de calculer des expressions mathématiques simples et même de faire une requête sur le moteur de recherche de son choix.
En proposant de nombreuses options de personnalisations (gestes d'actions rapides, personnalisation de l'apparence, moteurs de recherche personnalisés) ses créateurs (français !) prétendent que l'essayer une semaine, c'est l'adopter pour trois ans !
Ce lanceur d'applications prend en charge les widgets (horloge, calendrier, ...) et dispose d'une barre de favoris.
Cette application est très légère sur la consommation de la batterie et des ressources système, et est développée activement.
KISS Launcher est disponible sur [F-Droid](https://framalibre.org/notices/f-droid.html) et sur le Play Store, bien à jour sur les deux plateformes.
