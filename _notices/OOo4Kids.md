---
nom: "OOo4Kids"
date_creation: "Lundi, 23 février, 2015 - 11:30"
date_modification: "Lundi, 10 mai, 2021 - 14:22"
logo:
    src: "images/logo/OOo4Kids.png"
site_web: "http://educoo.org/OOo4Kids.php"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "OOo4Kids est une version d'OpenOffice adaptée aux enfants"
createurices: ""
alternative_a: "Microsoft Office, iWork"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "bureautique"
    - "suite bureautique"
    - "enfant"
    - "dyslexie"
lien_wikipedia: "http://fr.wikipedia.org/wiki/OOo4Kids"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/ooo4kids"
---

OOo4Kids est une version d'OpenOffice adaptée aux enfants et à l'éducation.
Cette version est allégée des quelques options plutôt compliquées à utiliser pour des enfants (programme adapté aux enfants de 7 à 12 ans).
Ses principales modifications sont :
polices cursives installées par défaut,
3 niveaux d'utilisateur pour aider le nouveau venu mais aussi laisser des options plus poussées aux habitués,
une interface adaptée et simplifiée (grandes icônes, couleurs vives),
prise en charge des handicaps (troubles visuels et dyslexie),

