---
nom: "FreeOTP"
date_creation: "Dimanche, 3 décembre, 2017 - 14:35"
date_modification: "Lundi, 4 décembre, 2017 - 11:20"
logo:
    src: "images/logo/FreeOTP.png"
site_web: "https://freeotp.github.io/"
plateformes:
    - "Android"
    - "Apple iOS"
langues:
    - "English"
description_courte: "FreeOTP est une application de double authentification générant des mots de passe à usage unique."
createurices: ""
alternative_a: "Google Authenticator"
licences:
    - "Licence Apache (Apache)"
tags:
    - "sécurité"
    - "chiffrement"
    - "authentification"
    - "mot de passe"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freeotp"
---

FreeOTP est une application de double authentification pour les systèmes utilisant des protocoles de mots de passe uniques. Les jetons peuvent être ajoutés facilement en scannant un code QR.
FreeOTP implémente des standards ouverts : HOTP et TOTP.

