---
nom: "Waterbear"
date_creation: "Vendredi, 17 mars, 2017 - 15:08"
date_modification: "Vendredi, 5 août, 2022 - 13:36"
logo:
    src: "images/logo/Waterbear.PNG"
site_web: "http://waterbear.info/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Waterbear est un logiciel de gestion de bibliothèque full-web."
createurices: "Quentin Chevillon"
alternative_a: "Syracuse, Orphée, Décalog, Flora, BCDI, Co-libris"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "sigb"
    - "bibliothèque"
    - "centre de documentation"
    - "catalogue"
    - "gestion documentaire"
    - "livres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/waterbear"
---

Waterbear est un logiciel de gestion de bibliothèque à installer sur un serveur web (type Apache), PHP et MySQL. Il est full-web, ce qui signifie qu'il est accessible uniquement depuis un navigateur web.
Il permet d'assurer les fonctions essentielles d'une bibliothèque ou d'un centre de documentation :
Catalogage,
prêt, retours et réservations,
transit,
import/export unimarc,
échanges avec la Bibliothèque De Prêt,
revues (périodiques),
acquisitions,
statistiques,
etc.
Il est possible d'ajouter un catalogue en ligne (OPAC) afin de permettre aux utilisateurs d'interroger le catalogue, consulter leur compte lecteur, diffuser de l'information, etc.
L'OPAC recommandé est Bokeh mais d'autres logiciels sont possibles.

