---
nom: "Gummi"
date_creation: "Jeudi, 23 mars, 2017 - 17:02"
date_modification: "Lundi, 10 mai, 2021 - 14:13"
logo:
    src: "images/logo/Gummi.png"
site_web: "https://github.com/alexandervdm/gummi"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un petit éditeur de documents LaTeX rempli de fonctionnalités"
createurices: "Alexander van der Meij"
alternative_a: "Bakoma Tex, WinEdt, Scientific Workplace"
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "éditeur"
    - "latex"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gummi_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gummi"
---

Gummi est un éditeur LaTex léger et facile à utiliser.
Il possède les caractéristiques suivantes:
Affichage en temps réel du rendu PDF
Correction orthographique (plusieurs langues disponibles dont le français)
Ouverture de plusieurs fichiers simultanément
Gestion de projets LaTeX
Coloration Syntaxique
Utilisation d'extraits de code pour insérer rapidement du code
Assistant pour créer facilement certains objets (tableaux, matrices, images...)
Exportation au format PDF
Intégration de BibTeX pour gérer une bibliographie
Affichage des erreurs de compilations

