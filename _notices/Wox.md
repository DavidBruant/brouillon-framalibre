---
nom: "Wox"
date_creation: "Lundi, 2 septembre, 2019 - 11:12"
date_modification: "Mercredi, 12 mai, 2021 - 16:35"
logo:
    src: "images/logo/Wox.png"
site_web: "http://www.wox.one/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un lanceur d'applications… et bien plus"
createurices: ""
alternative_a: "Launchy, Alfred"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "lanceur d'applications"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wox"
---

Wox est un lanceur pour Windows, activable par défaut par la combinaison de touches Alt+Espace, dont le but premier est de lancer rapidement et facilement ses applications.
Mais les nombreuses fonctionnalités et les plugins de Wox lui permettent de faire bien plus : calculatrice, explorateur de dossiers, recherche sur le Web… sont ainsi quelques-unes des opérations que vous pourrez effectuer efficacement grâce à Wox.
Les utilisatrices et utilisateurs averti·e·s pourront également coder leur propre plugin (plusieurs langages sont supportés).

