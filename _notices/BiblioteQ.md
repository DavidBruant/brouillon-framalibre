---
nom: "BiblioteQ"
date_creation: "Lundi, 28 mars, 2022 - 19:33"
date_modification: "Vendredi, 5 août, 2022 - 13:38"
logo:
    src: "images/logo/BiblioteQ.png"
site_web: "https://textbrowser.github.io/biblioteq"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "BiblioteQ se veut une suite professionnelle d'archivage, de catalogage et de gestion de bibliothèque"
createurices: "textbrowser"
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "métiers"
    - "bibliothèque"
    - "collection"
    - "sigb"
    - "livres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/biblioteq"
---

BiblioteQ se veut une suite professionnelle d'archivage, de catalogage et de gestion de bibliothèque, utilisant une interface Qt et écrit en c++
Pour la stabilité, Il fête ses 20 ans cette année !
Il offre une connectivité à une base de données PostgreSQL ou SQLite.
Requêtes personnalisées en SQL de la base
Les protocoles Open Library, SRU et Z39.50 sont utilisés pour récupérer les données des livres, des revues et des magazines.
Gestion des emprunteurs
Gestion des prêts /retours

