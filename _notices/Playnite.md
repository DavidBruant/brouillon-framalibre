---
nom: "Playnite"
date_creation: "Mercredi, 24 juin, 2020 - 20:10"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Playnite.jpeg"
site_web: "https://playnite.link/"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Tous ses jeux de PC et d'émulateurs réunis dans la même interface"
createurices: ""
alternative_a: "Epic Games Launcher"
licences:
    - "Licence MIT/X11"
tags:
    - "jeu"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/playnite"
---

Cet outil permet de réunir tous ses jeux PC et émulateurs dans une interface unique.
Compatible avec l'intégralité des jeux des comptes Steam, GOG, Origin, Uplay ainsi que Battle.net et Epic Games Launcher, l'utilisateur peut gérer ses jeux, les classer par catégories, éditer leurs métadonnées et les lancer en un clic de souris.
Ce gestionnaire libre bénéficie de mises à jour régulières et d'une longue liste de fonctionnalités à venir. Notons enfin que ce logiciel est facile à prendre en main et propose une interface sobre et intuitive.

