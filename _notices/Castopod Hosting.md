---
layout: notice

nom: "Castopod Hosting"
date_creation: "Dimanche, 18 octobre, 2020 - 20:30"
date_modification: "Mardi, 2 novembre, 2021 - 15:45"
logo: 
    src: "./images/logo/Castopod Hosting.png"
site_web: "http://castopod.org/"
plateformes: "GNU/Linux, BSD, Windows, Autre"
langues: "Français, English"
description_courte: "Castopod Hosting est une plateforme open-source d’hébergement de podcasts, pour tous."
createurices: "Yassine Doghri, Benjamin Bellamy"
licences: "Licence Publique Générale Affero (AGPL)"
mots_clefs: "podcast"
lien_wikipedia: ""
lien_exodus: ""
---

Que vous soyez débutant, amateur ou professionnel, Castopod Host apporte tout ce dont vous avez besoin pour la baladodiffusion. Créez, téléversez, publiez vos podcasts.
D'autre part, Castopod intègre nativement ActivityPub : Conversez sans intermédiaire avec votre audience sur toutes les plateformes du Fédivers (Mastodon, Pleroma, GNU social…) ! Le podcast EST le réseau social.
Mesurez votre audience (norme IAB 2.0) tout en respectant la vie privée de vos auditeurs.
Castopod gère également la plupart des fonctionnalités du podcast namespace de PodcastIndex : GUID, Locked, Funding, Transcript, Chapters, Soundbite, Location, Person…
Enfin, Castopod s’installe en 5mn sur n'importe quelle infrastructure PHP/MySQL : dézippez le paquet et vous êtes prêt !

