---
nom: "Bluegriffon"
date_creation: "Dimanche, 13 août, 2017 - 19:27"
date_modification: "mercredi, 13 novembre, 2024 - 17:06"
logo:
    src: "images/logo/Bluegriffon.png"
site_web: "http://bluegriffon.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Un éditeur HTML Wysiwyg avec un éditeur de code."
createurices: "Daniel Glazman"
alternative_a: "Adobe Dreamweaver"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "développement"
    - "html"
    - "éditeur html"
lien_wikipedia: "https://fr.wikipedia.org/wiki/BlueGriffon"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bluegriffon"
---

"Après 14 ans d'existence", le site est fermé. (Vu le 13 novembre 2024)

Bluegriffon est un éditeur html wysiwyg ("what you see is what you get"). Sa particularité est qu'il intègre également un éditeur de code.
Il supporte la majorité des éléments HTML5 et CSS3.
Il est le successeur de Mozilla Composer et de NVU.
L'éditeur embarque Svg-edit qui permet de créer des images vectorielles et de les utiliser directement dans les pages web.
Comme Firefox, il propose des extensions, dont la majorité sont payantes.
Le logiciel a obtenu un Open Innovation Award lors de la Demo Cup organisée lors de l'Open World Forum 2010 à Paris.
