---
nom: "Persalys"
date_creation: "Mercredi, 3 novembre, 2021 - 15:13"
date_modification: "Lundi, 20 juin, 2022 - 14:19"
logo:
    src: "images/logo/Persalys.png"
site_web: "https://persalys.fr/index.php"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Atelier Salome permettant la gestion des probabilités"
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "incertitudes"
    - "probabilités"
    - "calcul scientifique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/persalys"
---

Persalys est une interface graphique distribuée dans le logiciel Salome permettant d'utiliser la bibliothèque de traitement d'incertitudes OpenTURNS.
Il permet de faire de l'analyse de variabilité, de fiabilité, de la réduction de modèle, du recalage et de l'optimisation probabiliste.

