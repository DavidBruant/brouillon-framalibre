---
nom: "Cartes"
date_creation: "lundi, 22 juillet, 2024 - 22:23"
date_modification: "lundi, 22 juillet, 2024 - 22:23"
logo:
    src: "images/logo/Cartes.png"
site_web: "https://cartes.app"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Des cartes libres et gratuites sur le Web, alternatives aux maps Google et Apple, pour trouver un commerce, un lieu, visiter une ville, calculer un trajet..."
createurices: "Maël THOMAS-QUILLÉVÉRÉ"
alternative_a: "Google & Apple Maps"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "carte"
    - "OpenStreetMap"
    - "itinéraire"
    - "transport"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Des cartes en ligne souveraines et modernes, alternatives aux *maps* des GAFAM.
Fonctions : recherche lieux, commerces, tourisme, photos, photos de rues, itinéraires à pieds, en vélo, en transport en commun et train, voiture partagée, ferry : l'accent sera mis sur les transports écologiques.

Voir [la raison d'être du projet](https://cartes.app/blog/un-beau-voyage).

Ce service est gratuit, entièrement [libre](https://github.com/laem/cartes), et sans collecte de données.

Il est construit sur la carte collaborative <OsmAttribution/> et nombre d'autres projets.
