---
nom: "MakeHuman"
date_creation: "Vendredi, 5 août, 2022 - 00:09"
date_modification: "Vendredi, 5 août, 2022 - 00:09"
logo:
    src: "images/logo/MakeHuman.png"
site_web: "http://www.makehumancommunity.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "MakeHuman est un logiciel libre de modélisation 3D de corps humains."
createurices: ""
alternative_a: "Cinema 4D, Poser, Daz Studio"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "création"
    - "3d"
    - "modélisation"
    - "graphisme"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MakeHuman"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/makehuman-0"
---

MakeHuman est un logiciel libre de modélisation 3D de corps humains. Les modèles générés sont destinés à être importés dans des logiciels de modélisations 3D plus généraux comme Blender ou 3D Studio Max.
MakeHuman dispose d'une interface simple pour générer un corps de type humain en se basant sur des critères généraux tels que le sexe, la musculature, le poids ou l'âge. Il permet de plus d'affiner chaque partie du corps séparément et de modifier la pose du modèle à l'aide d'un système d'os. Il est possible d'enregistrer les poses et les corps générés dans un format spécifique ou de les exporter au format Wavefront OBJ ou Collada.

