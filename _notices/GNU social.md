---
nom: "GNU social"
date_creation: "Vendredi, 7 septembre, 2018 - 18:33"
date_modification: "jeudi, 15 février, 2024 - 10:39"
logo:
    src: "images/logo/GNU social.png"
site_web: "https://gnusocial.rocks/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel de microblogage libre et décentralisé."
createurices: "Matt Lee, Evan Prodromo, Mikael Nordfeldth"
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "microblog"
    - "décentralisation"
    - "fediverse"
    - "gnu"
    - "ostatus"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNU_social"
lien_exodus: ""
identifiant_wikidata: "Q20204906"
mis_en_avant: "non"
redirect_from: "/content/gnu-social"
---

GNU social est un logiciel de microblogage libre et décentralisé.
Grâce au protocole OStatus, GNU social est un logiciel fédéré faisant partie du Fediverse. Il peut interagir avec d'autres logiciels comme Mastodon, Pleroma ou Hubzilla.
