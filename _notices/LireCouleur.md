---
nom: "LireCouleur"
date_creation: "mercredi, 19 février, 2025 - 11:09"
date_modification: "mercredi, 19 février, 2025 - 11:09"
logo:
    src: "images/logo/LireCouleur.png"
site_web: "https://lirecouleur.forge.apps.education.fr/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Outil de transformation de textes pour en faciliter la lecture."
createurices: "Marie-Pierre Brungard"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "adaptation"
    - "dys"
    - "lecture"
    - "texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

LireCouleur utilise le principe de la lecture en couleurs pour faciliter la lecture en mettant en évidence les phrases, les mots, les syllabes ou les graphèmes.
