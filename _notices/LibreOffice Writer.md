---
nom: "LibreOffice Writer"
date_creation: "Mardi, 3 mars, 2015 - 22:55"
date_modification: "jeudi, 27 février, 2025 - 20:14"
logo:
    src: "images/logo/LibreOffice Writer.png"
site_web: "https://fr.libreoffice.org/discover/writer/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "LibreOffice Writer est un logiciel de traitement de texte"
createurices: ""
alternative_a: "Microsoft Word"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "format"
    - "open document"
lien_wikipedia: "http://fr.wikipedia.org/wiki/LibreOffice#Writer"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/libreoffice-writer"
---

LibreOffice Writer est un logiciel de traitement de texte appartenant à la suite bureautique LibreOffice. Principal concurrent de Microsoft Word, ce logiciel libre permet l'édition complète de documents courts ou longs. Il est possible de sauvegarder ses documents en de multiples formats (dont le .doc, mais aussi .odt, .rtf, .html...), lire les formats de Microsoft Word (doc ou docx), exporter au format PDF avec plusieurs options, gérer le suivi de modifications, faire du publipostage, et toutes les autres fonctionnalités que possèdent les logiciels de traitement de texte sur le marché, y compris les plus chers.
