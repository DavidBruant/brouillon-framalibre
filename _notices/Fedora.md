---
nom: "Fedora"
date_creation: "Lundi, 30 janvier, 2017 - 13:05"
date_modification: "Vendredi, 4 janvier, 2019 - 16:46"
logo:
    src: "images/logo/Fedora.png"
site_web: "https://getfedora.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Fedora, la vitrine technologique communautaire du Logiciel Libre."
createurices: "Fedora Project"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "distribution gnu/linux"
    - "linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Fedora_(GNU/Linux)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fedora"
---

Fedora est une distribution GNU/Linux communautaire, sponsorisée par la société américaine Red Hat. Fedora sert à mettre en valeur et faire avancer le Logiciel Libre dans son ensemble.
Fedora est axé sur 4 valeurs fondamentales (Les 4F) :
First : distribuer en premier une nouvelle technologie ;
Freedom : ne proposer que des logiciels libres ;
Friends : être une communauté, pour échanger et progresser ensemble ;
Features : pour ajouter et développer des technologies nécessaires.
Fedora est dérivé en trois produits principaux :
Workstation: pour les stations de travail (ordinateur fixe ou portable) ;
Server : pour les serveurs ;
Atomic : pour les environnements Cloud.
Disponible sur de multiples architectures et proposant de nombreuses variantes. Fedora peut s'adapter aux besoins de tous.

