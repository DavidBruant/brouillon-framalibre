---
nom: "Greenshot"
date_creation: "Samedi, 17 mars, 2018 - 08:58"
date_modification: "Dimanche, 21 juillet, 2019 - 20:45"
logo:
    src: "images/logo/Greenshot.png"
site_web: "http://getgreenshot.org/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "L'application la plus efficace que je connaisse pour faire des captures d'écrans et les éditer ensuite."
createurices: "J. Klingen, Robin Krom"
alternative_a: "snagip"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "capture d'écran"
    - "utilitaire"
lien_wikipedia: "https://en.wikipedia.org/wiki/Greenshot"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/greenshot"
---

Greenshot est un petite appli simple et efficace. Elle permet de faire des captures d'écrans en sélectionnant la zone à capturer sur l'écran. Mais sa puissance est surtout lié au petit éditeur fournit qui possède tout les outils nécessaire pour éditer la capture.
La configuration par défaut du logiciel n'est pas optimale, je recommande les changements suivants :
- ouverture automatique de l'éditeur de greenshot à chaque capture
- désactivation de la loupe

