---
nom: "Singularity"
date_creation: "Lundi, 13 novembre, 2017 - 14:53"
date_modification: "Mercredi, 12 mai, 2021 - 15:01"
logo:
    src: "images/logo/Singularity.png"
site_web: "http://www.singularityviewer.org/home"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Navigateur 3D type V2 utilisé pour se connecter à des mondes virtuels OpenSimulator ou à Second life™."
createurices: ""
alternative_a: "Second Life viewer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "simulation"
    - "3d"
    - "metarvers"
    - "éducation"
    - "monde virtuel"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/singularity"
---

Navigateur 3D (viewer) utilisé pour se connecter à OpenSimulator ou à Second Life™ avec un compte utilisateur.
Ce viewer semble être plus facile d'accès aux débutants  que le viewer Firestorm.  Il  offre de bonnes performances. Bien qu' il n'ait pas été mis à jour depuis juin 2016, il reste compatible avec la  version 0.9.0.0 d'OpenSimulator (dernière version en date du 13 novembre 2017).

