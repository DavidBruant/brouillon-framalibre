---
nom: "Parlatype"
date_creation: "Samedi, 12 octobre, 2019 - 17:21"
date_modification: "Samedi, 12 octobre, 2019 - 17:21"
logo:
    src: "images/logo/Parlatype.png"
site_web: "https://gkarsay.github.io/parlatype/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Logiciel d'aide à la retranscription"
createurices: ""
alternative_a: "Sonal"
licences:
    - "Multiples licences"
tags:
    - "bureautique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/parlatype"
---

Parlatype est un logiciel d'aide à la retranscription d'entretiens audios. Il s'interface avec LibreOffice et permet un contrôle au clavier pour la gestion des médias (play/stop, avancer de 10 secondes...).

