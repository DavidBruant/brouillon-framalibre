---
nom: "CudaText"
date_creation: "Jeudi, 25 mai, 2023 - 09:52"
date_modification: "Jeudi, 25 mai, 2023 - 10:01"
logo:
    src: "images/logo/CudaText.png"
site_web: "https://cudatext.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Editeur de texte et logiciel de développement multiplateforme."
createurices: "Alexey Torgashin"
alternative_a: "Notepad, WordPad, MS Visual Studio, Sublime Text"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "ide"
lien_wikipedia: "https://en.wikipedia.org/wiki/CudaText"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cudatext"
---

Éditeur de texte et logiciel de développement multiplateforme. Écrit en Objet Pascal, ses fonctionnalités peuvent être étendues grâce à des modules complémentaires en Python: plugins, linters, analyseurs d'arbres de code, outils externes, etc.

