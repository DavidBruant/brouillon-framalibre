---
nom: "asunder"
date_creation: "Jeudi, 29 décembre, 2016 - 16:55"
date_modification: "Jeudi, 29 décembre, 2016 - 17:17"
logo:
    src: "images/logo/asunder.png"
site_web: "http://littlesvr.ca/asunder/index.php"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Asunder est un logiciel d'extraction et d'encodage multi-formats pour CD audio."
createurices: "Eric Lathrop, Andrew Smith"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "cd ripper"
    - "extracteur"
    - "audio"
    - "son"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/asunder"
---

Transformer ses CD audio en format Ogg, Mp3, Wav, Flac, WavePack, MusePack, Monkey's Audio ou AAC c'est possible avec Asunder, l'encodeur qui facilite l'extraction des pistes CD audio.
Disponible dans les dépôts de la plupart des distributions GNU/Linux, vous pouvez aussi l'obtenir depuis leur site principal.
Asunder est minimal dans son interface, mais très complet en matière de format de sortie. il vous faudra bien sûr les codecs adéquats sur votre système pour les plus exotiques, mais Asunder prend nativement en charge les formats courants pour vos outils multimédias connectés.

