---
nom: "Biblys"
date_creation: "mercredi, 19 février, 2025 - 11:51"
date_modification: "mercredi, 19 février, 2025 - 11:51"
logo:
    src: "images/logo/Biblys.png"
site_web: "https://biblys.org"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Logiciel libre pour créer des boutiques de livres en ligne"
createurices: "Clément Latzarus"
alternative_a: "Amazon"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "e-commerce"
    - "livres"
    - "ebook"
    - "crowdfunding"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Biblys est un logiciel libre et gratuit pour permettre à des maisons d'édition et des librairies de vendre des livres papiers et numériques sur le web. Il permet à une maison d'édition, une librairie ou tout autre professionel·le du livre de vendre des livres en ligne sans dépendre d'une plateforme comme Amazon.

Parmi les fonctionnalités proposées : la gestion d'un catalogue avec des informations bibliographiques détaillées, une boutique en ligne organisée à l'aide de rayons et de mots-clés, la vente par correspondance avec la gestion des commandes (y compris l’expédition, le suivi et la relance), la gestion du stock (fournisseurs, réassort, retours  inventaires), la vente de livres numériques sans DRM avec téléchargement sécurisé, le paiement sécurisé par carte bancaire, et la possibilité de gérer du contenu éditorial (blog, calendrier d'évènements…) et des campagnes de financement participatif (crowdfunding).
