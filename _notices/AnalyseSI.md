---
nom: "AnalyseSI"
date_creation: "Jeudi, 30 mars, 2017 - 13:16"
date_modification: "jeudi, 16 janvier, 2025 - 21:01"
logo:
    src: "images/logo/AnalyseSI.png"
site_web: "https://launchpad.net/analysesi"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un logiciel de création de base de données, selon la méthode Merise"
createurices: "Loïc Dreux"
alternative_a: "PowerAMC"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "sql"
    - "base de données"
    - "merise"
    - "modélisation"
    - "mysql"
    - "postgresql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/AnalyseSI"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/analysesi"
---

AnalyseSI est un logiciel de modélisation de bases de données.
Il s'appuie sur la méthode Merise et nécessite un environnement Java pour fonctionner.
En construisant simplement le schéma MCD, il génère ensuite les requêtes SQL nécessaires à la création de la base de données. Enfin, on peut connecter le logiciel à sa base de donnée pour qu'il construise la BDD, en respectant les syntaxes MySQL, PostgreSQL ou OracleDB.
