---
nom: "FreeBSD"
date_creation: "Mardi, 3 janvier, 2017 - 16:56"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/FreeBSD.png"
site_web: "https://www.freebsd.org/"
plateformes:
    - "BSD"
langues:
    - "English"
description_courte: "Système d'exploitation de type Unix"
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "bsd"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeBSD"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freebsd"
---

FreeBSD est un système d'exploitation de type Unix, créé en 1993 sur la base de 386BSD.
Le système est très présent sur les serveurs mais il peut être également utilisé pour une machine de bureau, via l'installation d'un environnement graphique. FreeBSD est un système complet: on y trouve un noyau, des applications, une partie utilisateur et les sources du système.
FreeBSD possède un manuel, le handbook, réputé pour sa très grande qualité. Parmi ses autres atouts: les jails, le système de fichiers ZFS, l'hyperviseur bhyve et le système des ports (applications portées sous FreeBSD).
Il existe de nombreux projets utilisant FreeBSD comme base: PfSense, BSD Router Project, TrueOS, FreeNAS....
FreeBSD se présente en version STABLE et CURRENT. CURRENT est recommandée pour les développeurs ou utilisateurs ayant de bonnes connaissances techniques. Dans le cas contraire il est recommandé d'utilisé la version STABLE.

