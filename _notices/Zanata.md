---
nom: "Zanata"
date_creation: "Mercredi, 28 mars, 2018 - 21:43"
date_modification: "Lundi, 4 mars, 2019 - 02:57"
logo:
    src: "images/logo/Zanata.png"
site_web: "http://zanata.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un logiciel de traduction collaboratif."
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "traduction"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zanata"
---

Un logiciel de traduction collaboratif. Il prend en charge tout type de projet, de la traduction de logiciels à celle de sites web, en passant par les documents textes.
Le logiciel se compose de deux parties, une application serveur et un client. Il existe un client web accessible depuis n'importe quel navigateur. Le serveur est disponible sous GNU/Linux, Mac OS X et Windows.
L'interface peut être déroutante au premier abord, mais à l'usage le logiciel se révèle très puissant.

