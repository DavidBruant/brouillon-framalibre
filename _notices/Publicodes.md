---
nom: "Publicodes"
date_creation: "mercredi, 4 décembre, 2024 - 11:59"
date_modification: "mercredi, 4 décembre, 2024 - 11:59"
logo:
    src: "images/logo/Publicodes.svg"
site_web: "https://publi.codes"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
description_courte: "Un langage commun pour les devs et les expert·es"
createurices: "BetaGouv"
alternative_a: "Languages de programmation trop génériques ; DSL"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "algorithmes"
    - "public"
    - "moteur de calcul"
    - "modèle de calcul"
    - "simulation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Publicodes permet de modéliser des domaines métiers complexes, en les décomposant en règles élémentaires simples qui soient lisibles par tout le monde.


Publicodes est utilisé pour calculer plusieurs millions de simulations chaque mois, notamment dans l'administration français pour assurer une version libre, documentée et officielle de pans calculatoires de la loi française. Il est aussi utilisé par des projets sur le domaine de l'environnement où calculs d'empreinte carbone deviennent de plus en plus structurants.
