---
nom: "Matomo"
date_creation: "Lundi, 27 mars, 2017 - 09:27"
date_modification: "Lundi, 24 janvier, 2022 - 23:05"
logo:
    src: "images/logo/Matomo.PNG"
site_web: "https://matomo.org"
plateformes:
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Matomo (anciennement Piwik) est une plateforme de mesures de statistiques web."
createurices: "Matthieu Aubry"
alternative_a: "Google Analytics, Xiti"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "mesure de statistiques"
    - "analyse"
    - "web analytics"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Piwik"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/matomo"
---

Matomo (Piwik) permet de récolter et d'analyser des données sur les visites de votre site afin de voir les heures d'affluence, l'évolution des visites, la provenance des visiteurs, leurs actions, etc.
Il propose également un grand nombre de fonctionnalités avancées: la possibilité de définir des objectifs à atteindre, de créer des variables personnalisées, le suivi du commerce électronique, etc.
Matomo se veut de plus respectueux de la vie privée et propose des options telles que anonymisation des logs, l'absence de profilage marketing, le respect du choix du visiteur s'il a activé l'option "Ne pas pister" de son navigateur, etc.
Il s'agit d'une alternative auto hébergée à Google Analytics de qualité.
Matomo peut être autant utilisé par des particuliers que par des petites entreprises, associations, collectivités ou même des multinationales. Parmi ses utilisateurs, on compte par exemple la CNIL, Forbes, Next INpact…
Il existe une application mobile pour Android et iOS.

