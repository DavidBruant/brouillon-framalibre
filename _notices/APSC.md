---
nom: "APSC"
date_creation: "Mercredi, 16 octobre, 2019 - 14:45"
date_modification: "Vendredi, 7 mai, 2021 - 11:17"
logo:
    src: "images/logo/APSC.png"
site_web: "https://lewebpedagogique.com/apsc"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Logiciel de conception et édition de séquences en arts plastiques."
createurices: "Lama Angelo"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "collège"
    - "école"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/apsc"
---

Cette application va permettre à la fois, de gérer la mise en forme de la séquence du professeur en arts plastiques (et en tenant compte des exigences disciplinaires), mais aussi la mise en page (simplifiée) de la fiche élève, et pour finir la disposition de la feuille d'évaluation.
Le logiciel permet à l'enseignant d'enregistrer ses préparations sous la forme d'un fichier (avec extension .artp).
Un des atouts de cette solution est qu'elle donne la possibilité à l'enseignant de fractionner dans le temps la préparation de ses séquences (les éléments constitutifs, c'est à dire, par exemple, l'incitation, la demande, les notions abordées, ... le questionnement, les questions d'enseignement, ... les différentes références artistiques, les critères de l'évaluation, ... peuvent être modifiés à tout moment dans l'interface du logiciel).

