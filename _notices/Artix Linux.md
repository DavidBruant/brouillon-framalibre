---
nom: "Artix Linux"
date_creation: "lundi, 24 juin, 2024 - 17:29"
date_modification: "mercredi, 7 août, 2024 - 19:58"
logo:
    src: "images/logo/Artix Linux.png"
site_web: "https://artixlinux.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Artix Linux est une distribution rolling-release basée sur Arch Linux"
createurices: "kotor"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "distribution gnu/linux"
    - "linux"
    - "Arch Linux"
    - "Sans-systemd"
lien_wikipedia: "https://en.wikipedia.org/wiki/Artix_Linux"
lien_exodus: ""
identifiant_wikidata: "Q293179"
mis_en_avant: "non"

---

Artix Linux est une distribution Linux en cours de déploiement, partiellement basée sur Arch Linux, dont l'objectif principal est d'offrir une liberté en matière de gestion d'initialisation et de services. Artix propose OpenRC, runit, s6 et dinit. Contrairement à Arch, Artix n'utilise pas systemd et le gestionnaire d'initialisation et de services par défaut est OpenRC.

Artix Linux a ses propres dépôts, et il n'est pas recommandé par les développeurs d'utiliser les paquets Arch en raison de différences telles que les conventions de nommage et les systèmes d'initialisation contrastés.

Arch OpenRC et Manjaro OpenRC ont été lancés en 2012. En 2017, ces projets ont été divisés et Artix Linux a été créé.
