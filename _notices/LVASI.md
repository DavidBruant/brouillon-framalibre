---
nom: "LVASI"
date_creation: "Lundi, 8 août, 2022 - 18:00"
date_modification: "Jeudi, 29 décembre, 2022 - 10:57"
logo:
    src: "images/logo/LVASI.png"
site_web: "https://github.com/SebastienFRA/LVASI"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Un outils permettant de télécharger et installer automatiquement 221 logiciels."
createurices: "Sébastien FRANCOIS"
alternative_a: "Ninite"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "installation"
    - "téléchargement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lvasi"
---

LVASI est un petit programme qui permet de rendre l'acquisition ou la réinstallation de son ordinateur moins fastidieuse.
Pour utiliser LVASI, il vous suffit de sélectionner les logiciels souhaités parmis les 221 proposés (au 23/12/2022) et de lancer le processus. LVASI va alors télécharger les dernières versions disponibles de chaque logiciels sélectionnés et les installer sans aucune action de votre part.
Il est possible de seulement télécharger la sélection, pour ensuite les installer manuellement si vous le préférez.
LVASI est également capable d'installer ce qui a été précédemment téléchargé, utile pour des installations sur plusieurs postes ou pour des installations hors-ligne.

