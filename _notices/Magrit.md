---
nom: "Magrit"
date_creation: "Jeudi, 20 avril, 2017 - 10:42"
date_modification: "mercredi, 25 septembre, 2024 - 11:37"
logo:
    src: "images/logo/Magrit.svg"
site_web: "http://magrit.cnrs.fr/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Magrit est un logiciel de cartographie thématique ou statistique en ligne."
createurices: "UAR RIATE (CNRS / Université Paris Cité)"
alternative_a: "Philcarto, khartis"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "cartographie"
    - "carte géographique"
    - "statistique"
    - "analyse statistique"
    - "statistiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q98732050"
mis_en_avant: "non"
redirect_from: "/content/magrit"
---

Magrit est un logiciel de cartographie thématique ou statistique en ligne.
Après avoir importé ses propres données (fichiers csv, shapefile ou geojson par exemple) l'utilisateur peut réaliser un grand nombre de types de cartes telles que les cartes en figurés proportionnels, les cartes choroplèthes ou les cartes de données qualitatives.
Magrit permet aussi de créer d'autres cartes plus complexes utilisant des concepts d'analyse spatiale (cartes lissées,  cartes des discontinuités, carroyages, anamorphoses...).
Une fois mise en page, une carte peut être sauvegardée sous forme de fichier projet ou exportée en png ou en svg.
