---
nom: "Qubes OS"
date_creation: "lundi, 15 avril, 2024 - 18:39"
date_modification: "mercredi, 17 avril, 2024 - 09:52"
logo:
    src: "images/logo/Qubes OS.png"
site_web: "https://www.qubes-os.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Si vous prenez la sécurité au sérieux, @QubesOS est le meilleur système d'exploitation disponible aujourd'hui. C'est celui que j'utilise, et c'est gratuit.\" Edward Snowden - Lanceur d'alerte - défenseur de la vie privée."
createurices: "Joanna Rutkowska, Rafal Wojtczuk, Alexander Tereshkin, Rafał Wojdyła"
alternative_a: "Windows, Virtual Box"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système d'exploitation (os)"
    - "sécurité"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Qubes_OS"
lien_exodus: ""
identifiant_wikidata: "Q7269652"
mis_en_avant: "non"

---

Axé sur la sécurité, Qubes OS se veut être un système d'exploitation offrant **une protection maximale de la vie privée et des données personnelles**. 

Pour ce faire, il met en oeuvre - à l'aide l'hyperviseur XEN - **des machines virtuelles (VM)** qui **vont isoler les différentes activités que vous effectuez sur votre ordinateur**. 

Avec Qubes OS, vous allez pouvoir définir le schéma organisationnel de vos VM de manière à compartimenter, au plus près de vos besoins, à la fois vos applications, vos données ainsi que les connexions réseau que vous souhaitez utiliser.
  
Par exemple, vos activités de navigation sur l'Internet seront effectuées via une VM dédiée connectée au réseau, tandis que vos données personnelles jugées sensibles (comptes bancaires, mots de passe, photos de famille...) seront stockées dans une VM également dédiée, mais non connectée à l'Internet et donc inattaquable depuis ce dernier. 

De la même manière, vous pourrez, par exemple, choisir d'utiliser votre client de messagerie dans une VM dédiée et limiter ainsi la surface d'attaque exposée en cas d'attaque organisée via votre messagerie. 

En résumé, si une VM est compromise, les autres, elles, demeureront protégées.

Par ailleurs, pour réduire encore plus les risques liés à une éventuelle attaque, vous aurez à votre disposition des VM "jetables". Ainsi, vous pourrez par exemple choisir d'ouvrir n'importe quel document reçu dans votre messagerie dans une VM de ce type, d'un seul clic et sans risque dès lors que la VM jetable s'auto-détruira une fois éteinte...

Bien qu'isolées les unes des autres, vous pourrez néanmoins, sous votre contrôle, réaliser des copie de fichiers d'une VM à l'autre ou encore des opérations de copier-coller.

La possibilité d'utiliser des VM comme "modèle" (template) pour créer des VM applicatives (AppVM) vous permettra de limiter l'ampleur des opérations de mises à jour, mais aussi le taux d'occupation de votre espace disque.

Côté ergonomie, vous aurez notamment la possibilité d'associer des couleurs distinctes à vos différentes VM pour mieux les différencier selon leur niveau de sécurité. Vous retrouverez cette couleur au niveau des icônes et des fenêtres applicatives présentes sur votre bureau. Pratique lorsque vous avez ouvert sur ce dernier des applications issues de VM distinctes !

Dans sa dernière version (4.2), vous aurez également accès à une barre des tâches particulièrement bien pensée dans laquelle vous retrouverez toutes vos VM, classées par catégorie (VM applicatives, VM templates, VM service), vos raccourcis applicatifs préférés, sans oublier les multiples outils associés à l'hyperviseur.

Enfin, pour parfaire le décor, vous disposerez d'un gestionnaire de VM qui vous permettra notamment de réaliser en quelques clics des opérations de sauvegarde/restauration de votre système.

Bref, vous l'aurez compris, Qubes OS sait parfaitement se rendre vite indispensable, avec toutefois une réflexion à mener en préalable comme garantie d'efficacité, celle qui vise à définir précisément les VM dont vous aurez besoin avec leurs principales caractéristiques (VM templates, VM applicatives, connexions réseau...).

_N.B : a noter que, par défaut, vous disposerez dès l'installation des dernières VM Debian, Fedora, ou encore Whonix pour utiliser le réseau Tor..._
