---
nom: "PDFtk"
date_creation: "Vendredi, 21 avril, 2017 - 21:43"
date_modification: "Mercredi, 21 juin, 2017 - 15:11"
logo:
    src: "images/logo/PDFtk.png"
site_web: "https://www.pdflabs.com/tools/pdftk-server/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "La boîte à outil ultime pour modifier les fichiers PDF."
createurices: ""
alternative_a: "Adobe Acrobat DC"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/pdftk"
---

PDFtk (PDF toolkit) fourmille de commandes pour modifier de fond en comble les fichiers PDF.
Fonctionnant en ligne de commande (pour sa version PDFtk Server) il évite d'avoir à lancer une interface graphique usine à gaz pour l'utiliser.
Une simple consultation de l'aide intégrée permet de démarrer sur les chapeaux de roues.
Vous pourrez triturer vos PDF dans tous les sens en les tournant, les découpant, les assemblant, leur collant un filigrane…
NB: PDFtk propose maintenant une interface utilisateur : PDFtk Free. Pour une interface utilisateur sur GNU/Linux, voyez PDFChain.

