---
nom: "LibreOffice Draw"
date_creation: "Jeudi, 27 avril, 2017 - 08:27"
date_modification: "jeudi, 27 février, 2025 - 19:50"
logo:
    src: "images/logo/LibreOffice Draw.png"
site_web: "http://fr.libreoffice.org/discover/draw/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Pour vos dessins : du simple texte illustré aux graphiques vectoriels fouillés."
createurices: "The Document Foundation"
alternative_a: "Microsoft Paint, Adobe Illustrator, Microsoft Publisher, Adobe Acrobat Pro"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "création"
    - "dessin vectoriel"
    - "dessin"
    - "svg"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LibreOffice#Draw"
lien_exodus: ""
identifiant_wikidata: "Q11052448"
mis_en_avant: "oui"
redirect_from: "/content/libreoffice-draw"
---

Pour vos dessins : du simple texte illustré aux graphiques vectoriels fouillés.
LibreOffice Draw fait partie de la suite LibreOffice et permet de dessiner rapidement et simplement des logigrammes, des graphiques divers grâce à une bibliothèque de formes simples.
Très pratique pour produire une présentation dynamique avec Sozi : on dessine une scène sous LibreOffice Draw (on peut également y intégrer des dessins d'OpenClipart.org) puis on l'importe (au format svg) dans Sozi pour l'animer en deux coups de cuiller à pot.
Alternative libre : framavectoriel.org, Inkscape, drawio (desktop)
