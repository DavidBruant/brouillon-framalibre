---
nom: "GRR"
date_creation: "dimanche, 8 septembre, 2024 - 20:02"
date_modification: "dimanche, 8 septembre, 2024 - 20:02"
logo:
    src: "images/logo/GRR.png"
site_web: "https://site.devome.com/fr/grr3"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "GRR est un système de gestion de réservation de ressources telles que des salles"
createurices: "Laurent Delineau et repris par l'entreprise Devome"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "association"
    - "gestion"
    - "réservation de salles"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

GRR est entièrement configurable (gestion des horaires, des accès, des e-mails, etc.) et reste simple d'utilisation.
*Administration de plusieurs types de ressources partagées (salles, voitures, matériels, …)
*Gestion des accès aux ressources
*Réservations périodiques
*Jours « fériés », inaccessible aux réservations
*Réservations « sous réserve », à confirmer avant une date fixée
*Réservations « avec modération » (validation par un gestionnaire)
*Réservations au nom d’autres utilisateurs ou de personnes extérieures 
...
Sous (licence libre)[https://github.com/JeromeDevome/GRR/blob/master/LICENSE] vous pouvez le télécharger et l'installer sur un serveur ou trouver une offre d'hébergement tout compris sur le site .
Documentation : https://devome.com/GRR/DOC/
