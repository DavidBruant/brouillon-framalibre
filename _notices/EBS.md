---
nom: "EBS"
date_creation: "mardi, 19 novembre, 2024 - 11:11"
date_modification: "mardi, 19 novembre, 2024 - 11:11"
logo:
    src: "images/logo/EBS.png"
site_web: "https://ebs.apes-hdf.org/fr"
plateformes:
    - "le web"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un logiciel pour permettre de faire des échanges de biens et de services"
createurices: "Apes Hauts-de-France"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bibliothèque"
    - "échanger"
    - "travail collaboratif"
    - "Prêt"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le logiciel EBS, libre, ouvert et open source, permet aux Tiers-Lieux de faciliter le prêt d'objets (tondeuse, broyeur, voiture, gauffrier) et l'échange de Services (compétences, savoirs, …) sur un territoire donné (un quartier, une ville, un hameau,…) où les acteurs et le habtitants souhaitent mieux collaborer, et mieux consommer au sens où la plateforme leur permettra d'avoir accès à des objets en prêt, sans avoir à multiplier les achats.

C'est une plateforme en marque blanche, qui permet à chaque lieu de personnaliser la plateforme, selon ses besoins, envies…

Le logiciel est destiné aux porteurs de projets qui souhaitent développer une plateforme coopérative à une échelle territoriale locale.

Ce logiciel propose une plateforme d'échange de biens et services au sein d'une communauté, plus ou moins large. La plateforme propose un moyen de rentrer en contact avec quelqu’un pour permettre cet échange : soit en tant que prêteur, qui propose un objet, soit en tant qu’emprunteur, qui l’utilise. Les prêteurs et emprunteurs peuvent être des individus, des lieux, des organisations, des groupes…

Les utilisateurs créent un profil et mettent les objets qu'ils veulent partager. Un calendrier leur permet de gérer les disponibilités de leurs objets ou services. Lors d'une demande d'emprunt, les deux parties peuvent échanger en direct dans une messagerie instantanée. ils se mettent ainsi d'accord sur les modalités de l'emprunt. Les utilisateurs peuvent créer et rejoindre des groupes privés, et décider de partager leurs objets avec plus ou moins d'utilisateurs. La plateforme permet d'envoyer des SMS de rappel pour les emprunteurs.

Pour les administrateur·rice·s de la plateforme, elle est personnalisable en configurant les fonctionnalités disponibles et en personnalisant les contenus via l’espace d’administration. Un tableau de bord permet de mesurer l'activité et de gérer les groupes et les utilisateurs.

La plateforme a été conçue au format responsive pour être utilisée tant sur ordinateur qu’appareil mobile (tablette ou smartphone). Le code est disponible sous licence AGPL-3 (Affero General Public License).

tester l'outil sur une démo : https://ebs.apes-hdf.org/fr

accéder au code et lire la documentation technique: https://github.com/Apes-HDF/EBS

contact: contact@apes-hdf.org
