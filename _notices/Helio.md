---
nom: "Helio"
date_creation: "jeudi, 11 janvier, 2024 - 13:15"
date_modification: "jeudi, 11 janvier, 2024 - 13:15"
logo:
    src: "images/logo/Helio.png"
site_web: "https://helio.fm/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un séquenceur musical multiplateformes"
createurices: ""
alternative_a: "Reason"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sequenceur"
    - "musique"
    - "mao"
    - "midi"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Helio est un logiciel libre de composition musicale multiplateformes (pour bureau et Android). Il offre un séquenceur, un moyen de visualiser la musique, et il connaît très bien la musique micro-tonale.

Il fonctionne avec CoreAudio, ASIO, DirectSound, ALSA, JACK, OpenSLES, et permet d'exporter vers les formats MIDI, WAV, et FLAC.
