---
nom: "Argos Panoptès"
date_creation: "mardi, 9 juillet, 2024 - 17:25"
date_modification: "mardi, 9 juillet, 2024 - 17:25"
logo:
    src: "images/logo/Argos Panoptès.jpg"
site_web: "https://argos-monitoring.framasoft.org/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Logiciel de supervision de sites web"
createurices: "Framasoft, Alexis Métaireau"
alternative_a: "Nagios"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "supervision"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---
Argos Panoptès est un outil de supervision de sites web qui se veut simple et efficace : simple à installer, à configurer, à utiliser et efficace dans sa rapidité et ses capacités d’alertes.

Argos ne permet de surveiller que des sites web, mais il le fait bien.

[Présentation d’Argos sur le Framablog](https://framablog.org/2024/05/16/argos-panoptes-la-supervision-de-sites-web-simple-et-efficace/)